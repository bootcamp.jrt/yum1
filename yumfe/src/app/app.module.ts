import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AuthApi } from './remote';
import { ChefModule } from './chef/chef.module';
import 'hammerjs';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './anon/login/login.component';
import { HungryModule } from './hungry/hungry.module';
import { AppRouting } from './app.routing';
import { AnonModule } from './anon/anon.module';
import { CanBrowse, CanLogin, IsLoggedIn, CanActivateChef, CanActivateAdmin } from './can-browse.service';
// import { SettingsComponent } from './shared/settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SharedModule,
    AnonModule,
    ChefModule,
    AdminModule,
    HungryModule,
    AppRouting
  ],
  providers: [AuthApi, CanBrowse, CanLogin, IsLoggedIn, CanActivateChef, CanActivateAdmin ],
  bootstrap: [AppComponent],
})
export class AppModule { }
