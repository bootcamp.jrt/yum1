import { Injectable } from '@angular/core';
import {
  Routes,
  RouterModule,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { LoggedComponent } from '../shared/logged/logged.component';
import { UsersComponent } from './users/users.component';
import { AuthenticationService } from '../shared/authentication.service';
import { Observable } from "rxjs/Rx";
import { CanActivateAdmin } from "../can-browse.service";


const adminRoutes: Routes = [
  {
    path: 'admin',
    component: LoggedComponent,
    canActivate: [CanActivateAdmin],
     canActivateChild: [CanActivateAdmin],
    children: [
      { path: '', component: HomeComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'users/:id', component: UsersComponent }
    ],

  }
];

export const AdminRouting = RouterModule.forChild(adminRoutes);


