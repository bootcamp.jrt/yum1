import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { UserComponent } from './home/user/user.component';
import { AdminRouting } from './admin.routing';
import { HomeComponent } from './home/home.component';
import { DialogForceDeleteComponent } from './home/user/user.component';
import { UserFormComponent } from './home/user-form/user-form.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { TinyMCEDirective } from './tiny-mce.directive';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminRouting
  ],
  declarations: [
    UserComponent,
    HomeComponent,
    DialogForceDeleteComponent,
    UserFormComponent,
    SettingsComponent,
    UsersComponent,
    AdminNavBarComponent,
    TinyMCEDirective
  ],
  entryComponents: [
    DialogForceDeleteComponent
  ]
})
export class AdminModule { }
