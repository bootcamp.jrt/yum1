import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { TermsAndPrivacyService } from '../../../shared/terms-and-privacy/terms-and-privacy.service';
import { SnackbarService } from '../../../shared/snackbar.service';

import * as remote from '../../../remote';




@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  user: remote.UserSave = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    role: "hungry",
    version: 0
  };

  cleanForm = true;

  loading = false;

  submitted = false;

  regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
  + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:"
  + "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

  @Output()
  createUser: EventEmitter<void> = new EventEmitter<void>();

  roles = [
    'hungry',
    'chef',
    'admin'
  ];

  roleChoice: string = this.roles[0];

  errors = {
    email: 0,
    password: 0,
    confirm: 0
  }

  confirm = "";

  validationMessages = {
    'password': {
      'required': 'Password is required.',
      'minlength': 'Password must be at least 6 characters long.'
    },
    'firstName': {
      'required': 'First name is required.'
    },
    'lastName': {
      'required': 'Last name is required.'
    },
    'email': {
      'required': 'Email is required.',
      'pattern': 'Email must be valid.'
    },
    'repeat': {
      'required': 'Confirmation is required.',
      'validateEqual': 'Password mismatch'
    }
  };

  constructor(
    private adminService: remote.AdminApi,
    public snackBar: SnackbarService,
    public termsService: TermsAndPrivacyService
  ) { }

  ngOnInit() {

  }

  confirmPassword() {

    if (this.user.password.length === 0) {
      this.errors.password = 0;
    } else if (this.user.password.length < 6) {
      this.errors.password = -1;
    } else {
      this.errors.password = 1;
    }

    if (this.confirm === '') {
      this.errors.confirm = 0;
    } else if (this.confirm !== this.user.password) {
      this.errors.confirm = -1;
    } else {
      this.errors.confirm = 1;
    }

    this.cleanForm = false;
  }

  confirmEmail() {
    if (this.user.email === '') {
      this.errors.email = 0;
    } else if (!this.user.email.match(this.regex)) {
      this.errors.email = -1;
    } else {
      this.errors.email = 1;
    }

    this.cleanForm = false;
  }


  buttonAvailable() {
    return this.errors.confirm === 1 && this.errors.email === 1 && this.errors.password === 1;
  }

  submit() {

    this.loading = true;

    // this.user = this.newUserForm.value;
    //in order to be able to delete the extra property
    let userToSend: any = this.user;
    delete userToSend.repeat;
    this.user.version = 0;

    this.adminService.usersPost(userToSend).subscribe(() => {

      this.snackBar.openSuccess('User created!');
      // this.buildForm();
      this.createUser.emit();

      this.user = {
        email: "",
        password: "",
        firstName: "",
        lastName: "",
        role: "hungry",
        version: 0
      };

      this.confirm = '';
      this.cleanForm = true;
      this.loading = false;

    }, error => {
      this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
      this.loading = false;
    });
  }

}
