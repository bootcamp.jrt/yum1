import { Component, OnInit } from '@angular/core';
import * as remote from '../../remote';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading = true;

  users: Array<remote.User>;

  totalItems: number;

  sorting: string = "dsc";

  currentPage: number=1;

  pageSize: number = 10;

  numbersOfItems = [
    10,
    20,
    50,
    100
  ];


  //ordering by registration date is using ordering by id to include time apart from date
  ordering: string = 'id';
  orderingCriteria: Map<String, String> = new Map([
    ['id', 'Registration date'],
    ['firstName', 'First name'],
    ['lastName', 'Last name'],
    ['role', 'Role'],
    ['approved', 'Approval status']
  ]);


  constructor(private adminService: remote.AdminApi) { }


  ngOnInit() {

    this.getUsersList();

  }

  sortAndGet(input: string): void{

    if (input == this.sorting){
      return;
    }

    this.sorting = input;
    this.getUsersList();
  }

  changePageSize() {
    this.currentPage = 1;
    this.getUsersList();

  }

  getUsersList(): void{
    this.adminService.usersGet(this.ordering, this.sorting, this.currentPage-1, this.pageSize).subscribe(userList => {
      this.totalItems = userList.totalItems;
      this.users = userList.users;
      this.loading = false;
    }, error => console.log(error));

  }
  getUsersListPage($event): void{

   if ($event!=null){

      this.currentPage = $event;
      this.getUsersList();

   }


  }

}
