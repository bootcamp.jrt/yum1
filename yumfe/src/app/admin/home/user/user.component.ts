import { Component, OnInit, Input, EventEmitter, Output, Inject } from '@angular/core';
import { AuthenticationService } from '../../../shared/authentication.service';
import { MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { BASE_PATH } from '../../../remote/variables';
import * as remote from '../../../remote';

import { SnackbarService } from '../../../shared/snackbar.service';

@Component({
  selector: 'app-admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  @Input()
  user: remote.User;

  @Output()
  delete: EventEmitter<void> = new EventEmitter<void>();

  imgUrl: string;

  constructor(private adminService: remote.AdminApi,
    private dialog: MdDialog,
    private authService: AuthenticationService,
     public snackBar: SnackbarService,
     @Inject(BASE_PATH) private basePath: string) {
  }

  ngOnInit() {
    this.imgUrl = this.basePath + '/users/'+ this.user.id + '/picture/token?token=' + this.authService.getToken();
  }

  public getLoggedId(): any {
    return this.authService.getLoggedInId();
  }

  deleteUser(): void{
    this.adminService.usersIdDelete(this.user.id).subscribe(() => {
      this.snackBar.openSuccess("user deleted");
      this.delete.emit();
    }, error => {
      console.log(error);
      if (error.status == 402 || error.status == 409 || error.status == 412) {
        this.openDialogDelete(error.json().message, error.status);
      }else{
        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
    }
    });
  }



  openDialogDelete(reason: String, code: Number) {
    let dialogRef = this.dialog.open(DialogForceDeleteComponent);

    dialogRef.componentInstance.reason = reason;
    dialogRef.componentInstance.errorCode = code;

    dialogRef.afterClosed().subscribe(result => {
      if (result === "force") {
        if (code==409){
          this.adminService.usersIdDelete(this.user.id, true).subscribe(() => {

            this.snackBar.openSuccess("ok. finally deleted");
            this.delete.emit();

          }, error => {
            this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
          });
        }else{
          let approveDTO: remote.Approve = {
            approve: false,
            force: true
          };
          this.adminService.usersIdApprovedPut(this.user.id, approveDTO).subscribe(() => {

            this.snackBar.openSuccess("ok. finally dissaproved");
            this.delete.emit();

          }, error => {
            this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
        });
      }
    }
  });
}


}


@Component({

  templateUrl: './dialog-force-delete.html',
})
export class DialogForceDeleteComponent {

  public reason: String;
  public errorCode: Number;

  constructor(public dialogRef: MdDialogRef<DialogForceDeleteComponent>) { }
}
