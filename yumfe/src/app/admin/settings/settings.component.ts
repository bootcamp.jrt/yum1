import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as remote from '../../remote';
import { GlobalSettingsService } from "../../shared/globalSettings.service";
import { DomSanitizer } from "@angular/platform-browser";
import { SnackbarService } from '../../shared/snackbar.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  loading = true;

  settingsForm: FormGroup;
  settings: remote.Settings;

  currencyMap = [
    "&euro;",
    "&dollar;",
    "&pound;",
    "&yen;",
    "&#8381;",
    "&#8377;",
    "&#20803;"
  ];

  currency: string = "&euro;";

  constructor(
    private formBuilder: FormBuilder,
    private adminService: remote.AdminApi,
    public gs: GlobalSettingsService,
    private router: Router,
    private snackBar: SnackbarService
  ) { }

  ngOnInit() {

    this.settings = {
      currency: '',
      deadline: '',
      notes: '',
      terms: '',
      policy: ''
    };

    this.settingsForm = this.formBuilder.group({
      'currency': this.settings.currency,
      'deadline': this.settings.deadline,
      'notes':  this.settings.notes,
      'terms': this.settings.terms,
      'policy': this.settings.policy
    });

    this.adminService.globalsettingsGet().subscribe(settings => {
      this.settings = settings;
      this.loading = false;
    });
  }

  save() {

    this.adminService.globalsettingsPut(this.settings).subscribe(version => {
      this.snackBar.openSuccess('Global Settings are updated!');
      this.router.navigate(['/admin']);
    }, error => {
      console.log(error);
      this.snackBar.openError(error);
    });
  }
}
