import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import * as remote from '../../remote';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';

import { SnackbarService } from '../../shared/snackbar.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  loading = true;

  user: remote.User = {hasPicture:true};
  private sub: any;
  public id: number = -1;

  constructor(private adminService: remote.AdminApi,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService,
    public snackBar: SnackbarService) { }


  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

      this.id = +params['id'];
      if (this.id > 0) {
        this.adminService.usersIdGet(this.id).subscribe(userToEdit => {
          this.user = userToEdit;
        });
      }
      this.loading = false;
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();

  }

  userChange(update: remote.User) {
    this.user = update;
  }
  updateVersion() {
    this.user.version++;
    this.user.approved = !this.user.approved;
  }

  save() {// userPutApiCall
    this.adminService.usersIdPut(this.user.id, this.user).subscribe(() => {


      this.authService.updateUserDetails(this.user.firstName, this.user.lastName);

      this.snackBar.openSuccess('User updated.');

      this.router.navigate(['/admin']);

    }, error =>{
      this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
    });
  }

  reset() {//resetPwdApiCall
    this.adminService.usersIdForgotpwdPost(this.user.id).subscribe(() => {

      this.user.version++;

      this.snackBar.openSuccess('Email with instructions sent to user.');

    }, error =>{
      this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
    });
  }
}
