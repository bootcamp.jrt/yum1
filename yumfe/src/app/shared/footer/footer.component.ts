import { Component, OnInit } from '@angular/core';
import { TermsAndPrivacyService } from "../../shared/terms-and-privacy/terms-and-privacy.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    public termsService: TermsAndPrivacyService
  ) { }

  ngOnInit() {
  }

}
