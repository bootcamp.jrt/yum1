import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import * as remote from '../../remote';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { FileUploader } from 'ng2-file-upload';
import { BASE_PATH } from '../../remote/variables';
import { SnackbarService } from '../../shared/snackbar.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  loading = false;

  _user: remote.User;

  @Input('user')
  set user(newUser: remote.User) {
    if (this._user && this._user.version != newUser.version && newUser.hasPicture)
      this.refreshImg();

    this._user = newUser;
  }

  get user(): remote.User {
    return this._user;
  }

  @Input() showType: number = 0;

  @Output()
  myChange: EventEmitter<remote.User> = new EventEmitter<remote.User>();

  showUploadBtn: boolean = false;

  public uploader: FileUploader;
  uploadURL;

  imgUrl: string = '';
  hungryGetPicPrefix: string = this.basePath + '/settings/picture/token?token=';
  adminGetPicPrefix: string = this.basePath + '/users/';
  form: FormGroup;


  regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
  + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:"
  + "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

  userRoles = [
    'admin',
    'hungry',
    'chef'
  ];

  formErrors = {
    'firstName': '',
    'lastName': '',
    'email': '',
  };

  validationMessages = {
    'firstName': {
      'required': 'First name is required.'
    },
    'lastName': {
      'required': 'Last name is required.'
    },
    'email': {
      'required': 'Email is required.',
      'pattern': 'Email must be valid.'
    }
  };

  constructor(
    private builder: FormBuilder,
    private authService: AuthenticationService,
    private hungryService: remote.HungryApi,
    private adminService: remote.AdminApi,
    @Inject(BASE_PATH) private basePath: string,
    private snackBar: SnackbarService
  ) {
  }
  ngOnInit() {

    this.form = this.builder.group({
      'firstName': [this._user.firstName, Validators.required],
      'lastName': [this._user.lastName, Validators.required],
      'email': [this._user.email, [Validators.required, Validators.pattern(this.regex)]],
      'role': this._user.role
    });


    this.showPicture();
    if (this.showType === 0) {
      this.uploadURL = this.basePath + '/settings/picture';
    } else {
      this.uploadURL = this.basePath + '/users/' + this.showType + '/picture';
    }

    this.uploader = new FileUploader({ url: this.uploadURL, autoUpload: true, authToken: "Bearer " + this.authService.getToken(), removeAfterUpload: true });

    this.uploader.onAfterAddingFile = () => {
      this.loading = true;
    };
    this.uploader.onCompleteAll = () => {
      this.loading = false;
    };
    this.uploader.onErrorItem = () => {
      this.showUploadBtn = false;
      this.snackBar.openError('Something went wrong, try again later.');
    }
    this.uploader.onSuccessItem = () => {
      this.showUploadBtn = false;
      this.refreshImg();
      if(this.showType===0){
        this.authService.updateUserHasPicture(true);
      }
      this.updateUserVersion();
      this.snackBar.openSuccess("Picture saved.");
    }

  }
  public deletePicture() {
    if (this.showType === 0) {
      this.hungryService.settingsPictureDelete().subscribe(() => {
        this.snackBar.openSuccess("Picture successfully deleted.");
        this._user.hasPicture = false;
        this.authService.updateUserHasPicture(false);
        this.updateUserVersion();
      }, error => {
        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
      });
    } else {
      this.adminService.usersIdPictureDelete(this.showType).subscribe(() => {

        this.snackBar.openSuccess("Picture successfully deleted.");
        this._user.hasPicture = false;
        this.updateUserVersion();
      }, error => {

        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
      });
    }
  }

  private updateUserVersion() {
    this._user.version++;
    this.myChange.emit(this._user);
  }

  public refreshImg() {
    this._user.hasPicture = true;
    this.imgUrl = this.completeGetUrl();
  }

  private completeGetUrl(): string {

    if (this.showType === 0) {

      return this.hungryGetPicPrefix + this.authService.getToken() + "&ts=" + (new Date().getTime());

    } else {

      return this.adminGetPicPrefix + this.showType + '/picture/token?token=' + this.authService.getToken() + "&ts=" + (new Date().getTime());

    }
  }


  showUploadButton() {
    this.showUploadBtn = true;
  }

  showMe() {
    if (this.form.valid) {
      this.myChange.emit(this._user);
    }
  }

  showPicture() {

    if (this.showType === 0) {

      this.imgUrl = this.hungryGetPicPrefix + this.authService.getToken() + "&ts=" + (new Date().getTime());

    } else {

      this.imgUrl = this.adminGetPicPrefix + this.showType + '/picture/token?token=' + this.authService.getToken() + "&ts=" + (new Date().getTime());

    }

  }

  onBlurValueChanged(field: any) {
    if (!this.form) { return; }

    const checkForm = this.form;

    this.formErrors[field] = '';
    const control = checkForm.get(field);

    if (control && control.dirty && !control.valid) {

      const messages = this.validationMessages[field];
      for (const key in control.errors) {

        this.formErrors[field] += messages[key] + ' ';

      }
    }
  }

}
