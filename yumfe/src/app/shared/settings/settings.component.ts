import { Component, OnInit } from '@angular/core';
import * as remote from '../../remote';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MdDialog, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { AuthenticationService } from '../../shared/authentication.service';
import { SnackbarService } from '../snackbar.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  current: remote.User = { hasPicture: true };
  pwdForm: FormGroup;
  version: number = 0;
  passwd1: string = '';


  constructor(private hungryService: remote.HungryApi,
    private builder: FormBuilder,
    private router: Router,
    public snackBar: SnackbarService,
    private authService: AuthenticationService) { }

  ngOnInit() {



    this.hungryService.settingsGet().subscribe(settings => {

      this.current = settings;

    }, error => console.log(error));

    this.pwdForm = this.builder.group({
      password: '',
      repeat: ''
    });

  }

  submit() {

    let userSave: remote.UserSave = {};
    userSave.email = this.current.email;
    userSave.password = this.passwd1;
    userSave.lastName = this.current.lastName;
    userSave.firstName = this.current.firstName;
    userSave.role = this.current.role;
    userSave.version = this.current.version;

    this.hungryService.settingsPut(userSave).subscribe(() => {
      //TODO pianw return object k kanw update to localStorage (inject to service)
      this.snackBar.openSuccess("Settings succesfully updated!");
      this.authService.updateUserDetails(this.current.firstName, this.current.lastName);
      this.router.navigate(['/' + this.current.role]);
    }, error => {
      console.log(error.status);
      if (error.status === 409) {
        this.snackBar.openError(error.json().message);
        //TODO update ton current user edw k call sto authService
      }
      else {//400
        this.snackBar.openError(error.json().message);
      }
    });
  }

  loggedChange(update: remote.User) {
    this.current = update;
  }

  fdsubmit() {

  }


}
