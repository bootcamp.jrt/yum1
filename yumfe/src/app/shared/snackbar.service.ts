import { Injectable } from '@angular/core';
import { MdDialog, MdSnackBar, MdSnackBarConfig } from '@angular/material';

@Injectable()
export class SnackbarService {

  constructor(public snackBar: MdSnackBar) { }


  openError(msg: string) {
    let config = new MdSnackBarConfig();
    config.extraClasses = ['isa_error'];
    this.snackBar.open(msg, 'Close', config);
  }
  openSuccess(msg: string) {
    let config = new MdSnackBarConfig();
    config.duration = 5000;
    config.extraClasses = ['isa_success'];
    this.snackBar.open(msg, 'Ok', config);
  }

}
