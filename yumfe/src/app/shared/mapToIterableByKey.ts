import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'mapToIterableByKey' })
export class MapToIterableByKey implements PipeTransform {
  transform(value) {
    const result = [];
    if (value.entries) {
      value.forEach((value: any, key: any) => {
          result.push(key);
      });
    } else {
      return value;
    }
    return result;
  }
}
