import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { BASE_PATH } from '../../remote/variables';
import { Router } from "@angular/router";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  firstName: string;
  lastName: string;
  hasPicture: boolean = true;
  imgUrl: string;
  role: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    @Inject(BASE_PATH) private basePath: string
  ) { }

  goToAccountSettings() {
    this.router.navigate(['/settings']);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.firstName = this.authService.getLoggedInFirstName();
    this.lastName = this.authService.getLoggedInLastName();
    this.hasPicture = this.authService.getLoggedInHasPicture();
    this.role = ''+this.authService.getLoggedInRole();

    if (this.hasPicture) {
      this.imgUrl = this.basePath + '/settings/picture/token?token=' + this.authService.getToken();
    }
    this.authService.subjectChanges().subscribe(urlExtension => {
      this.imgUrl = this.basePath + '/settings/picture/token?token=' + this.authService.getToken() + urlExtension;
      if (urlExtension.length !== 0) {

        this.hasPicture = true;

      } else {

        this.hasPicture = false;
      }
    });
  }

}
