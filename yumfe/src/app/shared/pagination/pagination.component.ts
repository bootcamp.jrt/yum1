import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input()
  totalItems: number;
  @Input()
  currentPage;
  @Input()
  pageSize;

  dotsAfter = false;
  dotsPrevious = false;
  public pager: any;

  @Output()
  pageChanged: EventEmitter<number> = new EventEmitter<number>();

  setPage(page: number) {
    if (page < 1) {
      return;
    }
    if (this.pager && page > this.pager.totalPages) {
      return;
    }

    //this.getPager( page, this.pageSize);
    //this.currentPage = page;
    this.pageChanged.emit(page);

  }

  ngOnInit() {
    this.setPage(1);
  }


  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {

    this.pager = this.getPager(this.currentPage, this.pageSize);

  }


  getPager(currentPage: number = 1, pageSize: number = 1) {

    // calculate total pages
    let totalPages = Math.ceil(this.totalItems / pageSize);

    let startPage: number, endPage: number;
    if (totalPages <= 5) {
      // less than 5 total pages so show all
      startPage = 1;
      endPage = totalPages;
      this.dotsAfter = false;
      this.dotsPrevious = false;
    } else {
      // more than 5 total pages so calculate start and end pages
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 5;
        this.dotsAfter = true;
        this.dotsPrevious = false;
      } else if (currentPage + 2 >= totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
        this.dotsPrevious = true;
        this.dotsAfter = false;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
        this.dotsAfter = true;
        this.dotsPrevious = true;
      }

    }


    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, this.totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = Array.from({ length: (endPage + 1 - startPage) }, (v, i) => i + startPage);

    // return object with all pager properties required by the view
    return {
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

}
