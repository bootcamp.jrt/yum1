import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as remote from '../remote';

const currentUserItem = 'currentUser1';

@Injectable()
export class AuthenticationService {

  private user: any = {};

  subject: Subject<string> = new Subject<string>();

  constructor(
    private authService: remote.AuthApi,
    private hungryService: remote.HungryApi,
    private conf: remote.Configuration
  ) { }

  subjectChanges(): Observable<string> {
    return this.subject;
  }

  login(username: string, password: string): Observable<boolean> {

    let creds: remote.Login = { email: username, password: password };
    return this.authService.authLoginPost(creds).map(token => {
      // login successful if there's a jwt token in the response
      if (token.token) {
        // store username and jwt token in local storage to keep user logged in between page refreshes
        this.user = {
          username: username,
          token: token.token,
          role: token.role,
          firstName: token.firstName,
          lastName: token.lastName,
          id: token.id,
          hasPicture: token.hasPicture
        }

        localStorage.setItem(currentUserItem, JSON.stringify(this.user));

        this.conf.apiKey = "Bearer " + token.token;
        // return true to indicate successful login
        return true;
      } else {
        // return false to indicate failed login
        return false;
      }
    }).catch((error: any) => {

      let msg = "Server error";
      console.log(error);
      let response = error.json();
      if (error && response) {
        if (response.message) msg = response.message;
        if (response.error) msg += " (" + response.error + ")";
      }
      return Observable.throw(msg);
    });
  }

  getToken(): string {
    var token = this.user && this.user.token;
    return token ? token : "";
  }

  isLogged(): boolean {
    let ls = localStorage.getItem(currentUserItem);
    if (ls === null) {
      this.user = undefined;
      this.conf.apiKey = undefined;
      return false;
    }
    this.user = JSON.parse(ls);
    return ((this.user !== undefined) && this.user.token !== undefined);
  }


  getLoggedInId(): Number {
    return this.user ? this.user.id : null;
  }

  getLoggedInRole(): String {
    if (!this.isLogged()) return null;
    return this.user ? this.user.role : null;
  }

  getLoggedInFirstName(): string {
    return this.user.firstName;
  }

  getLoggedInHasPicture(): boolean {
    return this.user.hasPicture;
  }

  getLoggedInLastName(): string {
    return this.user.lastName;
  }

  bootstrapUser(): void {

    this.user = JSON.parse(localStorage.getItem(currentUserItem));

    let token = this.getToken();

    if (token !== "") {

      this.conf.apiKey = "Bearer " + token;

      this.hungryService.settingsGet().subscribe(settings => {

        this.user.role = settings.role;
        this.user.firstName = settings.firstName;
        this.user.lastName = settings.lastName;

        localStorage.setItem(currentUserItem, JSON.stringify(this.user));
      });
    }
  }

  getLoggedInUser() {
    return this.isLogged() ? this.user : undefined;
  }

  updateUserDetails(firstName: String, lastName: String) {

    // takes an object with firstName and lastName
    // this method is called when the user goes to /settings page and after PUT is successful
    // we need to change the user details of the loggedIn user
    // - updates firstName and lastName of the private object in the component
    // - persists the user details in localStorage

    this.user.firstName = firstName;
    this.user.lastName = lastName;

    localStorage.setItem(currentUserItem, JSON.stringify(this.user));
  }

  updateUserHasPicture(hasPicture: boolean) {
    if (hasPicture) {

      this.subject.next("&ts=" + (new Date().getTime()));

    } else {
      //emit to header component that user has no picture any more
      this.subject.next("");

    } this.user.hasPicture = hasPicture;
    localStorage.setItem(currentUserItem, JSON.stringify(this.user));
  }


  logout(): void {

    // removes the localStorage item
    // removes the apiKey in conf
    // removes the private object in the component storing the user details.
    // clear token remove user from local storage to log user out

    localStorage.removeItem(currentUserItem);
    this.user = undefined;
    this.conf.apiKey = undefined;
  }
}
