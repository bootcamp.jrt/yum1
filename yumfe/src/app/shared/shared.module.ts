import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedComponent } from './logged/logged.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { GlobalSettingsService } from './globalSettings.service';
import { Configuration } from '../remote/configuration';
import { SettingsComponent } from './settings/settings.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { MapToIterable } from './mapToIterable';
import { MapToIterableByKey } from './mapToIterableByKey';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminApi } from '../remote';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BASE_PATH } from '../remote/variables';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { EqualValidatorDirective } from './equal-validator.directive';
import { PaginationComponent } from './pagination/pagination.component';
import { DialogForceDisapproveComponent } from './toggle-approval/toggle-approval.component';
import { FileUploadModule } from 'ng2-file-upload';
import {
  MdSlideToggleModule,
  MdGridListModule,
  MdInputModule,
  MdButtonModule,
  MdCardModule,
  MdListModule,
  MdCheckboxModule,
  MdDialogModule,
  MdDialog,
  MdIconModule,
  MdRadioModule,
  MdChipsModule,
  MdSelectModule,
  MdSnackBarModule,
  MdMenuModule,
  MdTabsModule,
  MdTooltipModule,
  MdProgressSpinnerModule
} from '@angular/material';
import { ToggleApprovalComponent } from './toggle-approval/toggle-approval.component';
import { SnackbarService } from './snackbar.service';
import { TermsAndPrivacyDialog, TermsAndPrivacyService } from './terms-and-privacy/terms-and-privacy.service';
import { SpinnerComponent } from './spinner/spinner.component'

@NgModule({
  imports: [
    MdIconModule,
    MdInputModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    MdCardModule,
    MdSelectModule,
    FlexLayoutModule,
    MdButtonModule,
    MdGridListModule,
    MdSlideToggleModule,
    MdMenuModule,
    FileUploadModule,
    MdTabsModule,
    MdTooltipModule,
    MdProgressSpinnerModule
  ],
  declarations: [
    NotFoundComponent,
    LoggedComponent,
    SettingsComponent,
    FooterComponent,
    HeaderComponent,
    ProfileComponent,
    MapToIterable,
    MapToIterableByKey,
    EqualValidatorDirective,
    PaginationComponent,
    ToggleApprovalComponent,
    DialogForceDisapproveComponent,
    TermsAndPrivacyDialog,
    SpinnerComponent
  ],
  exports: [
    BrowserAnimationsModule,
    MdSlideToggleModule,
    MdGridListModule,
    MdInputModule,
    MdButtonModule,
    MdCardModule,
    MdListModule,
    MdCheckboxModule,
    MapToIterable,
    MapToIterableByKey,
    MdDialogModule,
    MdIconModule,
    MdRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MdChipsModule,
    MdTooltipModule,
    EqualValidatorDirective,
    MdSelectModule,
    FlexLayoutModule,
    PaginationComponent,
    MdSnackBarModule,
    ProfileComponent,
    ToggleApprovalComponent,
    SpinnerComponent
  ],
  providers: [
    AuthenticationService,
    GlobalSettingsService,
    Configuration,
    MdDialog,
    AdminApi,
    MdSnackBarModule,
    {provide: BASE_PATH, useValue: 'http://localhost:8082/api'},
    SnackbarService,
    TermsAndPrivacyService
  ],
  entryComponents: [
    DialogForceDisapproveComponent,
    TermsAndPrivacyDialog
  ]
})
export class SharedModule { }
