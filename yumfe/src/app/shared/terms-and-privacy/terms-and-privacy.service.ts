import { Injectable, Component } from '@angular/core';
import { MdDialogRef, MdDialog } from '@angular/material';

import * as remote from '../../remote';

@Injectable()
export class TermsAndPrivacyService {

  constructor(
    private dialog: MdDialog,
    private authService: remote.AuthApi
  ) { }

  private openDialog(tabIndex: number) {
    this.authService.authPolicyGet().subscribe(policy => {
      const dialogRef = this.dialog.open(TermsAndPrivacyDialog);
      dialogRef.componentInstance.privacy = policy.policy;
      dialogRef.componentInstance.terms = policy.terms;
      dialogRef.componentInstance.tabIndex = tabIndex;
    });
  }

  showTerms() {
    this.openDialog(0);
  }

  showPolicy() {
    this.openDialog(1);
  }

}

@Component({

  templateUrl: './terms-and-privacy.dialog.html',
  styleUrls: ['./terms-and-privacy.scss']
})
export class TermsAndPrivacyDialog {

  terms: string;
  privacy: string;
  tabIndex: number;

  constructor(
    public dialogRef: MdDialogRef<TermsAndPrivacyDialog>
  ) { }
}
