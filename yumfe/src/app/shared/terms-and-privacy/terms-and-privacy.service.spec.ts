import { TestBed, inject } from '@angular/core/testing';

import { TermsAndPrivacyService } from './terms-and-privacy.service';

describe('TermsAndPrivacyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TermsAndPrivacyService]
    });
  });

  it('should ...', inject([TermsAndPrivacyService], (service: TermsAndPrivacyService) => {
    expect(service).toBeTruthy();
  }));
});
