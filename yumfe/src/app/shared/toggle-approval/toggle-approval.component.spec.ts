import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleApprovalComponent } from './toggle-approval.component';

describe('ToggleApprovalComponent', () => {
  let component: ToggleApprovalComponent;
  let fixture: ComponentFixture<ToggleApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
