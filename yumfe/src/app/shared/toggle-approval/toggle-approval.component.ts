import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../remote';

import { SnackbarService } from '../snackbar.service';

@Component({
  selector: 'app-toggle-approval',
  templateUrl: './toggle-approval.component.html',
  styleUrls: ['./toggle-approval.component.scss']
})
export class ToggleApprovalComponent implements OnInit {
  @Input()
  isApproved: boolean;
  @Input()
  userId: number;
  @Output()
  versionChanged: EventEmitter<void> = new EventEmitter<void>();
  constructor(
    private adminService: remote.AdminApi,
    private dialog: MdDialog,
    public snackBar: SnackbarService
  ) { }

  ngOnInit() {
  }

  toggleApproval(val): void {

    let approveDTO: remote.Approve = {
      approve: false,
      force: false
    };

    approveDTO.approve = val.checked;
    this.adminService.usersIdApprovedPut(this.userId, approveDTO).subscribe(() => {

      this.isApproved = approveDTO.approve;
      let success = 'disapproved';
      if (this.isApproved)
        success = 'approved';
      this.versionChanged.emit();
      this.snackBar.openSuccess("User got " + success);

    }, error => {
      console.log(error);
      val.source.toggle();
      if (error.status == 409) {
        this.openDialog(error.json().message, val);
      } else {
        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
      }
    });

  }

  openDialog(reason: String, tog: any) {
    let dialogRef = this.dialog.open(DialogForceDisapproveComponent);
    dialogRef.componentInstance.reason = reason;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "force") {
        let approveDTO: remote.Approve = {
          approve: false,
          force: true
        };
        this.adminService.usersIdApprovedPut(this.userId, approveDTO).subscribe(() => {
          tog.source.toggle();
          this.isApproved = false;
          this.versionChanged.emit();
          this.snackBar.openSuccess("User finally got disapproved");
        }, error => {
          this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');

        });
      }
    });
  }

}

@Component({

  templateUrl: './dialog-force-disapprove.html',
})
export class DialogForceDisapproveComponent {

  public reason: String;

  constructor(public dialogRef: MdDialogRef<DialogForceDisapproveComponent>) { }
}
