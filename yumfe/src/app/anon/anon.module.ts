import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';

import { AuthApi } from '../remote';
import { SharedModule } from '../shared/shared.module';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { ChangePwdComponent } from './change-pwd/change-pwd.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    RegisterComponent,
    ForgotPwdComponent,
    ChangePwdComponent
  ],
  providers: [
    AuthApi
  ],
})
export class AnonModule { }
