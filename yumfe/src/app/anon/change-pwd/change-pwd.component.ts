import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../remote';

import { SnackbarService } from '../../shared/snackbar.service';

@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.scss']
})
export class ChangePwdComponent implements OnInit {

  private sub: any;
  private secret: string;
  form: FormGroup;
  model: remote.ChangePwd={};
  loading = false;
  formErrors = {
    'password': '',
    'repeat': ''
  };

  validationMessages = {
    'password': {
      'required': 'Password is required.',
      'minlength': 'Password must be at least 6 characters long.'
    },
    'repeat': {
      'required': 'Confirmation is required.',
      'validateEqual': 'Password mismatch'
    }
  };


  constructor(
              private authService: remote.AuthApi,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              public snackBar: SnackbarService,
              private router: Router
             ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
       this.secret = params['secret'];
       this.model.secret = this.secret;
    });

    this.buildForm();

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  changePwd(){
    this.loading = true;
    this.model.newPassword = this.form.controls.password.value;
    this.authService.authChangepwdPut(this.model).subscribe(() => {
      this.loading = false;
      this.snackBar.openSuccess('Password changed!');
      this.router.navigate(['/login']);
    }, error => {
      this.loading = false;
      this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
    });
  }

   buildForm(): void {

    this.form = this.fb.group({
      'password': ['', [
        Validators.required,
        Validators.minLength(6)
      ]],

      'repeat': ''

    });

  }

  confirmPassword() {
    if (this.form.controls.repeat.value === '') {
      this.formErrors.repeat = '0';
    } else if (this.form.controls.repeat.value !== this.form.controls.password.value) {
      this.formErrors.repeat = '-1';
    } else {
      this.formErrors.repeat = '1';
    }
  }

  onBlurValueChanged(field: any) {
    if (!this.form) { return; }

    const checkForm = this.form;

    // clear previous error message (if any)
    this.formErrors[field] = '';
    const control = checkForm.get(field);

    if (control && control.dirty && !control.valid) {

      const messages = this.validationMessages[field];
      for (const key in control.errors) {

        this.formErrors[field] += messages[key] + ' ';

        // }
      }
    }
  }

}
