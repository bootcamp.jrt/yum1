import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { SnackbarService } from "../../shared/snackbar.service";
import { TermsAndPrivacyService } from '../../shared/terms-and-privacy/terms-and-privacy.service';

import * as remote from '../../remote';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  loading = false;

  userSave: remote.UserSave = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    role: "hungry",
    version: 0
  };

  errors = {
    email: 0,
    password: 0,
  }

  confirm = "";

  private regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
  + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:"
  + "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

  constructor(
    private router: Router,
    private authService: remote.AuthApi,
    private authenticationService: AuthenticationService,
    private snackbar: SnackbarService,
    public termsService: TermsAndPrivacyService

  ) { }

  ngOnInit() {

  }

  login(){
    this.router.navigate(['/login']);
  }

  confirmPassword() {
    if (this.confirm === '') {
      this.errors.password = 0;
    } else if (this.confirm !== this.userSave.password) {
      this.errors.password = -1;
    } else {
      this.errors.password = 1;
    }
  }

  confirmEmail() {
    if (this.userSave.email === '') {
      this.errors.email = 0;
    } else if (!this.userSave.email.match(this.regex)) {
      this.errors.email = -1;
    } else {
      this.errors.email = 1;
    }
  }

  register() {
    this.confirmEmail();
    if(this.errors.email < 1 || this.errors.password < 1){
      return;
    }

    this.authService.authRegisterPost(this.userSave)
      .subscribe(
      () => {

        let name = (this.userSave.firstName + ' ' + this.userSave.lastName).trim();

        this.authenticationService.logout();
        this.router.navigate(['/']);
        this.snackbar.openSuccess('User ' + name + ' has been created!');
      },
      error => {
        this.loading = false;
        this.snackbar.openError(JSON.parse(error._body).message);
      });
  }
}
