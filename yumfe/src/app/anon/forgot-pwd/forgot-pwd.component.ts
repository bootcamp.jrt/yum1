import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { Router } from "@angular/router";
import * as remote from '../../remote';

import { SnackbarService } from '../../shared/snackbar.service';

@Component({
  selector: 'app-forgot-pwd',
  templateUrl: './forgot-pwd.component.html',
  styleUrls: ['./forgot-pwd.component.scss']
})
export class ForgotPwdComponent implements OnInit {

  model: remote.ForgotPwd={};
  loading = false;
  check = false;
  mailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")"
  + "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:"
  + "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";


  constructor(private authService: remote.AuthApi, public snackBar: SnackbarService, public router: Router) { }

  ngOnInit() {
  }
  reset(){
    this.loading = true;
    this.authService.authForgotpwdPost(this.model).subscribe(() =>{
      this.loading = false;
      this.snackBar.openSuccess('Email sent!');
    }, error => {
      this.loading = false;
      this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
    });
  }

  validation(){
    this.check = true;
  }

  public backToLogin(){
    this.router.navigate(['/login']);
  }
}
