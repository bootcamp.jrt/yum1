import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { SnackbarService } from "../../shared/snackbar.service";

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: SnackbarService
  ) { }

  ngOnInit() {
    // reset login status
    //this.authenticationService.logout();
  }

  login() {

    if ((this.model.username === '' || this.model.username === undefined) && (this.model.password === '' || this.model.username === undefined)) {
      this.error = 'both';
      return;
    }

    if (this.model.username === '' || this.model.username === undefined) {
      this.error = 'username';
      return;
    }

    if (this.model.password === '' || this.model.password === undefined) {
      this.error = 'password';
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        if (result === true) {
          // login successful
          this.router.navigate(['/']);
        } else {
          // login failed
          this.error = 'Username or password is incorrect';
          this.loading = false;
          this.snackBar.openError(this.error);
        }
      }, error => {
        this.loading = false;
        this.snackBar.openError(error);
      });
  }

}

