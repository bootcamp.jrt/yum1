import { Injectable } from '@angular/core';
import {

  CanActivate, CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { AuthenticationService } from './shared/authentication.service';

@Injectable()
export class CanBrowse implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    var user = this.authenticationService.getLoggedInRole();
    if (user) {
      this.router.navigate(['/'+user ]);
      return false;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}


@Injectable()
export class CanActivateChef implements CanActivate, CanActivateChild  {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    var user = this.authenticationService.getLoggedInRole();
    if (user === 'admin' || user === 'chef') return true;

    this.router.navigate(['/']);
    return false;
  }
  canActivateChild() {
     return this.canActivate();
    }

}

@Injectable()
export class CanActivateAdmin implements CanActivate, CanActivateChild {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    var user = this.authenticationService.getLoggedInRole();
    if (user === 'admin') return true;

    this.router.navigate(['/']);
    return false;
  }
  canActivateChild() {
     return this.canActivate();
    }

}

@Injectable()
export class IsLoggedIn implements CanActivate, CanActivateChild {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    let logged = this.authenticationService.isLogged();
    if (logged) return true;

    this.router.navigate(['/']);
    return false;
  }
  canActivateChild() {
     return this.canActivate();
    }
}

@Injectable()
export class CanLogin implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {

    if (this.authenticationService.isLogged()) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }

}
