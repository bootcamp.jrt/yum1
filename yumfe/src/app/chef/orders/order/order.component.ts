import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FoodService } from '../../food.service';
import { GlobalSettingsService } from '../../../shared/globalSettings.service';
import * as remote from '../../../remote';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {

  @Input() order: remote.OrdersChefInner = {};

  mapOfFoods: Map<Number, remote.Food>;
  tot: number;

  constructor(private foodService: FoodService, public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {

    this.foodService.getFoodsListAndMap().subscribe(listAndMap => {
      this.mapOfFoods = listAndMap.map;
      this.tot = 0;

      for (const food of this.order.foods) {
        this.tot += this.mapOfFoods.get(food.id).price * food.quantity;
      }
    });
  }

  getFoodName(id: number): string {

    return this.mapOfFoods ? this.mapOfFoods.get(id).name : '';
  }

    getFoodType(id: number): string {

    return this.mapOfFoods ? this.mapOfFoods.get(id).type : '';
  }



}

