import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as remote from '../../remote';


@Component({
  selector: 'app-chef-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  loading = true;

  viewOrders: remote.OrdersChef;
  private year: number;
  private month: number;
  private sub: any;
  currentMonth = false;
  private existingOrders: Map<String, remote.OrdersChefInner> = new Map<String, remote.OrdersChefInner>();


  constructor(private chefService: remote.ChefApi,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {


    this.sub = this.route.params.subscribe(params => {
      if (params.year && params.month) {

        this.year  = +params['year'];
        this.month = +params['month'];

        let dateToCheck = new Date(); // now

        // current month
        if (this.year === dateToCheck.getFullYear() && this.month - 1 === (dateToCheck.getMonth())) {
          this.router.navigate(['/chef/viewOrders']);
        }

        this.chefService.ordersMonthlyYearMonthGet(this.month, this.year).subscribe(ordersList => {

          if (ordersList.length === 0) {
            // day with no orders
            this.initOrders(new Date(this.year, this.month - 1, 1));
          } else {
            for (const order of ordersList) {
              this.existingOrders.set(order.date.toDateString(), order);
            }

            this.initOrders(ordersList[0].date);
            this.populateGrid();

            this.loading = false;

          }
        }, error => console.log(error));
      }

      else{

        this.chefService.ordersMonthlyGet().subscribe(ordersList => {

          if (ordersList.length === 0) {
            // day with no orders
            let forGrid: Date = new Date();

            this.year = forGrid.getFullYear();
            this.month = forGrid.getMonth() + 1;

            this.initOrders(forGrid);

          }else {
            for (const order of ordersList) {
              this.existingOrders.set(order.date.toDateString(), order);
            }

            this.year = ordersList[0].date.getFullYear();
            this.month = ordersList[0].date.getMonth() + 1;

            this.initOrders(ordersList[0].date);
            this.populateGrid();
          }

          this.loading = false;

        }, error => console.log(error));
      }
    });
  }

  private initOrders(date: Date) {

    this.viewOrders = [];

    let dateOfGrid: Date = new Date(date.getFullYear(), date.getMonth(), 1);
    let displayMonth = dateOfGrid.getMonth();
    let day = dateOfGrid.getDay();

    let endDate: Date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let endDay = endDate.getDay();

    if (day === 0) {// Sunday
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }
    else if (day === 6) {// Saturday
      dateOfGrid.setDate(dateOfGrid.getDate() + 2);
    }
    else if (day === 1) {// Monday
      // do nothing
    }
    else {

      dateOfGrid.setDate(dateOfGrid.getDate() - (day - 1));
    }

    if (endDay === 0 || endDay > 5) {
      // do nothing
    } else {
      endDate.setDate(endDate.getDate() + (5 - endDay));
    }
    while (dateOfGrid <= endDate) {

      if (dateOfGrid.getDay() !== 0 && dateOfGrid.getDay() !== 6) {
        let empty2: remote.OrdersChefInner = {
          dailyMenuId: 0,
          date: new Date(dateOfGrid),
          foods: new Array<remote.OrdersChefInnerFoods>()
        };

        this.viewOrders.push(empty2);
      }
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }
  }

  private populateGrid() {

    for (let i = 0; i < this.viewOrders.length; i++) {
      let gridItem: remote.OrdersInner = this.viewOrders[i];

      let existing: remote.OrdersInner = this.existingOrders.get(gridItem.date.toDateString());

      if (existing) {

        this.viewOrders[i] = existing;
      }
    }
  }

  private nextYear(): String {
    if (!this.year) {
      return undefined;
    }
    if (this.month === 12) {
      return '' + (this.year + 1);
    }
    return this.year.toString();
  }

  private previousYear(): String {
    if (!this.year) {
      return undefined;
    }
    if (this.month === 1) {
      return '' + (this.year - 1);
    }
    return this.year.toString();

  }

  private nextMonth(): String {
    if (!this.month) {
      return undefined;
    }
    if (this.month === 12) {
      return '1';
    } else {
      return '' + (this.month + 1);
    }
  }

  private previousMonth(flag: number): String {
    if (!this.month) {
      return undefined;
    }
    if (this.month === 1) {
      return '12';
    } else {
      return '' + (this.month - 1);
    }
  }

   curMonth(order: remote.OrdersChefInner): boolean{
    if(order.date.getMonth() === this.month - 1){
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
