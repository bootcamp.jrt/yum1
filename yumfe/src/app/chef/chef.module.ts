import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FoodEditComponent } from './home/food-edit/food-edit.component';
import { FoodComponent } from './home/food/food.component';
import { HomeComponent } from './home/home.component';
import { ChefRouting } from './chef.routing';
import { ChefApi } from '../remote';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { FailDeleteFoodComponent } from './home/food/food.component';
import { DeleteFoodComponent } from './home/food/food.component';
import { EditFoodComponent } from './home/food-edit/food-edit.component';
import { OrdersDayComponent } from './orders-day/orders-day.component';
import { FooditemComponent } from './orders-day/fooditem/fooditem.component';
import { UserorderComponent } from './orders-day/userorder/userorder.component';
import { FoodService } from './food.service';

import { MenusComponent } from './menus/menus.component';
import { DailymenuComponent } from './menus/dailymenu/dailymenu.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';
import { ChefNavBarComponent } from './chef-nav-bar/chef-nav-bar.component';

@NgModule({
  imports: [
    CommonModule,
    ChefRouting,
    SharedModule,
    FormsModule
  ],
  declarations: [
    FoodEditComponent,
    FoodComponent,
    HomeComponent,
    FailDeleteFoodComponent,
    DeleteFoodComponent,
    EditFoodComponent,
    MenusComponent,
    DailymenuComponent,
    OrdersComponent,
    OrderComponent,
    OrdersDayComponent,
    FooditemComponent,
    UserorderComponent,
    ChefNavBarComponent
  ],
  providers: [
    ChefApi,
    FoodService
  ],
  entryComponents: [
    FailDeleteFoodComponent,
    DeleteFoodComponent,
    EditFoodComponent
  ]
})
export class ChefModule { }
