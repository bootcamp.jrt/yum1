import { Injectable } from '@angular/core';
import {
  Routes,
  RouterModule,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { LoggedComponent } from '../shared/logged/logged.component';
import { AuthenticationService } from '../shared/authentication.service';
import { Observable } from "rxjs/Rx";
import { HomeComponent } from './home/home.component';

import { MenusComponent } from './menus/menus.component';
import { OrdersComponent } from './orders/orders.component';
import { OrdersDayComponent } from './orders-day/orders-day.component';
import {  CanActivateChef } from '../can-browse.service';
const chefRoutes: Routes = [
  {
    path: 'chef',
    component: LoggedComponent,
    canActivate: [ CanActivateChef ],
     canActivateChild: [CanActivateChef],
    children: [
      { path: '', component: HomeComponent },
      { path: 'menus', component: MenusComponent },
      { path: 'menus/:year/:month', component: MenusComponent },
      { path: 'viewOrders', component: OrdersComponent },
      { path: 'viewOrders/:year/:month', component: OrdersComponent },
      { path: 'orders/day/:menuid', component: OrdersDayComponent }
    ],
  //  canActivateChild: [CanActivateChef],
  }
];
export const ChefRouting = RouterModule.forChild(chefRoutes);
