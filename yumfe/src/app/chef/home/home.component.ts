import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialog, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../remote';

import { SnackbarService } from '../../shared/snackbar.service';

@Component({
  selector: 'app-chef-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  loadingSpinner = true;

  foods: remote.FoodsList;
  foodSave: remote.FoodSave = {};
  newFoodForm: FormGroup;
  name: string;
  foodExists: boolean;

  sort: string;
  order: string;

  getArchived: boolean;
  filter: string;
  sorting: string;
  pageSize: number;
  currentPage: number;
  totalItems: number;

  loading = false;

  presentingFoods = 0;

  selectFoodTypesCreateDropdown = [
    { value: 'mainDish', viewValue: 'Main Dish' },
    { value: 'salad', viewValue: 'Salad' },
    { value: 'drink', viewValue: 'Drink' }
  ];

  selectFoodTypes = [
    { value: 'all', viewValue: 'All' },
    { value: 'mainDish', viewValue: 'Main Dish' },
    { value: 'salad', viewValue: 'Salad' },
    { value: 'drink', viewValue: 'Drink' }
  ];

  popularity = [
    { value: 'id', viewValue: 'All' },
    { value: 'popularityHigh', viewValue: 'Most Popular food' },
    { value: 'popularityLow', viewValue: 'Least Popular food' },
    { value: 'priceHigh', viewValue: 'Highest Price' },
    { value: 'priceLow', viewValue: 'Lower Price' },
  ];

  numberOfItems = [
    10,
    20,
    50,
    100
  ]

  constructor(public dialog: MdDialog, private formBuild: FormBuilder, private chefService: remote.ChefApi, public snackBar: SnackbarService) { }

  ngOnInit() {
    this.getListOfFoods();
    this.createFood();
    this.resetPages();
  }

  sortingOrderer() {
    if (this.sorting === 'popularityHigh') {
      this.sort = 'popularity';
      this.order = 'desc';
    } else if (this.sorting === 'popularityLow') {
      this.sort = 'popularity';
      this.order = 'asc';
    } else if (this.sorting === 'priceHigh') {
      this.sort = 'price';
      this.order = 'desc';
    } else if (this.sorting === 'priceLow') {
      this.sort = 'price';
      this.order = 'asc';
    } else {
      this.sort = 'id';
      this.order = 'desc';
    }
  }

  checkArchived() {
    this.getArchived = !this.getArchived;
    this.fetchPages();
  }

  resetPages(){
      this.getArchived = false;
      this.filter = 'all';
      this.sorting = 'id';
      this.pageSize = 10;
      this.fetchPages();
  }

  fetchPages() {
    this.loadingSpinner = true;
    this.sortingOrderer();
    this.currentPage = 1;
    this.chefService.foodsGet(undefined, this.getArchived, this.filter, this.sort, this.order, this.currentPage - 1, this.pageSize)
      .subscribe(foodList => {
        this.totalItems = foodList.totalItems;
        this.foods = foodList.foods;
        this.loadingSpinner = false;
        this.presentingFoods = foodList.foods.length;
      }, error => console.log(error));
  }

  changePage($event){
    this.currentPage = $event;
    this.chefService.foodsGet(undefined, this.getArchived, this.filter, this.sort, this.order, this.currentPage - 1, this.pageSize)
      .subscribe(foodList => {
        this.totalItems = foodList.totalItems;
        this.foods = foodList.foods;
      }, error => console.log(error));
  }

  checkName() {
    this.foodExists = false;
    if (this.name) {
      this.chefService.foodsFindByNameNameGet(this.name).subscribe(food => {
        this.foodExists = false;
      }, error => {
        this.foodExists = true;
        console.log(error);
      });
    }
  }

  // gets all foods (archived and not) from foods service
  getListOfFoods() {
    this.chefService.foodsGet().subscribe(foodList => {
      this.foods = foodList.foods;
    }, error => console.log(error));
  }

  createFood() {
    this.newFoodForm = this.formBuild.group({
      name: ['', Validators.required],
      type: [''],
      description: [''],
      price: ['', Validators.required]
    });
  }

  submit() {

     this.loading = true;

     this.foodSave = this.newFoodForm.value;
     this.chefService.foodsPost(this.foodSave).subscribe(() => {
      this.snackBar.openSuccess('FOOD CREATED');
    },
      error => {
        this.loading = false;
        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
      },
      () => {
        this.loading = false;
        this.resetPages();
      });

      this.newFoodForm.reset();
  }
}
