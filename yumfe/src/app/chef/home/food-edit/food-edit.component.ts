import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../../remote';

import { SnackbarService } from '../../../shared/snackbar.service';

@Component({
  selector: 'app-chef-home-food-edit',
  templateUrl: './food-edit.component.html',
  styleUrls: ['./food-edit.component.scss']
})
export class FoodEditComponent implements OnInit {

  @Input() foodToEdit: remote.EditedFood = {};
  @Output() cancelEdit = new EventEmitter();
  @Output() successEdit = new EventEmitter();

  id: number;
  version: number;
  clone = false;

  foodTypes = [ //TODO: use foodTypes like the homepage
    'mainDish',
    'salad',
    'drink'
  ];

  editedFoodForm: FormGroup;

  constructor(public dialog: MdDialog, private formBuild: FormBuilder, private chefService: remote.ChefApi, public snackBar: SnackbarService) { }

  ngOnInit() {
    this.editFood();

  }

  completedForm() {
    this.editedFoodForm = this.formBuild.group({
      name: [this.foodToEdit.name],
      type: [this.foodToEdit.type],
      description: [this.foodToEdit.description],
      price: [this.foodToEdit.price],
    });

  }

  isSame(): boolean {
    const rowValues = this.editedFoodForm.getRawValue();
    return this.foodToEdit.description === rowValues.description &&
           this.foodToEdit.name === rowValues.name &&
           '' + this.foodToEdit.price === '' + rowValues.price &&
           this.foodToEdit.type === rowValues.type;
  }

  editFood() {

    this.completedForm();

    this.chefService.foodsIdGet(this.foodToEdit.id, true).subscribe(foodEditable => {
      this.foodToEdit.description = foodEditable.editedFood.description;
      this.foodToEdit.name = foodEditable.editedFood.name;
      this.foodToEdit.price = foodEditable.editedFood.price;
      this.foodToEdit.type = foodEditable.editedFood.type;
      this.foodToEdit.version = foodEditable.editedFood.version;
      this.id = this.foodToEdit.id;
      this.version = this.foodToEdit.version;

      if (foodEditable.editable === false) {
        this.basicModal();
      }
    },
      error => {
        console.log(error);
        if (error.status === 404) {
          this.snackBar.openError('You deleted this food item earlier');
          this.successEdit.emit();
        }
      }
    );
    this.clone = false;
  }

  cancel() {
    this.cancelEdit.emit();
  }

  submit() {
    this.foodToEdit = this.editedFoodForm.getRawValue();
    this.foodToEdit.id = this.id;
    this.foodToEdit.version = this.version;

    this.chefService.foodsIdPut(this.foodToEdit.id, this.foodToEdit, this.clone).subscribe(newEntry => {
      this.foodToEdit = newEntry.id;
      this.foodToEdit = newEntry.version;
      this.snackBar.openSuccess('FOOD SUCCESSFULLY EDITED');
      this.successEdit.emit();
    },
      error => {
        console.log(error);
        this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
        this.successEdit.emit();
      });
  }

  basicModal() {
    const dialogRef = this.dialog.open(EditFoodComponent);
    dialogRef.afterClosed().subscribe(result => {

      if (result === 'cancel') {
        this.editedFoodForm.get('name').disable();
        this.editedFoodForm.get('price').disable();
        this.editedFoodForm.get('type').disable();
      } else if (result === 'quit') {
        this.cancelEdit.emit();
      } else {
        this.clone = true;
      }
    });
  }
}

@Component({
  templateUrl: './edit-dialog.html',
})
export class EditFoodComponent {
  public reason: string;
  public errorCode: Number;
  constructor(public dialogRef: MdDialogRef<EditFoodComponent>) { }
}
