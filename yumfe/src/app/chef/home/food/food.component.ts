import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { GlobalSettingsService } from "../../../shared/globalSettings.service";
import * as remote from '../../../remote';

import { SnackbarService } from '../../../shared/snackbar.service';

@Component({
  selector: 'app-chef-home-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})


export class FoodComponent implements OnInit {

  @Input()
  food: remote.Food;
  @Input()
  archivedMode: boolean;
  @Output()
  edit: EventEmitter<void> = new EventEmitter<void>();
  @Output()
  successEditEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output()
  delete: EventEmitter<void> = new EventEmitter<void>();


  showForm = false;

  constructor(
    public dialog: MdDialog,
    private chefService: remote.ChefApi,
    public snackBar: SnackbarService,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() { }

  enableEdit() {
    this.showForm = true;
    this.edit.emit();
  }

  handleCancelEdit() {
    this.showForm = false;
  }

  handleSuccessEdit() {
    this.showForm = false;
    this.successEditEvent.emit();
  }

  deleteFood() {
    const dialogRef = this.dialog.open(DeleteFoodComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'OK') {
        this.basicModal();
      }
    });

  }

  basicModal() {
    this.chefService.foodsIdDelete(this.food.id).subscribe(foodDelete => {
      this.delete.emit();
      this.snackBar.openSuccess('FOOD deleted');
    },
      error => {
        console.log(error);
        if (error.status === 406) {
          this.openDialogDelete(error.json().message, error.status);
        } else {
          this.snackBar.openError('You deleted this food item earlier.');
          this.delete.emit();
        }
      });
  }

  openDialogDelete(reason: string, code: number) {
    const dialogRef = this.dialog.open(FailDeleteFoodComponent);
    dialogRef.componentInstance.reason = reason;
    dialogRef.componentInstance.errorCode = code;

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'OK') {
        this.chefService.foodsIdDelete(this.food.id, true).subscribe(foodDelete => {
          this.snackBar.openSuccess('FOOD archived');
          this.delete.emit();
        },
          error => {
            console.log(error);
             this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
          });
      }
    });
  }
}

@Component({
  templateUrl: './delete-food-fail-dialog.html',
})
export class FailDeleteFoodComponent {
  public reason: string;
  public errorCode: Number;
  constructor(public dialogRef: MdDialogRef<FailDeleteFoodComponent>) { }
}

@Component({
  templateUrl: './success-food-dialog.html',
})
export class DeleteFoodComponent {
  public reason: string;
  public errorCode: Number;
  constructor(public dialogRef: MdDialogRef<DeleteFoodComponent>) { }
}
