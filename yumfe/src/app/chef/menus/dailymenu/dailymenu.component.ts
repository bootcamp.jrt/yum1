import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as remote from '../../../remote';
import { FoodService } from '../../food.service';
import { Observable } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { SnackbarService } from '../../../shared/snackbar.service';
import { GlobalSettingsService } from '../../../shared/globalSettings.service';

@Component({
  selector: 'app-dailymenu',
  templateUrl: './dailymenu.component.html',
  styleUrls: ['./dailymenu.component.scss']
})
export class DailymenuComponent implements OnInit {

  @Input() menu: remote.DailyMenuSave;
  @Input() isInMonth: boolean;

  deadline: Date ;

  foodsToAdd: Array<remote.Food> = [];
  ids: Array<number> = [];
  selected: number;
  listOfFoods: Array<remote.Food> = [];
  mapOfFoods: Map<Number, remote.Food>;
  private initialState  =  {
              foods: new Array<remote.DailyMenuSaveFoods>(),
              ids: new Array<number>()
            };


  constructor( private foodservice: FoodService,
               private chefService: remote.ChefApi,
               public snackBar: SnackbarService,
               private settings: GlobalSettingsService ) { }

  ngOnInit() {


    this.foodservice.getFoodsListAndMap().subscribe( listAndMap =>{
      this.listOfFoods = listAndMap.list;
      this.mapOfFoods = listAndMap.map;
      this.getFoodsForMenu();
    } );

      this.setInitialState();

      this.settings.getDeadLine().subscribe(deadline =>{
        let d = new Date(deadline);
        this.deadline = d;
      });

  }

  getFoodsForMenu(){

      this.foodsToAdd = new Array<remote.Food>();

        for (let food of this.listOfFoods){

          if ( this.ids.indexOf(food.id) < 0 ){
            this.foodsToAdd.push(food);
          }
        }
  }

  getFoodName(id: number): string {
     return this.mapOfFoods ? this.mapOfFoods.get(id).name : '';
  }

  getFoodType(id: number): string {
     return this.mapOfFoods ? this.mapOfFoods.get(id).type : '';
  }

  cancelChanges(){
      this.revertToInitialState();
  }

  datePassed(): boolean {
    if(this.deadline){
        let check: Date = this.deadline;

      if(new Date() > check){
        check.setDate(check.getDate() +1);
      }

      if (this.menu.date < check){
        return true;
      }
        return false;
    }

  }


  submitChanges(){

    if(this.menu.foods.length === 0 && this.menu.id === 0){
      this.snackBar.openError("Cannot create en empty menu, please add foods");
       this.revertToInitialState();
    }
    else if( JSON.stringify(this.menu.foods) === JSON.stringify(this.initialState.foods) ){
      this.snackBar.openError("No changes detected in the menu");
    }
    else{
      this.ids = [];

      let updatedFoods: Array<remote.DailyMenuSaveFoods> = [];

      this.menu.foods.forEach((x) => {
          this.ids.push(x.id);

          const copy: remote.DailyMenuSaveFoods = {
            id: x.id,
            isOrdered: x.isOrdered
          };

          updatedFoods.push(copy);

      });

      let updated: remote.DailyMenuSave = {
        id: this.menu.id ,
        date: this.menu.date,
        version: this.menu.version,
        foods: updatedFoods
      }

      if ( this.menu.id === 0 ){

          this.chefService.dailyMenusPost(updated).subscribe( newEntry => {
            this.menu.id = newEntry.id;
            this.menu.version = newEntry.version;
            this.setInitialState();
            this.snackBar.openSuccess("Menu succesfully created!");
          }, error => {
            if ( error.status === 409 ) {
                this.snackBar.openError(error.json().message);
                this.menu = error.json().dto;
                this.menu.date = new Date(this.menu.date);
                this.setInitialState();
              }else{
                this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
                this.revertToInitialState();
              }
          } );
      }
      else{

          let backupDate: Date  = this.menu.date;

          this.chefService.dailyMenusIdPut(this.menu.id, updated).subscribe( updatedMenu => {
          this.menu = updatedMenu;
          if (this.menu.id === 0){//deleted
                 this.menu.date = backupDate;
                 this.snackBar.openSuccess("Menu succesfully deleted!");
                }
          else{
            this.snackBar.openSuccess("Menu succesfully updated!");
          }
          this.setInitialState();
      }, error => {
              console.log(error);
              if ( error.status == 409 || error.status == 410) {

                this.menu = error.json().dto;
                if (this.menu.id === 0){//deleted
                 this.menu.date = backupDate;
                }
                else{
                  this.menu.date = new Date(this.menu.date);
                }
                this.setInitialState();
                this.snackBar.openError(error.json().message);
              }else{
                this.snackBar.openError(error.json().message ? error.json().message : 'Something went wrong, try again later.');
                this.revertToInitialState();
              }
          } );

      }
    }
  }

  removeFood(food: remote.DailyMenuSaveFoods){

    this.menu.foods.splice(this.menu.foods.indexOf(food), 1);
    this.ids.splice(this.ids.indexOf(food.id), 1);
    this.selected = 0;

    this.getFoodsForMenu();
  }

  dropdownChange(){
    this.ids.push(this.selected);

    let added: remote.DailyMenuSaveFoods = {
            id: this.selected,
            isOrdered: false
            } ;

    this.menu.foods.push(added);
    this.getFoodsForMenu();

  }

  private setInitialState(){

      this.ids = [];
      this.initialState.foods = [];
      this.initialState.ids = [];

      this.menu.foods.forEach((x) => {
        this.ids.push(x.id);

        const copy: remote.DailyMenuSaveFoods = {
          id: x.id,
          isOrdered: x.isOrdered
        };

        this.initialState.ids.push(copy.id);
        this.initialState.foods.push(copy);

    });
  }

  private revertToInitialState(){

      this.ids = [];
      this.menu.foods = [];

      this.initialState.foods.forEach((x) => {

        const copy: remote.DailyMenuSaveFoods = {
          id: x.id,
          isOrdered: x.isOrdered
        };

        this.ids.push(copy.id);
        this.menu.foods.push(copy);

    });

    this.selected = 0;

    this.getFoodsForMenu();
  }

}