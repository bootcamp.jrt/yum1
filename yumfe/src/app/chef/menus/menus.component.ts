import { Component, OnInit } from '@angular/core';
import * as remote from '../../remote';
import { FoodService } from '../food.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {

  loading = true;

  menus: remote.DailyMenusChef = [];
  private year: number;
  private month: number;
  private existingMenus: Map<String, remote.DailyMenuSave> = new Map<String, remote.DailyMenuSave>();
  private sub: any;
  cancel: Array<boolean> = [];


  constructor( private chefApi: remote.ChefApi,
               private chefService: FoodService,
               private route: ActivatedRoute,
               private router: Router,
              ) { }

  ngOnInit() {



    this.sub = this.route.params.subscribe(params => {

      if (params.year && params.month) {

        this.year = +params['year'];
        this.month = +params['month'];

        let dateToCheck = new Date();
        if (this.year === dateToCheck.getFullYear() && this.month - 1 === (dateToCheck.getMonth())) {
          this.router.navigate(['/chef/menus']);
        }

        this.chefApi.dailyMenusMonthlyYearMonthGet(this.month, this.year).subscribe(dailyMenus => {

          if (dailyMenus.length === 0) {
            this.initHistory(new Date(this.year, this.month - 1, 1));
          }
          else {
            for (const dailyMenu of dailyMenus) {
              this.existingMenus.set(dailyMenu.date.toDateString(), dailyMenu);
            }

            this.initHistory(dailyMenus[0].date);
            this.populateGrid();
          }

          this.loading = false;

        }, error => console.log(error));
      }
      else {

        this.chefApi.dailyMenusMonthlyGet().subscribe(dailyMenus => {

          if (dailyMenus.length === 0) {
            let forGrid: Date = new Date();

            this.year = forGrid.getFullYear();
            this.month = forGrid.getMonth() + 1;

            this.initHistory(forGrid);
          }
          else {

            for (const dailyMenu of dailyMenus) {
              this.existingMenus.set(dailyMenu.date.toDateString(), dailyMenu);
            }

            this.year = dailyMenus[0].date.getFullYear();
            this.month = dailyMenus[0].date.getMonth() + 1;

            this.initHistory(dailyMenus[0].date);
            this.populateGrid();

          }

          this.loading = false;

          // this.menus = dailyMenus;
        }, error => console.log(error));
      }

    });



  }

  private initHistory(date: Date) {

    this.menus = [];

    let dateOfGrid: Date = new Date(date.getFullYear(), date.getMonth(), 1);
    let displayMonth = dateOfGrid.getMonth();
    let day = dateOfGrid.getDay();

    let endDate: Date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let endDay = endDate.getDay();


    if (day == 0) {//Sunday
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }
    else if (day == 6) {//Saturday
      dateOfGrid.setDate(dateOfGrid.getDate() + 2);
    }
    else if (day == 1) {//Monday
      //do nothing
    }
    else {
      dateOfGrid.setDate(dateOfGrid.getDate() - (day - 1));
    }

    if (endDay === 0 || endDay > 5) {
      //do nothing
    }
    else {
      endDate.setDate(endDate.getDate() + (5 - endDay));
    }


    while (dateOfGrid <= endDate) {

      if (dateOfGrid.getDay() !== 0 && dateOfGrid.getDay() !== 6) {

        let empty2: remote.DailyMenuSave = {
          id: 0,
          date: new Date(dateOfGrid),
          version: -1,
          foods: new Array<remote.DailyMenuSaveFoods>()
        };

        this.menus.push(empty2);
      }
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }
  }


  private populateGrid() {

    for (let i = 0; i < this.menus.length; i++) {
      let gridItem: remote.DailyMenuSave = this.menus[i];

      let existing: remote.DailyMenuSave = this.existingMenus.get(gridItem.date.toDateString());

      if (existing) {

        this.menus[i] = existing;
      }
      this.cancel[i] = false;

    }

  }

  currentMonth(menu: remote.DailyMenuSave): boolean {
    if (menu.date.getMonth() === this.month - 1) {
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private nextYear(): String {

    if (!this.year)
      return undefined;

    if (this.month === 12) {
      return '' + (this.year + 1);
    }

    return this.year.toString();


  }


  private previousYear(): String {


    if (!this.year)
      return undefined;


    if (this.month === 1) {
      return '' + (this.year - 1);
    }

    return this.year.toString();

  }


  private nextMonth(): String {
    if (!this.month) {
      return undefined;
    }


    if (this.month === 12) {
      return "1";
    }
    else {
      return "" + (this.month + 1);
    }

  }

  private previousMonth(flag: number): String {

    if (!this.month) {
      return undefined;
    }

    if (this.month === 1) {
      return "12";
    }
    else {
      return '' + (this.month - 1);
    }

  }

}