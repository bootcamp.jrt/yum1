import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefNavBarComponent } from './chef-nav-bar.component';

describe('ChefNavBarComponent', () => {
  let component: ChefNavBarComponent;
  let fixture: ComponentFixture<ChefNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
