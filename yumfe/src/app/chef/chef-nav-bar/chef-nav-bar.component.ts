import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chef-nav-bar',
  templateUrl: './chef-nav-bar.component.html',
  styleUrls: ['./chef-nav-bar.component.scss']
})
export class ChefNavBarComponent implements OnInit {

  date: Date;
  private year: number = 0;
  private month: number = 0;
  private sub: any;

  constructor( private route: ActivatedRoute,
               private router: Router) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

        if (params.year && params.month) {

            this.year =  +params['year'];
            this.month = +params['month'];

            this.date = new Date(this.year, this.month - 1, 1);

            let dateToCheck = new Date();
            if(this.year === dateToCheck.getFullYear() && this.month - 1 === (dateToCheck.getMonth()) ){
              if (this.router.url.search('menus') === -1) {//chef at viewOrders
                  this.router.navigate(['/chef/viewOrders']);
              }
              else{//chef at menus
                  this.router.navigate(['/chef/menus']);
              }

            }
        }
        else{
          this.date = new Date();
          this.year = this.date.getFullYear();
          this.month = this.date.getMonth() + 1;
        }
    }, error => console.log(error));

  }

  previous(){

    if (this.router.url.search('menus') === -1) {//chef at viewOrders
      this.router.navigate(['/chef/viewOrders/'  + this.previousYear() + '/' + this.previousMonth()]);
    }
    else{//chef at menus
      this.router.navigate(['/chef/menus/' + this.previousYear() + '/' + this.previousMonth()]);
    }

  }

  next(){

    if (this.router.url.search('menus') === -1) {//chef at viewOrders
      this.router.navigate(['/chef/viewOrders/' + this.nextYear() + '/' + this.nextMonth()]);
    }
    else{//chef at menus
      this.router.navigate(['/chef/menus/' + this.nextYear() + '/' + this.nextMonth()]);
    }

  }


  private nextYear() : String {

    if(!this.year)
      return undefined;

    if(this.month === 12){
     return ''+( this.year + 1);
    }

    return this.year.toString();


  }


  private previousYear() : String {


    if(!this.year)
      return undefined;


    if(this.month === 1){
      return ''+( this.year - 1);
    }

    return this.year.toString();

  }


  private  nextMonth(): String{
      if(!this.month){
      return undefined;
    }


    if(this.month === 12){
      return "1";
    }
    else{
      return ""+(this.month + 1);
    }

  }

  private previousMonth() : String {

    if(!this.month){
      return undefined;
    }

    if(this.month === 1){
      return "12";
    }
    else{
      return ''+( this.month - 1);
    }

  }

}
