import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import * as remote from '../remote';

@Injectable()
export class FoodService {
  private foodsMap: Map<Number, remote.Food> = new Map<Number, remote.Food>();
  private foodsList: Array<remote.Food> = [];

  private observable;
  private version: number = -1;

  constructor(private chefService: remote.ChefApi) { }

  public getFoodDetailsById(id: number): Observable<remote.Food> {
    return new Observable(observer => {
      this.getMapOfFoods().subscribe(foodsMap => {

        let food = foodsMap.get(id);
        if (food !== undefined) {
          observer.next(food);
          observer.complete();
        } else {
          observer.error('food id not found');
        }

      });
    });
  }

  public getFoodNameById(id: number): Observable<string> {
    return new Observable(observer => {
      this.getMapOfFoods().subscribe(foodsMap => {

        let food = foodsMap.get(id);
        if (food !== undefined) {
          observer.next(food.name);
          observer.complete();
        } else {
          observer.error('food id not found');
        }

      });
    });
  }

  //not used may be deleted at last code review
  //functionality moved to component for perfomance reasons
  public getFoodsForMenu(ids: Array<number>): Observable<Array<remote.Food>>{
    return new Observable(observer => {
      this.getMapOfFoods().subscribe(foodsMap => {

        let list: Array<remote.Food> = new Array<remote.Food>();

        for (let food of this.foodsList){

          if ( ids.indexOf(food.id) < 0 ){
            list.push(food);
          }
        }


        observer.next(list);
        observer.complete();

      });
    });
  }

  public getFoodDetailsByIdList(ids: Array<number>): Observable<Array<remote.Food>> {
    return new Observable(observer => {
      this.getMapOfFoods().subscribe(foodsMap => {

        let list: Array<remote.Food> = new Array<remote.Food>();

        for (let id of ids) {
          let food = foodsMap.get(id);
          if (food !== undefined) {
            list.push(food);
          }
        }

        observer.next(list);
        observer.complete();

      });
    });
  }

  //not used may be deleted at last code review
  //functionality moved to component for perfomance reasons
  public getFoodsList(): Observable<Array<remote.Food>>{

      return new Observable(observer => {
        this.getMapOfFoods().subscribe( foodsMap => {
            observer.next(this.foodsList);
            observer.complete();
        });
      })

  }

  public getFoodsListAndMap(): Observable<{list:Array<remote.Food>,map:Map<Number, remote.Food>}>{

      return new Observable(observer => {
        this.getMapOfFoods().subscribe( foodsMap => {
            observer.next({list:this.foodsList,map:foodsMap});
            observer.complete();
        });
      })

  }


  private getMapOfFoods(): Observable<Map<Number, remote.Food>> {


    if (this.observable === undefined) {
      this.observable = Observable.defer(() => {

            return new Observable(observer => {
            this.chefService.foodsGet(this.version).subscribe(foods => {
              this.version = foods.version;
              this.foodsList = foods.foods;
              for (let f of foods.foods) {
                this.foodsMap.set(f.id, f);
              }

              observer.next(this.foodsMap);
              observer.complete();

            }, error => {

              if (error.status == 304) {
                observer.next(this.foodsMap);
                observer.complete();
              } else {
                observer.error(error);
              }

            });
          });

        })
      .publishReplay(1, 2000)
      .refCount()
      .take(1);
    }
    return this.observable;
  }
}