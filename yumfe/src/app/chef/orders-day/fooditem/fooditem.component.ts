import { Component, OnInit, Input } from '@angular/core';
import { FoodService } from '../../food.service';
import * as remote from '../../../remote';
import { GlobalSettingsService } from "app/shared/globalSettings.service";

@Component({
  selector: 'app-fooditem',
  templateUrl: './fooditem.component.html',
  styleUrls: ['./fooditem.component.scss']
})
export class FooditemComponent implements OnInit {

  @Input()
  food


  constructor(private foodService: FoodService, public globalSettingsService: GlobalSettingsService ) { }

  ngOnInit() {

  }


}


