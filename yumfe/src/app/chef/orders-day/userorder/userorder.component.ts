import { Component, OnInit, Input } from '@angular/core';
import { GlobalSettingsService } from "../../../shared/globalSettings.service";

@Component({
  selector: 'app-userorder',
  templateUrl: './userorder.component.html',
  styleUrls: ['./userorder.component.scss']
})
export class UserorderComponent implements OnInit {

  @Input()
  order;


  constructor( public gs: GlobalSettingsService) { }

  ngOnInit() {
  }

}
