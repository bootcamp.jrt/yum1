

import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FoodService } from '../food.service';
import * as remote from '../../remote';
import { GlobalSettingsService } from "app/shared/globalSettings.service";

@Component({
  selector: 'app-orders-day',
  templateUrl: './orders-day.component.html',
  styleUrls: ['./orders-day.component.scss']
})
export class OrdersDayComponent implements OnInit, OnDestroy {

  loading = true;

  summary: remote.OrderSummary = {};
  private sub: any;
  private menuId: number;

  public date;

  foodsDetailed: Array<remote.Food>;

  foodItemsComplete;
  ordersComplete = [];

  foodItemsTotalPrice;

  orders: Array<remote.OrderSummaryDailyOrders>;
  foods: Array<remote.OrdersChefInnerFoods>;
  foodsByType = {
    'mainDish': [],
    'drink': [],
    'salad': []
  };



  constructor(private foodService: FoodService,
    private chefService: remote.ChefApi,
    private route: ActivatedRoute,
    public gs: GlobalSettingsService
  ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.menuId = +params['menuid']; // (+) converts string 'id' to a number
    });

    this.getFoodsList();

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getFoodsList() {
    this.chefService.ordersDailyDailyMenuIdGet(this.menuId).subscribe(sum => {
      this.summary = sum;
      this.foods = this.summary.foods;
      this.date = this.summary.date;
      this.orders = this.summary.dailyOrders;


      this.populateUsingService();

    }, error => console.log(error));
  }

  populateUsingService() {
    let ids = [];

    for (let food of this.foods) {
      ids.push(food.id);
    }

    this.foodService.getFoodDetailsByIdList(ids).subscribe(foods => {
      this.foodsDetailed = foods;

      this.arrangeFoodsByType(this.foodsDetailed);

      let listAndTotal = this.addQuantitiesAndCalculateTotal(this.foods);
      this.foodItemsComplete = listAndTotal.list;
      this.foodItemsTotalPrice = listAndTotal.total;


      for (let order of this.orders) {
        let listAndTotal = this.addQuantitiesAndCalculateTotal(order.foods);
        let myOrder = {
          'surname': '',
          'orderItems': [],
          'total': 0
        };
        myOrder.surname = order.lastName;
        myOrder.orderItems = listAndTotal.list;
        myOrder.total = listAndTotal.total;
        this.ordersComplete.push(myOrder);
      }

      this.loading = false;
    });
  }

  arrangeFoodsByType(foods) {
    for (let food of foods) {

      this.foodsByType[food.type].push(food);

    }
  }

  addQuantitiesAndCalculateTotal(foods): any {

    let itemsByType = {
      'mainDish': [],
      'drink': [],
      'salad': []
    };

    for (let item of this.foodsByType.mainDish) {
      const newItem: remote.Food = {
        id: item.id,
        description: item.description,
        name: item.name,
        type: item.type,
        price: item.price
      }
      itemsByType.mainDish.push(newItem);
    }

    for (let item of this.foodsByType.drink) {
      const newItem: remote.Food = {
        id: item.id,
        description: item.description,
        name: item.name,
        type: item.type,
        price: item.price
      }
      itemsByType.drink.push(newItem);
    }

    for (let item of this.foodsByType.salad) {
      const newItem: remote.Food = {
        id: item.id,
        description: item.description,
        name: item.name,
        type: item.type,
        price: item.price
      }
      itemsByType.salad.push(newItem);
    }

    // let itemsByType = this.foodsByType;
    let totalPrice = 0;

    for (let food of foods) {

      for (let item of itemsByType.mainDish) {
        if (item.id === food.id) {

          item.quantity = food.quantity;
          totalPrice += item.price * item.quantity;

        }
      }
      for (let item of itemsByType.drink) {
        if (item.id === food.id) {

          item.quantity = food.quantity;
          totalPrice += item.price * item.quantity;

        }
      }
      for (let item of itemsByType.salad) {
        if (item.id === food.id) {

          item.quantity = food.quantity;
          totalPrice += item.price * item.quantity;

        }
      }
    }

    return {
      list: itemsByType,
      total: totalPrice
    };

  }

  public print = (): void => {
    let printContents, popupWin;
    printContents = document.getElementById('container-to-print').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=1260px');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Daily Order Summary</title>
          <style>

          .no-print {
            display: none;
          }
          app-fooditem, app-userorder {
            display: block;
          }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


}


