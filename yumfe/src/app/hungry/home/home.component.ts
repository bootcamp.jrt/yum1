import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as remote from '../../remote';

import { DailyOrder } from '../home/daily-order-form/daily-order';
import { GlobalSettingsService } from "../../shared/globalSettings.service";

@Component({
  selector: 'app-hungry-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading = true;

  weekMenus: Array<DailyOrder> = new Array<DailyOrder>();

  weekTotal: number = 0;

  private week: number;
  private year: number;
  public weekDate: Date;

  constructor(
    private hungryService: remote.HungryApi,
    private route: ActivatedRoute,
    private router: Router,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {

      this.year = +params['year'];
      this.week = +params['week'];

      if (this.year && this.week) {

        this.weekDate = this.getMonday();
        this.weekDate.setDate(this.weekDate.getDate() + 2);
        this.hungryService.menusWeeklyYearWeekGet(this.week, this.year).subscribe(menus => this.getMenus(menus), error => console.log(error));

      } else {

        this.weekDate = new Date();
        this.weekDate.setHours(0, 0, 0, 0);
        this.year = this.weekDate.getFullYear();
        this.week = this.getCurrentWeek(this.weekDate);

        this.hungryService.menusWeeklyGet().subscribe(menus => this.getMenus(menus), error => console.log(error));
      }

    });
  }

  nextWeek() {
    let w = this.week === this.getTotalWeeks(this.year) ? 1 : this.week + 1;
    let y = this.week === this.getTotalWeeks(this.year) ? this.year + 1 : this.year;
    let now = new Date();
    if (y === now.getFullYear() && w === this.getCurrentWeek(now)) {
      this.router.navigate(['/hungry']);
    } else {
      this.router.navigate(['/hungry/' + y + '/' + w]);
    }
  }

  previousWeek() {
    let w = this.week === 1 ? this.getTotalWeeks(this.year - 1) : this.week - 1;
    let y = this.week === 1 ? this.year - 1 : this.year;
    let now = new Date();
    if (y === now.getFullYear() && w === this.getCurrentWeek(now)) {
      this.router.navigate(['/hungry']);
    } else {
      this.router.navigate(['/hungry/' + y + '/' + w]);
    }
  }

  // returns the date of Monday of the week given (current or from params)
  getMonday(): Date {
    const firstDay = new Date(this.year, 0, 1).getDay();
    return new Date(this.year, 0, this.week * 7 - (firstDay + 6) % 7 + 1);
  }

  // calculates the total weeks of the given year by call getCurrentWeek to get the number of the last week
  getTotalWeeks(year: number): number {
    const lastWeek = new Date(year, 11, 28, 0, 0, 0, 0);
    return this.getCurrentWeek(lastWeek);
  }

  // calculates the number of week of a give week
  // calculates the difference in milliseconds between Thursday of the give week and Wednesday of 1st week at midnight,
  // divides the difference by mills per week and returns the result rounded +1 (weeks are 1 indexed).
  getCurrentWeek(currentWeek: Date): number {
    currentWeek.setHours(0, 0, 0, 0);
    const firstWeek = new Date(currentWeek.getFullYear(), 0, 4, 0, 0, 0, 0);
    currentWeek.setDate(currentWeek.getDate() + 2 - (currentWeek.getDay() + 6) % 7);
    firstWeek.setDate(firstWeek.getDate() + 2 - (firstWeek.getDay() + 6) % 7);
    const ret = 1 + Math.round(((currentWeek.getTime() - firstWeek.getTime()) / 86400000) / 7);
    return ret;
  }

  getMenus(menus: remote.DailyMenus) {

    this.weekMenus = new Array<DailyOrder>();
    const weekOrders = new Map<String, DailyOrder>();
    const date = this.getMonday();
    this.weekTotal = 0;

    for (const menu of menus) {
      const myMenu: DailyOrder = {
        date: new Date(menu.date),
        menuId: menu.menuId,
        menuVersion: menu.menuVersion,
        orderId: menu.orderId,
        orderVersion: menu.orderVersion,
        orderIsFinal: menu.orderIsFinal,
        total: 0,
        foodsMap: new Map<string, Map<number, remote.DailyMenuFoods>>()
      };
      myMenu.date.setHours(0, 0, 0, 0);
      myMenu.foodsMap.set('mainDish', new Map<number, remote.DailyMenuFoods>());
      myMenu.foodsMap.set('salad', new Map<number, remote.DailyMenuFoods>());
      myMenu.foodsMap.set('drink', new Map<number, remote.DailyMenuFoods>());
      for (const food of menu.foods) {
        myMenu.foodsMap.get(food.type).set(food.id, food);
        myMenu.total += food.price * food.quantity;
      }
      this.weekTotal += myMenu.total;
      weekOrders.set(myMenu.date.toString(), myMenu);
    }

    for (let i = 0; i < 5; i++) {
      const d = new Date(date.getFullYear(), date.getMonth(), date.getDate() + i);
      let myMenu = weekOrders.get(d.toString());
      if (!myMenu) {
        myMenu = {
          date: d,
          menuId: 0,
          menuVersion: 0,
          orderId: 0,
          orderVersion: 0,
          orderIsFinal: false,
          total: 0,
          foodsMap: new Map<string, Map<number, remote.DailyMenuFoods>>()
        };
        myMenu.foodsMap.set('mainDish', new Map<number, remote.DailyMenuFoods>());
        myMenu.foodsMap.set('salad', new Map<number, remote.DailyMenuFoods>());
        myMenu.foodsMap.set('drink', new Map<number, remote.DailyMenuFoods>());
      }
      this.weekMenus.push(myMenu);
    }

    this.loading = false;
  }

  calcTotal($event) {
    this.weekTotal += $event;
  }

}
