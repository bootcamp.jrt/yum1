import * as models from '../../../remote/model/models';

export interface DailyOrder extends models.DailyMenu {

    total: number;

    foodsMap: Map<string, Map<number, models.DailyMenuFoods>>;
}
