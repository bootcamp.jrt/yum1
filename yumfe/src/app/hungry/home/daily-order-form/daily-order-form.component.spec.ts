import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyOrderFormComponent } from './daily-order-form.component';

describe('DailyOrderFormComponent', () => {
  let component: DailyOrderFormComponent;
  let fixture: ComponentFixture<DailyOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
