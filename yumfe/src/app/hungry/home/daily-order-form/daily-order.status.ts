
export enum DailyOrderStatus {
  OPEN,
  PLACED,
  FINAL,
  CLOSED,
  DELETE
}
