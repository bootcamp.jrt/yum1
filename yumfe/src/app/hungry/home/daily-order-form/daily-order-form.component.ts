import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DailyOrder } from './daily-order';
import { DailyOrderStatus } from './daily-order.status';
import { MapToIterable } from '../../../shared/mapToIterable';
import { MdDialog, MdDialogRef, MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../../remote';
import { GlobalSettingsService } from "../../../shared/globalSettings.service";

import { SnackbarService } from '../../../shared/snackbar.service';


@Component({

  templateUrl: './daily-order-form.dialog.html'
})
export class DailyOrderFormDialog {

  public name: string;
  public date: Date;
  public email: boolean;
  public foods: Array<any>;
  public totalPrice: number;
  public totalItems: number;
  public currency: string;

  constructor(
    public dialogRef: MdDialogRef<DailyOrderFormDialog>
  ) { }

  closeDialog(button: string) {
    const ret = {
      email: this.email ? this.email : false,
      button: button
    }
    this.dialogRef.close(ret);
  }

}


@Component({
  selector: 'app-daily-order-form',
  templateUrl: './daily-order-form.component.html',
  styleUrls: ['./daily-order-form.component.scss']
})
export class DailyOrderFormComponent implements OnInit {

  @Input()
  dailyOrder: DailyOrder;

  @Output()
  totalChanged: EventEmitter<number> = new EventEmitter<number>();

  private initialTotal: number;
  private initialState: string;

  loading = true;

  // public variables used in the template
  date: Date;
  height = 0;
  orderState: DailyOrderStatus;
  OrderStatus = DailyOrderStatus;

  private itterable: MapToIterable = new MapToIterable();
  // change: number;

  constructor(
    private hungryService: remote.HungryApi,
    public dialog: MdDialog,
    public globalSettingsService: GlobalSettingsService,
    public snackBar: SnackbarService
  ) { }

  ngOnInit() {

    // define componets height by getting the maximum size of the 3 lists
    if (this.dailyOrder.foodsMap.get('mainDish').size > this.dailyOrder.foodsMap.get('salad').size) {
      if (this.dailyOrder.foodsMap.get('mainDish').size > this.dailyOrder.foodsMap.get('drink').size) {
        this.height = this.dailyOrder.foodsMap.get('mainDish').size;
      } else {
        this.height = this.dailyOrder.foodsMap.get('drink').size;
      }
    } else {
      if (this.dailyOrder.foodsMap.get('salad').size > this.dailyOrder.foodsMap.get('drink').size) {
        this.height = this.dailyOrder.foodsMap.get('salad').size;
      } else {
        this.height = this.dailyOrder.foodsMap.get('drink').size;
      }
    }

    if (this.dailyOrder.orderId === 0 && this.dailyOrder.orderIsFinal === true) {
      this.orderState = DailyOrderStatus.CLOSED;
    } else if (this.dailyOrder.orderId > 0 && this.dailyOrder.orderIsFinal === true) {
      this.orderState = DailyOrderStatus.FINAL;
    } else if (this.dailyOrder.orderId === 0 && this.dailyOrder.orderIsFinal !== true) {
      this.orderState = DailyOrderStatus.OPEN;
    } else if (this.dailyOrder.orderId > 0 && this.dailyOrder.orderIsFinal !== true) {
      this.orderState = DailyOrderStatus.PLACED;
    }

    // create initial state
    this.initialState = this.getState();

    // inititate total to emit change after order
    this.initialTotal = this.dailyOrder.total;

    this.loading = false;

  }

  private getState(): string {
    let s = '|';
    for (let foodList of this.itterable.transform(this.dailyOrder.foodsMap)) {
      for (let food of this.itterable.transform(foodList)) {
        s += food.id + ':' + food.quantity + '|';
      }
    }
    return s;
  }

  private getDate(): string {
    return this.dailyOrder.date.getFullYear() + '-' + (this.dailyOrder.date.getMonth() + 1) + '-' + this.dailyOrder.date.getDate();
  }

  private updateDailyOrder(menu: remote.DailyMenu) {
    this.dailyOrder = {
      date: new Date(menu.date),
      menuId: menu.menuId,
      menuVersion: menu.menuVersion,
      orderId: menu.orderId,
      orderVersion: menu.orderVersion,
      orderIsFinal: menu.orderIsFinal,
      total: 0,
      foodsMap: new Map<string, Map<number, remote.DailyMenuFoods>>()
    };
    this.dailyOrder.date.setHours(0, 0, 0, 0);
    this.dailyOrder.foodsMap.set('mainDish', new Map<number, remote.DailyMenuFoods>());
    this.dailyOrder.foodsMap.set('salad', new Map<number, remote.DailyMenuFoods>());
    this.dailyOrder.foodsMap.set('drink', new Map<number, remote.DailyMenuFoods>());
    for (const food of menu.foods) {
      this.dailyOrder.foodsMap.get(food.type).set(food.id, food);
      this.dailyOrder.total += food.price * food.quantity;
    }
    this.totalChanged.emit(this.dailyOrder.total - this.initialTotal);
    this.initialTotal = this.dailyOrder.total;
    this.loading = false;

    if (this.dailyOrder.orderId === 0 && this.dailyOrder.orderIsFinal === true) {
      this.orderState = DailyOrderStatus.CLOSED;
    } else if (this.dailyOrder.orderId > 0 && this.dailyOrder.orderIsFinal === true) {
      this.orderState = DailyOrderStatus.FINAL;
    } else if (this.dailyOrder.orderId === 0 && this.dailyOrder.orderIsFinal !== true) {
      this.orderState = DailyOrderStatus.OPEN;
    } else if (this.dailyOrder.orderId > 0 && this.dailyOrder.orderIsFinal !== true) {
      this.orderState = DailyOrderStatus.PLACED;
    }

    this.initialState = this.getState();
  }

  private handleError(error) {
    error = JSON.parse(error._body);
    if (error.dto) {
      this.snackBar.openError(error.message);
      this.updateDailyOrder(error.dto);
    }
  }

  private placeOrder(newEntry: remote.NewEntry): void {
    this.dailyOrder.orderId = newEntry.id;
    this.dailyOrder.orderVersion = newEntry.version;
    this.orderState = DailyOrderStatus.PLACED;
  }

  // button (+) in each food
  add(id: number, type: string): void {
    const food = this.dailyOrder.foodsMap.get(type).get(id);
    food.quantity++;
    this.dailyOrder.total += food.price;
    this.orderState = DailyOrderStatus.OPEN;
  }

  // button (-) in each food
  minus(id: number, type: string): void {
    const food = this.dailyOrder.foodsMap.get(type).get(id);
    if (food.quantity > 0) {
      food.quantity--;
      this.dailyOrder.total -= food.price;
      this.orderState = DailyOrderStatus.OPEN;
      if (this.dailyOrder.total === 0 && this.dailyOrder.orderId > 0) {
        this.orderState = DailyOrderStatus.DELETE
      }
    }
  }

  orderIsChanged(): boolean {
    return this.getState() !== this.initialState;
  }

  // order button: opens a modal and
  // - if bonapetit: places the order (post or put)
  // - if cancel closes the modal
  order() {

    // create the modal and open it
    const dialogRef = this.dialog.open(DailyOrderFormDialog);
    dialogRef.componentInstance.name = '< TODO: get user name >';
    dialogRef.componentInstance.date = this.dailyOrder.date;
    dialogRef.componentInstance.foods = new Array();
    dialogRef.componentInstance.totalPrice = this.dailyOrder.total;
    dialogRef.componentInstance.totalItems = 0;
    this.globalSettingsService.getCurrency().subscribe(currency => dialogRef.componentInstance.currency);

    // create the dto
    const orderSave: remote.OrderSave = {
      date: this.dailyOrder.date,
      orderVersion: this.dailyOrder.orderVersion,
      dailyMenuId: this.dailyOrder.menuId,
      dailyMenuVersion: this.dailyOrder.menuVersion,
      orderItems: new Array<remote.OrderSaveOrderItems>()
    };

    // iterate foods to fill dto and modal lists
    for (const foodCategory of this.itterable.transform(this.dailyOrder.foodsMap)) {
      for (const food of this.itterable.transform(foodCategory)) {
        if (food.quantity > 0) {

          // place food id and quantity in dto
          const orderItem: remote.OrderSaveOrderItems = {
            foodId: food.id,
            quantity: food.quantity
          };
          orderSave.orderItems.push(orderItem);

          // place food name, quantity and price for diplay in modal list
          const foodDisplayInModal = {
            name: food.name,
            quantity: food.quantity,
            price: food.price
          };
          dialogRef.componentInstance.foods.push(foodDisplayInModal);
          dialogRef.componentInstance.totalItems += food.quantity;
        }
      }
    }

    dialogRef.afterClosed().subscribe(result => {

      if (result.button === 'cancel') {

        return;

      } else if (result.button === 'bonAppetit') {

        // if orderId = 0 -> order does not exist -> need to post
        if (this.dailyOrder.orderId === 0) {
          // orderSave.date.setMonth(orderSave.date.getMonth() - 1); // ----------------------------- //
          this.hungryService.ordersPost(orderSave, result.email).subscribe(
            newEntry => {
              this.placeOrder(newEntry);
              this.snackBar.openSuccess("Your order is Placed! Bon appetit!");
              this.totalChanged.emit(this.dailyOrder.total);
              this.initialTotal = this.dailyOrder.total;
              this.initialState = this.getState();
            }, error => this.handleError(error)
          );

          // orderId > 0 -> need to put
        } else {
          this.hungryService.ordersIdPut(this.dailyOrder.orderId, orderSave, result.email).subscribe(
            newEntry => {
              this.placeOrder(newEntry);
              this.snackBar.openSuccess("Your order is Modified! Bon appetit!");
              this.totalChanged.emit(this.dailyOrder.total - this.initialTotal);
              this.initialTotal = this.dailyOrder.total;
              this.initialState = this.getState();
            }, error => this.handleError(error)
          );
        }
      }

    });
  }

  // modify button in form: just changes components status
  modify() {
    this.loading = true;
    this.hungryService.ordersIdGet(this.dailyOrder.orderId, this.dailyOrder.menuId, this.getDate(), this.dailyOrder.menuVersion)
      .subscribe(menu => {
        if (this.dailyOrder.orderVersion !== menu.orderVersion) {
          this.snackBar.openError('A newer version of this order has been detected! Now you are up to date!');
        }
        this.updateDailyOrder(menu);
        this.orderState = DailyOrderStatus.OPEN;
        this.loading = false;
      }, error => this.handleError(error)
      );
  }

  cancel() {
    this.loading = true;
    this.hungryService.ordersIdGet(this.dailyOrder.orderId, this.dailyOrder.menuId, this.getDate(), this.dailyOrder.menuVersion)
      .subscribe(menu => {
        this.updateDailyOrder(menu);
        this.orderState = DailyOrderStatus.PLACED;
        this.loading = false;
      }, error => this.handleError(error)
      );
  }

  // delete button
  delete() {
    this.loading = true;
    this.hungryService.ordersIdDelete(this.dailyOrder.orderId, this.dailyOrder.menuId, this.getDate(), this.dailyOrder.menuVersion).subscribe(() => {
      this.dailyOrder.orderId = 0;
      this.dailyOrder.orderVersion = undefined;
      this.dailyOrder.total = 0;
      this.totalChanged.emit(-this.initialTotal);
      this.initialTotal = this.dailyOrder.total;
      this.initialState = this.getState();
      // this.change = 0;
      for (const foodCategory of this.itterable.transform(this.dailyOrder.foodsMap)) {
        for (const food of this.itterable.transform(foodCategory)) {
          food.quantity = 0;
        }
      }
      this.orderState = DailyOrderStatus.OPEN;
      this.loading = false;
    }, error => this.handleError(error)
    );
  }

}
