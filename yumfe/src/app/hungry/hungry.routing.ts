import { Injectable } from "@angular/core";
import { AuthenticationService } from '../shared/authentication.service';
import {
  Routes,
  RouterModule,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { LoggedComponent } from '../shared/logged/logged.component';
import { Observable } from "rxjs/Rx";
// import { SettingsComponent} from '../shared/settings/settings.component';

import {  IsLoggedIn } from '../can-browse.service';

const hungryRoutes: Routes = [
  {
    path: 'hungry',
    component: LoggedComponent,
    canActivate: [IsLoggedIn],
    canActivateChild: [IsLoggedIn],
    children: [
      { path: '', component: HomeComponent },
      { path: ':year/:week', component: HomeComponent },
      { path: 'history', component: HistoryComponent },
      { path: 'history/:year/:month', component: HistoryComponent }
    ],
  //  canActivateChild: [CanActivateHungry],
  }
];

export const HungryRouting = RouterModule.forChild(hungryRoutes);


