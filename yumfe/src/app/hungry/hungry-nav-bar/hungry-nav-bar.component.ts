import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hungry-nav-bar',
  templateUrl: './hungry-nav-bar.component.html',
  styleUrls: ['./hungry-nav-bar.component.scss']
})
export class HungryNavBarComponent implements OnInit {

  private week: number;
  private month: number;
  private year: number;

  date: Date;

  private currentRoute: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {

      this.week = params.week ? +params.week : -1;
      this.month = params.month ? +params.month : -1;
      this.year = params.year ? +params.year : -1;

      if (this.year === -1) {
        this.date = new Date();
      } else {
        if (this.month === -1) {
          this.date = this.getWednesday();
        } else {
          this.date = new Date(this.year, this.month - 1, 15);
        }
      }

      this.month = this.date.getMonth() + 1;

      this.date.setHours(0, 0, 0, 0);
    });
  }

  private navigateMonth(y, m) {

    // let next = this.month === 12 ? 1 : (this.month + 1 );
    // let prev = this.month === 1 ? 12 : ( this.month - 1 );
    let toGo = m ? this.month === 12 ? 1 : (this.month + 1 ) : this.month === 1 ? 12 : ( this.month - 1 );

    let now = new Date();
    if (y === now.getFullYear() && toGo === (now.getMonth() + 1)) {
      this.router.navigate(['/hungry/history']);
    } else {
      this.router.navigate(['/hungry/history/' + y + '/' + toGo ]);
    }
  }

  private navigateWeek(y, w) {
    let now = new Date();
    if (y === now.getFullYear() && w === this.getCurrentWeek(now)) {
      this.router.navigate(['/hungry']);
    } else {
      this.router.navigate(['/hungry/' + y + '/' + w]);
    }
  }

  next() {

    if (this.router.url.search('history') === -1) {
      // user is at home

      let target = this.date.getMonth() === 11 ? 0 : this.date.getMonth() + 1;
      while (target !== this.date.getMonth()) {
        this.date.setDate(this.date.getDate() + 7);
      }
      let w = this.getCurrentWeek(this.date);

      this.navigateWeek(this.date.getFullYear(), w);

    } else {
      // user is at history

      let m = this.date.getMonth() === 11 ? 0 : this.date.getMonth() + 1;
      let y = m === 0 ? this.date.getFullYear() + 1 : this.date.getFullYear();

      this.navigateMonth(y, true);

    }

  }

  previous() {

    if (this.router.url.search('history') === -1) {
      // user is at home

      let target = this.date.getMonth() >= 2 ? this.date.getMonth() - 2 : this.date.getMonth() === 1 ? 11 : 10;
      while (target !== this.date.getMonth()) {
        this.date.setDate(this.date.getDate() - 7);
      }
      this.date.setDate(this.date.getDate() + 7);
      let w = this.getCurrentWeek(this.date);
      this.router.navigate(['/hungry/' + this.date.getFullYear() + '/' + w]);

      this.navigateWeek(this.date.getFullYear(), w);

    } else {
      // user is at history

      let m = this.date.getMonth() === 0 ? 11 : this.date.getMonth() - 1;
      let y = m === 11 ? this.date.getFullYear() - 1 : this.date.getFullYear();

      this.navigateMonth(y, false);

    }

  }

  // returns the date of Monday of the week given (current or from params)
  getWednesday(): Date {
    const firstDay = new Date(this.year, 0, 1).getDay();
    return new Date(this.year, 0, this.week * 7 - (firstDay + 6) % 7 + 3);
  }

  // calculates the total weeks of the given year by call getCurrentWeek to get the number of the last week
  getTotalWeeks(year: number): number {
    const lastWeek = new Date(year, 11, 28, 0, 0, 0, 0);
    return this.getCurrentWeek(lastWeek);
  }

  // calculates the number of week of a give week
  // calculates the difference in milliseconds between Wednesday of the give week and Wednesday of 1st week at midnight,
  // divides the difference by mills per week and returns the result rounded +1 (weeks are 1 indexed).
  getCurrentWeek(currentWeek: Date): number {
    currentWeek.setHours(0, 0, 0, 0);
    const firstWeek = new Date(currentWeek.getFullYear(), 0, 4, 0, 0, 0, 0);
    currentWeek.setDate(currentWeek.getDate() + 2 - (currentWeek.getDay() + 6) % 7);
    firstWeek.setDate(firstWeek.getDate() + 2 - (firstWeek.getDay() + 6) % 7);
    const ret = 1 + Math.round(((currentWeek.getTime() - firstWeek.getTime()) / 86400000) / 7);
    return ret;
  }

}
