import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { HungryRouting } from './hungry.routing';
import { HungryApi } from '../remote';

import { SharedModule } from '../shared/shared.module';
import { DailyOrderFormComponent, DailyOrderFormDialog } from './home/daily-order-form/daily-order-form.component';
import { DailyOrderHistoryComponent } from './history/daily-order-history/daily-order-history.component';
import { HungryNavBarComponent } from './hungry-nav-bar/hungry-nav-bar.component';

@NgModule({
  imports: [
    CommonModule,
    HungryRouting,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    HistoryComponent,
    DailyOrderFormComponent,
    DailyOrderFormDialog,
    DailyOrderHistoryComponent,
    HungryNavBarComponent
  ],
  providers: [
    HungryApi
  ],
  entryComponents: [
    DailyOrderFormDialog
  ]
})
export class HungryModule { }
