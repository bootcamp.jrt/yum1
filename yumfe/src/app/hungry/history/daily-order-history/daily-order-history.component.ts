import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as remote from '../../../remote';
import { GlobalSettingsService } from "app/shared/globalSettings.service";

@Component({
  selector: 'app-daily-order-history',
  templateUrl: './daily-order-history.component.html',
  styleUrls: ['./daily-order-history.component.scss']
})

export class DailyOrderHistoryComponent implements OnInit {

  @Input() order: remote.OrdersInner = {};
  @Input() isInMonth: boolean;
  total: number = 0;
  deadline: Date ;

  constructor(public globalSettingsService: GlobalSettingsService,
              private router: Router) { }

  ngOnInit() {
    this.calcTotal();

    this.globalSettingsService.getDeadLine().subscribe(deadline =>{
        let d = new Date(deadline);
        this.deadline = d;
      });

  }

  calcTotal() {
    for (const food of this.order.foods){
        this.total += food.price * food.quantity ;
    }
  }

  // datePassed(): boolean{
  //   if (this.order.date < new Date()){
  //     return true;
  //   }
  //   return false;
  // }

  datePassed(): boolean {
    if(this.deadline){
        let check: Date = this.deadline;

      if(new Date() > check){
        check.setDate(check.getDate() +1);
      }

      if (this.order.date < check){
        return true;
      }
        return false;
    }

  }

  orderButton(){
    this.router.navigate(['/hungry/' + this.order.date.getFullYear() + '/' + this.getCurrentWeek(this.order.date)]);
  }

  getCurrentWeek(currentWeek: Date): number {
    currentWeek.setHours(0, 0, 0, 0);
    const firstWeek = new Date(currentWeek.getFullYear(), 0, 4, 0, 0, 0, 0);
    currentWeek.setDate(currentWeek.getDate() + 2 - (currentWeek.getDay() + 6) % 7);
    firstWeek.setDate(firstWeek.getDate() + 2 - (firstWeek.getDay() + 6) % 7);
    const ret = 1 + Math.round(((currentWeek.getTime() - firstWeek.getTime()) / 86400000) / 7);
    return ret;
  }

}
