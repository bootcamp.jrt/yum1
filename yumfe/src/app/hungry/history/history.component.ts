import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as remote from '../../remote';
import { GlobalSettingsService } from "../../shared/globalSettings.service";


@Component({
  selector: 'app-hungry-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  loading = true;

  history: Array<remote.OrdersInner> = [];
  private sub: any;
  private year: number;
  private month: number;
  private existingOrders: Map<String, remote.OrdersInner> = new Map<String, remote.OrdersInner>();


  constructor(private hungryService: remote.HungryApi,
    private route: ActivatedRoute,
    private router: Router,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {


      if (params.year && params.month) {

        this.year = +params['year'];
        this.month = +params['month'];

        this.hungryService.menusMonthlyYearMonthGet(this.month, this.year).subscribe(orders => {

          if (orders.length === 0) {
            //adeio
            this.initHistory(new Date(this.year, this.month - 1, 1));
          }
          else {
            for (const order of orders) {
              this.existingOrders.set(order.date.toDateString(), order);
            }

            this.initHistory(orders[0].date);
            this.populateGrid();
          }

          this.loading = false;

        }, error => console.log(error));
      }
      else {

        this.hungryService.menusMonthlyGet().subscribe(orders => {

          if (orders.length === 0) {
            //adeio
            let forGrid: Date = new Date();

            this.year = forGrid.getFullYear();
            this.month = forGrid.getMonth() + 1;

            this.initHistory(new Date());
          }
          else {
            for (const order of orders) {
              this.existingOrders.set(order.date.toDateString(), order);
            }

            this.year = orders[0].date.getFullYear();
            this.month = orders[0].date.getMonth() + 1;

            this.initHistory(orders[0].date);
            this.populateGrid();
          }

          this.loading = false;

        }, error => console.log(error));
      }

    });

  }

  calcTotal(): number{
    let total: number = 0;

    for ( let item of this.history ){

      if (item.foods){

        for (let food of item.foods){
          total += food.price*food.quantity;
        }

      }

    }
    return total;
  }

  private initHistory(date: Date) {

    this.history = [];

    let dateOfGrid: Date = new Date(date.getFullYear(), date.getMonth(), 1);
    let displayMonth = dateOfGrid.getMonth();
    let day = dateOfGrid.getDay();

    let endDate: Date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    let endDay = endDate.getDay();


    if (day == 0) {//Sunday
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }
    else if (day == 6) {//Saturday
      dateOfGrid.setDate(dateOfGrid.getDate() + 2);
    }
    else if (day == 1) {//Monday
      //do nothing
    }
    else {
      dateOfGrid.setDate(dateOfGrid.getDate() - (day - 1));
    }

    if (endDay === 0 || endDay > 5) {
      //do nothing
    }
    else {
      endDate.setDate(endDate.getDate() + (5 - endDay));
    }


    while (dateOfGrid <= endDate) {

      if (dateOfGrid.getDay() !== 0 && dateOfGrid.getDay() !== 6) {

        let empty: remote.OrdersInner = {
          dailyMenuId: 0,
          date: new Date(dateOfGrid),
          foods: new Array<remote.OrdersInnerFoods>()
        };

        this.history.push(empty);
      }
      dateOfGrid.setDate(dateOfGrid.getDate() + 1);
    }


  }


  private populateGrid() {

    for (let i = 0; i < this.history.length; i++) {
      let gridItem: remote.OrdersInner = this.history[i];

      let existing: remote.OrdersInner = this.existingOrders.get(gridItem.date.toDateString());

      if (existing) {

        this.history[i] = existing;
      }

    }

  }

  currentMonth(order: remote.OrdersInner) {
    if (order.date.getMonth() === this.month - 1) {
      return true;
    }
    return false;
  }



  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
