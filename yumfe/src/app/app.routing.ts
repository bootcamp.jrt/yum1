import { Injectable } from '@angular/core';
import {
  Routes,
  RouterModule,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { LoginComponent } from './anon/login/login.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { RegisterComponent } from './anon/register/register.component';
import { AuthenticationService } from './shared/authentication.service';
import { Observable } from "rxjs/Rx";
import { CanBrowse, CanLogin, IsLoggedIn } from './can-browse.service';
import { SettingsComponent} from './shared/settings/settings.component';
import { ForgotPwdComponent } from './anon/forgot-pwd/forgot-pwd.component';
import { ChangePwdComponent } from './anon/change-pwd/change-pwd.component';

const appRoutes: Routes = [
  { path: '', component:NotFoundComponent, pathMatch: 'full', canActivate: [CanBrowse] },
  { path: 'login', component: LoginComponent  , canActivate: [CanLogin] },
  { path: 'register', component: RegisterComponent },
  { path: 'settings', component: SettingsComponent, canActivate: [IsLoggedIn] },
  { path: 'forgotpasswd', component: ForgotPwdComponent},
  { path: 'changepasswd/:secret', component: ChangePwdComponent},
  { path: '**', component: NotFoundComponent }, //always last
];

export const AppRouting = RouterModule.forRoot(appRoutes);


