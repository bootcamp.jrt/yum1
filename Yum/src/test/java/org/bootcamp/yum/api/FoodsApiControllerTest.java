/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.api.model.FoodSave;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.service.FoodsService;
import org.bootcamp.yum.service.GlobalSettingsService;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class FoodsApiControllerTest {

    static Yum yum;
    
    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final FoodsService foodsService = new FoodsService();

    @Mock
    private FoodRepository mockFoodRepository;
    
    @Mock
    private GlobalSettingsService gss;

    private MockMvc mockMvc;

    public FoodsApiControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        
        yum = new Yum();
        yum.setDeadline(LocalTime.MIDNIGHT);
        yum.setCurrency("asdasd");
        yum.setId(1L);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        
        mockMvc = MockMvcBuilders.standaloneSetup(new FoodsApiController(foodsService))
        .setControllerAdvice(new ExceptionControllerAdvice())
        .build(); 
        
        given(gss.getSettings()).willReturn(yum);
    }

    @After
    public void tearDown() {
    }   

    /**
     * Test of foodsPost method, of class FoodsApiController.
     */
    @Test
    public void testFoodsPost_success() throws Exception {
        System.out.println("----------------foodsPost---------------------");

        //Foods list
        List<Food> mockFoods = new ArrayList<>();
        //Create mockFood + add it to the list
        Food mockFood = new Food(1);
        mockFood.setArchived(false);
        mockFood.setFoodType(FoodType.MDISH);
        mockFood.setName("Gemista");
        mockFood.setPrice(5.00);
        mockFood.setDescription("Me kima!!!");

        mockFoods.add(mockFood);

        FoodSave body = new FoodSave();
        body.setName("jhfjksn");
        body.setPrice(5.00);
        body.setDescription("AAAAAAAAA");
        body.setType("mainDish");

        given(mockFoodRepository.save(any(Food.class))).willReturn(new Food());

        mockMvc.perform(
                post("/api/foods")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString()))
                .andExpect(status().isNoContent());

        // we verify that we called save method once only on the repo
        verify(mockFoodRepository, times(1)).save(any(Food.class));
        verify(mockFoodRepository, times(1)).findByNameAndArchived("jhfjksn", false);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);

    }

    @Test
    public void testFoodsPost_notFound() throws Exception {
        System.out.println("----------------foodsPost---------------------");

        //Foods list
        List<Food> mockFoods = new ArrayList<>();
        //Create mockFood + add it to the list
        Food mockFood = new Food(1);
        mockFood.setArchived(false);
        mockFood.setFoodType(FoodType.MDISH);
        mockFood.setName("Gemista");
        mockFood.setPrice(5.00);
        mockFood.setDescription("Me kima!!!");

        mockFoods.add(mockFood);

        FoodSave body = new FoodSave();
        body.setName("jhfjksn");
        body.setPrice(-5.0);
        body.setDescription("AAAAAAAAA");
        body.setType("mainDish");

        given(mockFoodRepository.findById(mockFood.getId())).willReturn(mockFood);

        mockMvc.perform(
                post("/api/foods")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString()))
                .andExpect(status().is(400));

        // we verify that we called save method once only on the repo
        verify(mockFoodRepository, times(0)).save(any(Food.class));
        verify(mockFoodRepository, times(0)).findByNameAndArchived("jhfjksn", false);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);
    }

    @Test
    public void testFoodsPost_nullFoodType() throws Exception {
        System.out.println("----------------foodsPost---------------------");

        //Foods list
        List<Food> mockFoods = new ArrayList<>();
        //Create mockFood + add it to the list
        Food mockFood = new Food(1);
        mockFood.setArchived(false);
        mockFood.setFoodType(FoodType.MDISH);
        mockFood.setName("Gemista");
        mockFood.setPrice(5.00);
        mockFood.setDescription("Me kima!!!");

        mockFoods.add(mockFood);

        FoodSave body = new FoodSave();
        body.setName("jhfjksn");
        body.setPrice(5.0);
        body.setDescription("AAAAAAAAA");
        body.setType(" ");

        given(mockFoodRepository.findById(mockFood.getId())).willReturn(mockFood);

        mockMvc.perform(
                post("/api/foods")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString()))
                .andExpect(status().is(400));

        // we verify that we called save method once only on the repo
        verify(mockFoodRepository, times(0)).save(any(Food.class));
        verify(mockFoodRepository, times(0)).findByNameAndArchived("jhfjksn", false);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);
    }

    @Test
    public void testFoodsPost_sameName() throws Exception {
        System.out.println("----------------foodsPost---------------------");

        //Foods list
        List<Food> mockFoods = new ArrayList<>();
        //Create mockFood + add it to the list
        Food mockFood = new Food(1);
        mockFood.setArchived(false);
        mockFood.setFoodType(FoodType.MDISH);
        mockFood.setName("Gemista");
        mockFood.setPrice(5.00);
        mockFood.setDescription("Me kima!!!");

        mockFoods.add(mockFood);

        FoodSave body = new FoodSave();
        body.setName("jhfjksn");
        body.setPrice(5.0);
        body.setDescription("AAAAAAAAA");
        body.setType("mainDish");
        
        FoodSave body1 = new FoodSave();
        body1.setName("Gemista");
        body1.setPrice(7.0);
        body1.setDescription("AMopaaaaaaaa");
        body1.setType("salad");

        given(mockFoodRepository.findByNameAndArchived(body1.getName(), false)).willReturn(mockFood);

        mockMvc.perform(
                post("/api/foods")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body1.toJsonString()))
                .andExpect(status().is(406));

        // we verify that we called save method once only on the repo
        verify(mockFoodRepository, times(0)).save(any(Food.class));
        verify(mockFoodRepository, times(1)).findByNameAndArchived(body1.getName(), false);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockFoodRepository);
    }

}
