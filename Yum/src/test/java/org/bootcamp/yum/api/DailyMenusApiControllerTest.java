/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.model.DailyMenuSave;
import org.bootcamp.yum.api.model.DailyMenuSaveFoods;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.DailyMenusService;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DailyMenusApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final DailyMenusService dailyMenusService = new DailyMenusService();

    @Mock
    private FoodRepository mockFoodRepository;

    @Mock
    private DailyMenuRepository mockDailyMenuRepository;

    @Mock
    private UserRepository mockUserRepository;

    private MockMvc mockMvc;

    public DailyMenusApiControllerTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.standaloneSetup(new DailyMenusApiController(dailyMenusService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of dailyMenusPost method, of class DailyMenusApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testDailyMenusPost_success() throws Exception {
        System.out.println("-------------DailyMenusPost-----------------");

        LocalDate mockDate = LocalDate.now().plusDays(12);

        DailyMenuSave mockDailyMenu = new DailyMenuSave();

        mockDailyMenu.setDate(mockDate);
        mockDailyMenu.setVersion(1);

        //Foods list
        List<DailyMenuSaveFoods> mockFoods = new ArrayList<>();
        //Create mockFood + add it to the list
        DailyMenuSaveFoods mockFood = new DailyMenuSaveFoods();
        mockFood.setId(1L);
        DailyMenuSaveFoods mockFood2 = new DailyMenuSaveFoods();
        mockFood2.setId(2L);

        mockFoods.add(mockFood);
        mockFoods.add(mockFood2);

        mockDailyMenu.setFoods(mockFoods);

        Food mockDAOFood = new Food(mockFood.getId());
        Food mockDAOFood2 = new Food(mockFood2.getId());
        mockDAOFood.setFoodType(FoodType.MDISH);
        mockDAOFood2.setFoodType(FoodType.MDISH);
        mockDAOFood.setName("gvfsgfdsa");
        mockDAOFood2.setName("gvvdsvdsfsgfdsa");

        given(mockFoodRepository.findById(mockFood.getId())).willReturn(mockDAOFood);
        given(mockFoodRepository.findById(mockFood2.getId())).willReturn(mockDAOFood2);

        given(mockDailyMenuRepository.save(any(DailyMenu.class))).willReturn(new DailyMenu());

        System.out.println(mockDailyMenu.toJsonString());

        mockMvc.perform(
                post("/api/dailyMenus")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockDailyMenu.toJsonString()))
                .andExpect(status().isOk());

        // we verify that we called save method once only on the repo
        verify(mockDailyMenuRepository, times(1)).save(any(DailyMenu.class));
    }
}
