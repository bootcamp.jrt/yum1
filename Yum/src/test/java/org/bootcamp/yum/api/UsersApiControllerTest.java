/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.test.annotation.WithMockAuth;

import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.GlobalSettingsService;
import org.bootcamp.yum.service.UsersService;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class UsersApiControllerTest {

    User user;
    User user2;
    String body;

    static Yum yum;

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final UsersService usersService = new UsersService();

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private GlobalSettingsService gss;

    private MockMvc mockMvc;

    public UsersApiControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {

        yum = new Yum();
        yum.setDeadline(LocalTime.MIDNIGHT);
        yum.setCurrency("asdasd");
        yum.setId(1L);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // set up the mock controller
        mockMvc = MockMvcBuilders.standaloneSetup(new UsersApiController(usersService)).setControllerAdvice(new ExceptionControllerAdvice()).build();

        given(gss.getSettings()).willReturn(yum);

        user = new User();
        user.setId(1L);
        user.setEmail("test@test.com");
        user.setPassword("123456");
        user.setVersion(1);
        user.setApproved(Boolean.TRUE);
        user.setRole(UserRole.HUNGRY);

        user2 = new User();
        user2.setId(2L);
        user2.setEmail("test2@test.com");
        user2.setPassword("123456");
        user2.setVersion(1);
        user2.setApproved(Boolean.TRUE);
        user2.setRole(UserRole.HUNGRY);

        body = "{  \"id\": 1,\n"
                + "  \"firstName\": \"string\",\n"
                + "  \"lastName\": \"string\",\n"
                + "  \"email\": \"string@test.com\",\n"
                + "  \"role\": \"admin\",\n"
                + "  \"picture\": \"string\",\n"
                + "  \"approved\": true,\n"
                + "  \"regDate\": \"2017-04-28\",\n"
                + "  \"version\": 1}";

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of usersIdPut method, of class UsersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdPut_OK() throws Exception {
        given(mockUserRepository.findOne(1L)).willReturn(user);

        int id = 1;
        mockMvc.perform(
                put("/api/users/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body)
        ).andExpect(status().isNoContent());

        verify(mockUserRepository, times(2)).findOne(1L);
        verify(mockUserRepository, times(1)).findByEmail("string@test.com");

        verifyNoMoreInteractions(mockUserRepository);
    }

    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdPut_UserNotFound() throws Exception {
        given(mockUserRepository.findOne(1L)).willReturn(user);

        int id = 3;
        mockMvc.perform(
                put("/api/users/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body)
        ).andExpect(status().isNotFound());

        verify(mockUserRepository, times(1)).findOne(3L);
        verify(mockUserRepository, times(1)).findOne(1L);

        verifyNoMoreInteractions(mockUserRepository);
    }

    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdPut_ConcurrentModification() throws Exception {
        given(mockUserRepository.findOne(1L)).willReturn(user);
        given(mockUserRepository.findOne(2L)).willReturn(user2);
        user2.setVersion(0);
        int id = 2;
        mockMvc.perform(
                put("/api/users/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body)
        ).andExpect(status().isConflict());

        verify(mockUserRepository, times(1)).findOne(2L);
        verify(mockUserRepository, times(1)).findOne(1L);

        verifyNoMoreInteractions(mockUserRepository);
    }
}
