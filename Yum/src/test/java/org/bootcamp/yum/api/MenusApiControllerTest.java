/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.OrderItemPK;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.MenuService;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.*;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class MenusApiControllerTest {

    List<DailyMenu> mockMenusList;
    User user;
    DailyMenu mockMenu;

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final MenuService menuService = new MenuService();

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private DailyMenuRepository mockDailyMenuRepository;

    private MockMvc mockMvc;

    public MenusApiControllerTest() {
    }

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.standaloneSetup(new MenusApiController(menuService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();

        mockMenusList = new ArrayList<>();
        DailyOrder order1 = new DailyOrder();
        user = new User();
        mockMenu = new DailyMenu();
        List<OrderItem> mockOrderItems = new ArrayList<>();
        OrderItem mockItem = new OrderItem();
        OrderItemPK mockPK = new OrderItemPK();
        List<DailyOrder> mockDailyOrders = new ArrayList<>();
        List<Food> mockFoods = new ArrayList<>();
        Food food1 = new Food();

        food1.setId(1L);
        food1.addDailyMenu(mockMenu);
        food1.addOrderItem(mockItem);
        food1.setFoodType(FoodType.MDISH);
        mockFoods.add(food1);

        user.setId(1L);
        user.setEmail("test@test.com");
        user.setPassword("123456");
        user.setVersion(1);
        user.setApproved(Boolean.TRUE);
        user.setRole(UserRole.HUNGRY);
        user.addDailyOrder(order1);

        mockMenu.setDate(LocalDate.now());
        mockMenu.setId(1);
        mockMenu.setVersion(1);
        mockMenu.addFood(food1);
        mockMenu.addDailyOrder(order1);

        order1.setUser(user);
        order1.setId(1);
        order1.setDailyMenu(mockMenu);
        order1.addOrderItem(mockItem);

        mockPK.setDailyOrderId(1L);
        mockPK.setFoodId(1L);
        mockItem.setFood(food1);
        mockItem.setId(mockPK);
        mockItem.setQuantity(1);
        mockItem.setDailyOrder(order1);

        mockOrderItems.add(mockItem);
        mockDailyOrders.add(order1);

        mockMenusList.add(mockMenu);

    }

    /**
     * Test of menusMonthlyGet method, of class MenusApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testMenusMonthlyGet() throws Exception {

        LocalDate from = new LocalDate().withDayOfMonth(1);
        LocalDate to = new LocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

        given(mockUserRepository.findOne(1L)).willReturn(user);

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenusList);

        given(mockDailyMenuRepository.findByDateBetween(from, to)).willReturn(mockMenusList);

        mockMvc.perform(get("/api/menus/monthly")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].dailyMenuId", is(1)));

//        // we verify that we called findAll method once only on the repo.
        verify(mockDailyMenuRepository, times(1)).findByDateBetween(from, to);
//        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockDailyMenuRepository);
//        // we verify that we didnt modify the first food item in the repository
        assertEquals(mockMenu, mockDailyMenuRepository.findAll().iterator().next());
    }
}
