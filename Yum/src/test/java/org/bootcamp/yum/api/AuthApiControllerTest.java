/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.AuthService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthApiControllerTest {

    @InjectMocks
    private final AuthService mockAuthService = new AuthService();

    @Mock
    private UserRepository mockUserRepo;

    private MockMvc mockMvc;

    private Login mockLogin;
    private UserSave mockUserSave;
    private User mockUser;

    public AuthApiControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        // set up the mock controller        
        mockMvc = MockMvcBuilders.standaloneSetup(new AuthApiController(mockAuthService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();

        // set up the default mock data
        mockLogin = new Login();
        mockLogin.setEmail("test@test.com");
        mockLogin.setPassword("123456");

        mockUserSave = new UserSave();
        mockUserSave.setEmail("newuser@test.com");
        mockUserSave.setFirstName("test");
        mockUserSave.setLastName("mock");
        mockUserSave.setPassword("123456");

        mockUser = new User();
        mockUser.setId(1L);
        mockUser.setEmail("test@test.com");
        mockUser.setPassword("123456");
        mockUser.setApproved(true);
        mockUser.setFirstName("test");
        mockUser.setLastName("mock");
        mockUser.setRole(UserRole.HUNGRY);

        // mock user repository default behaviour
        given(mockUserRepo.findByEmail(mockLogin.getEmail())).willReturn(mockUser);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAuthLoginPost_defaultCase() throws Exception {
        System.out.println("authLoginPost_defaultCase");

        mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockLogin.toJsonString())
        ).andExpect(status().isOk());

        verify(mockUserRepo, times(1)).findByEmail(mockLogin.getEmail());
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthLoginPost_wrongPassword() throws Exception {
        System.out.println("authLoginPost_wrongPassword");

        mockLogin.setPassword("123455");

        mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockLogin.toJsonString())
        ).andExpect(status().is(403));

        verify(mockUserRepo, times(1)).findByEmail(mockLogin.getEmail());
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthLoginPost_nullPassword() throws Exception {
        System.out.println("authLoginPost_nullPassword");

        mockLogin.setPassword(null);

        mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockLogin.toJsonString())
        ).andExpect(status().is(403));

        verify(mockUserRepo, times(1)).findByEmail(mockLogin.getEmail());
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthLoginPost_wrongEmail() throws Exception {
        System.out.println("authLoginPost_wrongEmail");

        mockLogin.setEmail("wrong@email.com");
        mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockLogin.toJsonString())
        ).andExpect(status().is(403));

        verify(mockUserRepo, times(1)).findByEmail(mockLogin.getEmail());
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthLoginPost_notApprovedUser() throws Exception {
        System.out.println("authLoginPost_notApprovedUser");

        mockUser.setApproved(false);

        mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockLogin.toJsonString())
        ).andExpect(status().is(403));

        verify(mockUserRepo, times(1)).findByEmail(mockLogin.getEmail());
        verifyNoMoreInteractions(mockUserRepo);
    }

    /**
     * Test of authRegisterPost method, of class AuthApiController.
     */
    @Test
    public void testAuthRegisterPost_defaultCase() throws Exception {
        System.out.println("authRegisterPost_defaultCase");

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().isNoContent());

        verify(mockUserRepo, times(1)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(1)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthRegisterPost_invalidEmail() throws Exception {
        System.out.println("authRegisterPost_wrongEmail");

        mockUserSave.setEmail("invalidemailcom");

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().is(400));

        verify(mockUserRepo, times(0)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthRegisterPost_nullEmail() throws Exception {
        System.out.println("authRegisterPost_nullEmail");

        mockUserSave.setEmail(null);

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().is(400));

        verify(mockUserRepo, times(0)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthRegisterPost_wrongPassword() throws Exception {
        System.out.println("authRegisterPost_wrongPassword");

        mockUserSave.setPassword("12345");

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().is(400));

        verify(mockUserRepo, times(0)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthRegisterPost_nullPassword() throws Exception {
        System.out.println("authRegisterPost_nullPassword");

        mockUserSave.setPassword(null);

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().is(400));

        verify(mockUserRepo, times(0)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

    @Test
    public void testAuthRegisterPost_userExists() throws Exception {
        System.out.println("authRegisterPost_userExists");

        given(mockUserRepo.findByEmail(mockUserSave.getEmail())).willReturn(new User());

        mockMvc.perform(
                post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mockUserSave.toJsonString())
        ).andExpect(status().is(409));

        verify(mockUserRepo, times(1)).findByEmail(mockUserSave.getEmail());
        verify(mockUserRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(mockUserRepo);
    }

}
