/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import java.util.ArrayList;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.model.OrderSave;
import org.bootcamp.yum.api.model.OrderSaveOrderItems;
import org.bootcamp.yum.api.model.OrderSummary;
import org.bootcamp.yum.api.model.OrderSummaryDailyOrders;
import org.bootcamp.yum.api.model.OrdersChefInnerFoods;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.GlobalSettingsService;
import org.bootcamp.yum.service.OrdersService;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class OrdersApiControllerTest {

    public OrdersApiControllerTest() {
    }

    static Yum yum;

    User logged;
    DailyMenu menu;
    Food food1, food2;
    DailyOrder order;
    OrderItem item1, item2;

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final OrdersService ordersService = new OrdersService();

    @Mock
    private DailyOrderRepository mockDoRepo;
    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private FoodRepository mockFoodRepo;

    @Mock
    private DailyMenuRepository mockDmRepo;

    @Mock
    private OrderItemRepository mockOiRepo;

    @Mock
    private GlobalSettingsService gss;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        mockMvc = MockMvcBuilders.standaloneSetup(new OrdersApiController(ordersService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();

        given(gss.getSettings()).willReturn(yum);

    }

    @BeforeClass
    public static void setUpClass() {

        yum = new Yum();
        yum.setDeadline(LocalTime.MIDNIGHT);
        yum.setCurrency("asdasd");
        yum.setId(1L);

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        logged = new User(1L);
        logged.setApproved(true);
        logged.setEmail("thimios@gmail.com");
        logged.setFirstName("Thimios");
        logged.setLastName("Floros");
        logged.setPassword("pwd");
        logged.setRole(UserRole.CHEF);

        menu = new DailyMenu(1L);
        menu.setDate(LocalDate.now().plusDays(5));
        menu.setVersion(1);

        food1 = new Food(1L);
        food1.setArchived(false);
        food1.setName("Ntolmas");
        food1.setFoodType(FoodType.MDISH);
        food1.setVersion(1);
        food1.setDescription("Ntolmas leme");
        food1.setPrice(3.5);

        food2 = new Food(2L);
        food2.setArchived(false);
        food2.setName("Keftes");
        food2.setFoodType(FoodType.MDISH);
        food2.setVersion(1);
        food2.setDescription("KeDeFtes");
        food2.setPrice(3.5);

        ArrayList<Food> dmFoods = new ArrayList<>();
        dmFoods.add(food1);
        dmFoods.add(food2);

        menu.setFoods(dmFoods);

        order = new DailyOrder(1L);
        order.setDailyMenu(menu);
        order.setFinal(false);
        order.setUser(logged);
        order.setVersion(1);

        item1 = new OrderItem();
        item1.setDailyOrder(order);
        item1.setFood(food1);
        item1.setQuantity(4);

        item2 = new OrderItem();
        item2.setDailyOrder(order);
        item2.setFood(food2);
        item2.setQuantity(3);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(item1);
        orderItems.add(item2);

        order.setOrderItems(orderItems);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of ordersDailyDailyMenuIdGet method, of class OrdersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testOrdersDailyDailyMenuIdGet_200() throws Exception {
        System.out.println("testOrdersDailyDailyMenuIdGet_200");

        //prepare DB state
        User hungry = new User(2L);
        hungry.setApproved(true);
        hungry.setEmail("hungry@gmail.com");
        hungry.setFirstName("hungry");
        hungry.setLastName("user");
        hungry.setPassword("pwd");
        hungry.setRole(UserRole.HUNGRY);

        DailyOrder order1 = new DailyOrder(2);
        order1.setDailyMenu(menu);
        order1.setFinal(false);
        order1.setUser(hungry);

        order1.addOrderItem(item1);
        order1.addOrderItem(item2);

        menu.addDailyOrder(order1);

        //prepare expected response DTO
        OrderSummary responseDto = new OrderSummary();
        responseDto.setDate(menu.getDate());

        OrdersChefInnerFoods one = new OrdersChefInnerFoods(item1);
        one.setQuantity(8);

        OrdersChefInnerFoods two = new OrdersChefInnerFoods(item2);
        two.setQuantity(6);

        responseDto.addFoodsItem(one);
        responseDto.addFoodsItem(two);

        OrderSummaryDailyOrders osdo = new OrderSummaryDailyOrders();
        osdo.setFirstName(logged.getFirstName());
        osdo.setLastName(logged.getLastName());

        osdo.addFoodsItem(new OrdersChefInnerFoods(item1));
        osdo.addFoodsItem(new OrdersChefInnerFoods(item2));

        OrderSummaryDailyOrders osdo1 = new OrderSummaryDailyOrders();
        osdo1.setFirstName(hungry.getFirstName());
        osdo1.setLastName(hungry.getLastName());

        osdo1.addFoodsItem(new OrdersChefInnerFoods(item1));
        osdo1.addFoodsItem(new OrdersChefInnerFoods(item2));

        responseDto.addDailyOrdersItem(osdo);
        responseDto.addDailyOrdersItem(osdo1);

        given(mockDmRepo.findOne(1L)).willReturn(menu);

        mockMvc.perform(
                get("/api/orders/daily/{dailyMenuId}", 1)
        ).andExpect(status().isOk());

    }

    /**
     * Test of ordersIdPut method, of class OrdersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_200() throws Exception {
        System.out.println("testOrdersIdPut_200");

        OrderSave body = new OrderSave();
        body.setDailyMenuId(menu.getId());
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems(item1);
        item3.setQuantity(8);

        body.addOrderItemsItem(item3);

        given(mockDoRepo.findOne(1L)).willReturn(order);
        given(mockUserRepository.findOne(1L)).willReturn(logged);   //any(Long.class)
//        given(mockYumRepo.findOne(1L)).willReturn(yum);
        given(mockDmRepo.findOne(1L)).willReturn(menu);
//        given(mockFoodRepo.findById(1L)).willReturn(food1); // access the food repo if add a different food

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().isOk());

        // we verify the expected repo calls.
        verify(mockDoRepo, times(1)).findOne(1L);
        verify(mockUserRepository, times(1)).findOne(1L);
//        verify(mockYumRepo, times(1)).findOne(1L);
        verify(mockDmRepo, times(1)).findOne(1L);
//        verify(mockFoodRepo, times(1)).findOne(1L);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockDoRepo);
        verifyNoMoreInteractions(mockUserRepository);
//        verifyNoMoreInteractions(mockYumRepo);
        verifyNoMoreInteractions(mockDmRepo);
//        verifyNoMoreInteractions(mockFoodRepo);

        // we verify that we have the expected modified data
        order.getOrderItems().clear();
        item1.setQuantity(8);
        order.addOrderItem(item1);

        assertEquals(order, mockDoRepo.findOne(1L));

    }
    
    @Test
    @WithMockAuth(id = "2")
    public void testOrdersIdPut_invalidId() throws Exception {
        System.out.println("testOrdersIdPut_invalidId");

        OrderSave body = new OrderSave();
        body.setDailyMenuId(menu.getId());
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems(item1);
        item3.setQuantity(8);

        body.addOrderItemsItem(item3);

        given(mockDoRepo.findOne(1L)).willReturn(order);
//        given(mockYumRepo.findOne(1L)).willReturn(yum);

        mockMvc.perform(
                put("/api/orders/{id}", 4)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().is(410));

        // we verify the expected repo calls.
        verify(mockDoRepo, times(1)).findOne(4L);
//        verify(mockYumRepo, times(1)).findOne(1L);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockDoRepo);
//        verifyNoMoreInteractions(mockYumRepo);

        // we verify that we have the expected data
        assertEquals(order, mockDoRepo.findOne(1L));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_OrderIsFinal() throws Exception {
        System.out.println("testOrdersIdPut_OrderIsFinal");

        menu.setDate(LocalDate.now());

        OrderSave body = new OrderSave();
        body.setDailyMenuId(menu.getId());
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems(item1);
        item3.setQuantity(8);

        body.addOrderItemsItem(item3);

//        given(mockYumRepo.findOne(1L)).willReturn(yum);
        given(mockDoRepo.findOne(1L)).willReturn(order);
//        given(mockUserRepository.findOne(1L)).willReturn(logged);   //any(Long.class) 
//        given(mockDmRepo.findOne(1L)).willReturn(menu);
//        given(mockFoodRepo.findById(1L)).willReturn(food1);

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().isBadRequest());

        // we verify the expected repo calls.
        verify(mockDoRepo, times(1)).findOne(1L);
//        verify(mockYumRepo, times(1)).findOne(1L);

        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockDoRepo);
//        verifyNoMoreInteractions(mockYumRepo);

        // we verify that we have the expected data
        assertEquals(order, mockDoRepo.findOne(1L));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_invalidMenuId() throws Exception {
        System.out.println("testOrdersIdPut_invalidMenuId");

        OrderSave body = new OrderSave();
        body.setDailyMenuId(4L);
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems(item1);
        item3.setQuantity(8);

        body.addOrderItemsItem(item3);

//        given(mockYumRepo.findOne(1L)).willReturn(yum);
        given(mockDoRepo.findOne(1L)).willReturn(order);
        given(mockDmRepo.findOne(4L)).willReturn(null);
//        given(mockUserRepository.findOne(1L)).willReturn(logged);   //any(Long.class) 
//        given(mockFoodRepo.findById(1L)).willReturn(food1);

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().isNotFound());

        // we verify the expected repo calls.
//        verify(mockYumRepo, times(1)).findOne(1L);
        verify(mockDoRepo, times(1)).findOne(1L);
        verify(mockDmRepo, times(1)).findOne(4L);

        // we verify that we didnt call anything else on the repo
//        verifyNoMoreInteractions(mockYumRepo);
        verifyNoMoreInteractions(mockDoRepo);
        verifyNoMoreInteractions(mockDmRepo);

        // we verify that we have the expected data
        assertEquals(order, mockDoRepo.findOne(1L));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_ZeroQuantityOrderItem() throws Exception {
        System.out.println("testOrdersIdPut_ZeroQuantityOrderItem");

//        menu.setDate(LocalDate.now());
        OrderSave body = new OrderSave();
        body.setDailyMenuId(menu.getId());
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems(item1);
        item3.setQuantity(0);

        body.addOrderItemsItem(item3);

//        given(mockYumRepo.findOne(1L)).willReturn(yum);
        given(mockDoRepo.findOne(1L)).willReturn(order);
        given(mockDmRepo.findOne(1L)).willReturn(menu);
        given(mockUserRepository.findOne(1L)).willReturn(logged);   //any(Long.class)
        given(mockFoodRepo.findById(1L)).willReturn(food1);

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().isBadRequest());

        // we verify the expected repo calls.
//        verify(mockYumRepo, times(1)).findOne(1L);
        verify(mockDoRepo, times(1)).findOne(1L);
        verify(mockDmRepo, times(1)).findOne(1L);
        verify(mockUserRepository, times(1)).findOne(1L);
//        verify(mockFoodRepo, times(1)).findOne(1L);

        // we verify that we didnt call anything else on the repo
//        verifyNoMoreInteractions(mockYumRepo);
        verifyNoMoreInteractions(mockDoRepo);
        verifyNoMoreInteractions(mockDmRepo);
        verifyNoMoreInteractions(mockUserRepository);

        // we verify that we have the expected data
        assertEquals(order, mockDoRepo.findOne(1L));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_InvalidFood() throws Exception {
        System.out.println("testOrdersIdPut_InvalidFood");

        OrderSave body = new OrderSave();
        body.setDailyMenuId(menu.getId());
        body.setOrderVersion(menu.getVersion());

        OrderSaveOrderItems item3 = new OrderSaveOrderItems();
        item3.setFoodId(4L);
        item3.setQuantity(8);

        body.addOrderItemsItem(item3);

        given(mockDoRepo.findOne(1L)).willReturn(order);
        given(mockUserRepository.findOne(1L)).willReturn(logged);   //any(Long.class)
//        given(mockYumRepo.findOne(1L)).willReturn(yum);
        given(mockDmRepo.findOne(1L)).willReturn(menu);
        given(mockFoodRepo.findById(4L)).willReturn(new Food(4));

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString())
        ).andExpect(status().isNotFound());

        // we verify the expected repo calls.
//        verify(mockYumRepo, times(0)).findOne(1L);
        verify(mockDoRepo, times(1)).findOne(1L);
        verify(mockDmRepo, times(1)).findOne(1L);
        verify(mockUserRepository, times(1)).findOne(1L);
        verify(mockFoodRepo, times(1)).findById(4L);

        // we verify that we didnt call anything else on the repo
//        verifyNoMoreInteractions(mockYumRepo);
        verifyNoMoreInteractions(mockDoRepo);
        verifyNoMoreInteractions(mockDmRepo);
        verifyNoMoreInteractions(mockUserRepository);
        verifyNoMoreInteractions(mockFoodRepo);

        // we verify that we have the expected data
        assertEquals(order, mockDoRepo.findOne(1L));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testOrdersIdPut_emptyDTO() throws Exception {
        System.out.println("testOrdersIdPut_emptyDTO");

        OrderSave body = new OrderSave();
//        body.setDailyMenuId(menu.getId());
//        body.setVersion(menu.getVersion());       

        mockMvc.perform(
                put("/api/orders/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(body.toJsonString()) //"{}"
        ).andExpect(status().isBadRequest());

//        assertEquals(order, mockDoRepo.findOne(1L));
    }
}
