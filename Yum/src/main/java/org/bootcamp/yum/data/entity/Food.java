/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.joda.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "food")
public class Food {

    @Id
    @Column(name = "food_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "food_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private double price;

    @Column(name = "archived")
    private boolean archived;
    
    @Column(name = "popularity")
    private long popularity;

    @Version
    @Column(name = "version")
    private int version;

    @Column(name = "type")
    @Convert(converter = FoodTypeConverter.class)
    private FoodType foodType;

    @OneToMany(mappedBy = "food")
    private List<OrderItem> orderItems;

    @ManyToMany
    @JoinTable(
            name = "daily_menu_food",
            joinColumns = @JoinColumn(name = "food_id", referencedColumnName = "food_id"),
            inverseJoinColumns = @JoinColumn(name = "daily_menu_id", referencedColumnName = "daily_menu_id"))
    private List<DailyMenu> dailyMenus;

    public Food() {
    }

    public Food(long id) {
        this.id = id;
    }

    public void addOrderItem(OrderItem orderItem){
        if (this.orderItems == null){
            this.orderItems = new ArrayList<>();
        }
        this.orderItems.add(orderItem);
    }
    
    public void addDailyMenu(DailyMenu dailyMenu){
        if (this.dailyMenus == null){
            this.dailyMenus = new ArrayList<>();
        }
        this.dailyMenus.add(dailyMenu);
    }
    
    public void setId(long id) {
        this.id = id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public List<DailyMenu> getDailyMenus() {
        return dailyMenus;
    }

    public List<DailyMenu> getOldDailyMenus() {
        List<DailyMenu> oldMenus = new ArrayList<>();

        for (DailyMenu dailyMenu : dailyMenus) {
            if (dailyMenu.getDate().isBefore(LocalDate.now())) {
                oldMenus.add(dailyMenu);
            }
        }
        return oldMenus;
    }

    public void setDailyMenus(List<DailyMenu> dailyMenus) {
        this.dailyMenus = dailyMenus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = Math.round(price * 100.0) / 100.0;
    }

    public boolean isArchived() {
        return archived;
    }

    public long getPopularity() {
        return popularity;
    }

    public void setPopularity(long popularity) {
        this.popularity = popularity;
    }

    public void addPopularity(long popularity) {
        this.popularity += popularity;
    }
    
    /*
        sets archived value and removes food from future daily menus
    */
    public void setArchived(boolean archived) {
        this.archived = archived;
    }
    
    

    public long getId() {
        return id;
    }

    public FoodType getFoodType() {

        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isOrdered() {

        return orderItems.size() > 0;

    }

    @Override
    public String toString() {
        return "Food{" + "id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + ", archived=" + archived + ", foodType=" + foodType + "}\n";
    }

  

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 61 * hash + Objects.hashCode(this.name);
        hash = 61 * hash + Objects.hashCode(this.description);
        hash = 61 * hash + (int) (Double.doubleToLongBits(this.price) ^ (Double.doubleToLongBits(this.price) >>> 32));
        hash = 61 * hash + (this.archived ? 1 : 0);
        hash = 61 * hash + this.version;
        hash = 61 * hash + Objects.hashCode(this.foodType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Food other = (Food) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.price) != Double.doubleToLongBits(other.price)) {
            return false;
        }
        if (this.archived != other.archived) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (this.foodType != other.foodType) {
            return false;
        }
        return true;
    }

}
