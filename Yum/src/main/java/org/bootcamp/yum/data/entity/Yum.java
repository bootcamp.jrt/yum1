/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.data.entity;

import org.joda.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.bootcamp.yum.api.model.Settings;

@Entity
@Table(name="yum")
public class Yum {
    
    @Id
    @Column(name="id")
    private Long id;
    
    @Column(name="terms")
    private String terms;
    
    @Column(name="privacy")
    private String privacy;
    
    @Column(name="notes")
    private String notes;
    
    @Column(name="deadline")
    @Convert(converter = TimeConverter.class)
    private LocalTime deadline;
    
    @Column(name="currency")
    private String currency;

    @Column(name="foods_version")
    private int foodsVersion;

//    @Version
    @Column(name="version")
    private int version;

    public Yum() { 
        setTerms("Big text of terms");
        setCurrency("Euro");
        setDeadline("12:00:00");
        setNotes("Big text of notes");
        setPrivacy("Big text of privacy");
        setFoodsVersion(1);
    }

    public Yum(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalTime deadline) {
        this.deadline = deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = LocalTime.parse(deadline);
    }
    
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }   

    public int getFoodsVersion() {
        return foodsVersion;
    }

    public void setFoodsVersion(int foodsVersion) {
        this.foodsVersion = foodsVersion;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    public void updateSettings(Settings settings){
        this.setCurrency(settings.getCurrency());
        this.setDeadline(settings.getDeadline());
        this.setNotes(settings.getNotes());
        this.setPrivacy(settings.getPolicy());
        this.setTerms(settings.getTerms());
    }
    
}
