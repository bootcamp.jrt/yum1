/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderItemPK implements Serializable {
    
    @Column(name="daily_order_id", nullable = false)
    private long dailyOrderId;

    @Column(name="food_id", nullable = false)
    private long foodId;
    
    public long getDailyOrderId() {
        return dailyOrderId;
    }

    public void setDailyOrderId(long dailyOrderId) {
        this.dailyOrderId = dailyOrderId;
    }

    public long getFoodId() {
        return foodId;
    }

    public void setFoodId(long foodId) {
        this.foodId = foodId;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof OrderItemPK){
            OrderItemPK orderItemPK = (OrderItemPK)obj;
            return orderItemPK.dailyOrderId== dailyOrderId && orderItemPK.foodId==foodId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(dailyOrderId)+Long.hashCode(foodId);
    }

    @Override
    public String toString() {
        return "CompositeKey{" + dailyOrderId + ',' + foodId + '}';
    }
    
}
