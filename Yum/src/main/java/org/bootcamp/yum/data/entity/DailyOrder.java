/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;


@Entity
@Table(name="daily_order")
public class DailyOrder {
    
    @Id
    @Column(name="daily_order_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(name="final")
    private boolean isFinal;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="daily_menu_id")
    private DailyMenu dailyMenu;

    @OneToMany(mappedBy="dailyOrder", orphanRemoval=true, cascade = CascadeType.ALL)
    private List<OrderItem> orderItems ;
    
    @Version
    @Column(name="version")
    private int version;

    public DailyOrder() {
        isFinal = false;
    }

    public DailyOrder(long id) {
        this.id = id;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public boolean isFinal(LocalTime deadline) {
        
        if(isFinal != true){
            LocalDateTime d = dailyMenu.getDate().minusDays(1).toLocalDateTime(deadline);
            LocalDateTime now = LocalDateTime.now();
            if(now.isAfter(d)){
                isFinal = true;
            }
        }
        
        return isFinal;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DailyMenu getDailyMenu() {
        return dailyMenu;
    }

    public void setDailyMenu(DailyMenu dailyMenu) {
        this.dailyMenu = dailyMenu;
    }

    public List<OrderItem> getOrderItems() {
        if (orderItems == null)
            orderItems = new ArrayList<>();
        
        return orderItems;
    }
    
    public void addOrderItem(OrderItem orderItem){
        if(orderItems == null)
           orderItems = new ArrayList<>();
        
        orderItems.add(orderItem);
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return "DailyOrder{" + "id=" + id + ", isFinal=" + isFinal + ", orderItems=" + orderItems + "}\n";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + (this.isFinal ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DailyOrder other = (DailyOrder) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.isFinal != other.isFinal) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return true;
    }
    
    
    
}
