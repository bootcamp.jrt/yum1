/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.data.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.joda.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="daily_menu")
public class DailyMenu {
    
    @Id
    @Column(name="daily_menu_id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Column(name="date")
    @Convert(converter=DateConverter.class)
    private LocalDate date;

    @OneToMany(mappedBy="dailyMenu")
    private List<DailyOrder> dailyOrders;
   
    @ManyToMany
    @JoinTable(
        name="daily_menu_food",
        joinColumns=@JoinColumn(name="daily_menu_id", referencedColumnName="daily_menu_id"),
        inverseJoinColumns=@JoinColumn(name="food_id", referencedColumnName="food_id"))
    private List<Food> foods;
    
    @Version
    @Column(name="version")
    private int version;

    public DailyMenu() {
    }

    public DailyMenu(long id) {
        this.id = id;
    }
    
    public long getId() {
        return id;
    }

    public List<DailyOrder> getDailyOrders() {
        return dailyOrders;
    }

    public void setDailyOrders(List<DailyOrder> dailyOrders) {
        this.dailyOrders = dailyOrders;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    public void addFood(Food food){
        if(foods == null)
            foods = new ArrayList<>();
        
        foods.add(food);
    }
    
    public void addDailyOrder(DailyOrder order){
        if(dailyOrders == null)
            dailyOrders = new ArrayList<>();
        
        dailyOrders.add(order);
    }
    
    @Override
    public String toString() {
        return "DailyMenu{" + "id=" + id + ", date=" + date +", foods=" + foods + ", dailyOrders=" + dailyOrders + "}\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 31 * hash + Objects.hashCode(this.date);
        hash = 31 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DailyMenu other = (DailyMenu) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
    
    
}
