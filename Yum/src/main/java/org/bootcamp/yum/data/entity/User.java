/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.data.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.multipart.MultipartFile;

import static javax.persistence.FetchType.LAZY;
import javax.imageio.ImageIO;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import java.awt.image.BufferedImage;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import org.bootcamp.yum.api.model.UserSave;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "role")
    @Convert(converter = UserRoleConverter.class)
    private UserRole role;

    @Column(name = "password")
    private String password;

    @Column(name = "registration_date")
    @Convert(converter = DateConverter.class)
    private LocalDate registrationDate;

    @Basic(fetch = LAZY)
    @Lob
    @Column(name = "picture")
    private byte[] picture;

    @Column(name = "approved")
    private Boolean approved;

//    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Column(name = "email")
    private String email;

    @Version
    @Column(name = "version")
    private int version;

    @Column(name = "secret")
    private String secret;

    @Column(name = "secret_expiration_time")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime secretExpirationTime;

    @OneToMany(mappedBy = "user")
    private List<DailyOrder> dailyOrders;

    public List<DailyOrder> getDailyOrders() {
        return dailyOrders;
    }

    public void setDailyOrders(List<DailyOrder> dailyOrders) {
        this.dailyOrders = dailyOrders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getPicture() {
        //if the pic was never set db send an array of 3 bytes as broken image
        if (this.picture == null || this.picture.length < 10) {
            return null;
        }
        return picture;
    }
    
    public boolean hasPicture(){
        if (this.picture == null || this.picture.length < 10) {
            return false;
        }
        return true;
    }

    public void setPicture(MultipartFile file) throws IOException {
        if (file == null) {
            this.picture = null;
        } else {
            byte[] bytes = file.getBytes();

            InputStream in = new ByteArrayInputStream(bytes);
            BufferedImage image = ImageIO.read(in);
            int min = 0;
            if (image.getWidth() > image.getHeight()) {
                min = image.getHeight();
            } else {
                min = image.getWidth();
            }

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Thumbnails.of(image)
                    .sourceRegion(Positions.CENTER, min, min)
                    .size(180, 180)
                    .outputFormat("jpeg")
                    .toOutputStream(output);

            //save this in the DAO
            bytes = output.toByteArray();
            this.picture = bytes;
        }
    }

    public Boolean isApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean validatePassword(String password) {
        return BCrypt.checkpw(password, this.password);
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret() {
        this.secret = UUID.randomUUID().toString();
        this.secretExpirationTime = LocalDateTime.now().plusHours(12);
    }

    public void dropSecret() {
        this.secret = null;
        this.secretExpirationTime = null;
    }

    public LocalDateTime getSecretExpirationTime() {
        return secretExpirationTime;
    }

    // set the default values when a user is created
    public User() {
        this.setRegistrationDate(LocalDate.now());
        this.setRole(UserRole.HUNGRY);
        this.setApproved(false);
    }

    public User(UserSave newUser) {
        this();
        this.setEmail(newUser.getEmail());
        this.setPassword(newUser.getPassword());
        this.setFirstName(newUser.getFirstName());
        this.setLastName(newUser.getLastName());
    }

    public UserOrdersState getOrdersCondition(LocalTime deadline) {

        boolean noPastOrders = true;
        boolean noFutureOrders = true;

        for (DailyOrder dailyOrder : this.dailyOrders) {

            if (dailyOrder.isFinal(deadline)) {

                noPastOrders = false;

            } else {

                noFutureOrders = false;

            }

            if (!(noPastOrders || noFutureOrders)) {
                break;
            }
        }

        if (noPastOrders == false && noFutureOrders == false) {

            return UserOrdersState.MIXORDERS;

        } else if (noPastOrders == false) {

            return UserOrdersState.PASTONLY;

        } else if (noFutureOrders == false) {

            return UserOrdersState.FUTUREONLY;

        } else {

            return UserOrdersState.NOORDERS;

        }

    }

    public void addDailyOrder(DailyOrder dailyOrder) {
        if (this.dailyOrders == null) {
            this.dailyOrders = new ArrayList<>();
        }
        this.dailyOrders.add(dailyOrder);
    }

    public User(Long id) {
        this.id = id;
    }

    public void updateUser(org.bootcamp.yum.api.model.User body) {
        email = body.getEmail();
        firstName = body.getFirstName();
        lastName = body.getLastName();

        UserRoleConverter uc = new UserRoleConverter();

        role = uc.convertToEntityAttribute(body.getRole());
        approved = body.getApproved();

        version++;
    }

    public static boolean validateSortField(String sort) {

        ArrayList<String> validFields = new ArrayList<>();
        validFields.add("id");
        validFields.add("firstName");
        validFields.add("lastName");
        validFields.add("role");
        validFields.add("registrationDate");
        validFields.add("approved");

        return validFields.contains(sort);
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", role=" + role + ", password=" + password + ", registrationDate=" + registrationDate + ", picture=" + picture + ", approved=" + approved + ", email=" + email + ", dailyOrders=" + dailyOrders + "}\n";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.firstName);
        hash = 53 * hash + Objects.hashCode(this.lastName);
        hash = 53 * hash + Objects.hashCode(this.role);
        hash = 53 * hash + Objects.hashCode(this.password);
        hash = 53 * hash + Objects.hashCode(this.registrationDate);
        hash = 53 * hash + Objects.hashCode(this.picture);
        hash = 53 * hash + Objects.hashCode(this.approved);
        hash = 53 * hash + Objects.hashCode(this.email);
        hash = 53 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.version != other.version) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.picture, other.picture)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.role != other.role) {
            return false;
        }
        if (!Objects.equals(this.registrationDate, other.registrationDate)) {
            return false;
        }
        if (!Objects.equals(this.approved, other.approved)) {
            return false;
        }
        return true;
    }

}
