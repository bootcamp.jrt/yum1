/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.data.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

@Entity
public class OrderItem {

    @EmbeddedId
    private OrderItemPK id;

    @ManyToOne
    @JoinColumn(name="food_id", insertable = false, updatable = false)
    private Food food;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="daily_order_id", insertable = false, updatable = false)
    private DailyOrder dailyOrder;

    @Column(name="quantity")
    private int quantity;

    public OrderItem() {
    }

    public OrderItem(OrderItemPK id) {
        this.id = id;
    }
    
    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public DailyOrder getDailyOrder() {
        return dailyOrder;
    }

    public void setDailyOrder(DailyOrder dailyOrder) {
        this.dailyOrder = dailyOrder;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public OrderItemPK getId() {
        return id;
    }

    public void setId(OrderItemPK id) {
        this.id = id;
    }
    
    @PrePersist
    private void prePersist(){
        OrderItemPK oiPk = new OrderItemPK();
        oiPk.setDailyOrderId(dailyOrder.getId());
        oiPk.setFoodId(food.getId());
            
        this.setId(oiPk);
    }
    
    @Override
    public String toString() {
        return "OrderItem{" + "id=" + id + ", food=" + food + ", quantity=" + quantity + "}\n";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.food);
        hash = 29 * hash + Objects.hashCode(this.dailyOrder);
        hash = 29 * hash + this.quantity;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderItem other = (OrderItem) obj;
        if (this.quantity != other.quantity) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.food, other.food)) {
            return false;
        }
        if (!Objects.equals(this.dailyOrder, other.dailyOrder)) {
            return false;
        }
        return true;
    }
    
    

}
