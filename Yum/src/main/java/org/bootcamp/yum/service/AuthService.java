/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.service;

import java.util.ArrayList;

import javax.transaction.Transactional;
import org.joda.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.JwtCodec;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.ChangePwd;
import org.bootcamp.yum.api.model.ForgotPwd;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Policy;
import org.bootcamp.yum.api.model.Token;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class AuthService {
    
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private GlobalSettingsService yum;
    
    @Autowired
    private MailService mailService;
    
    @Transactional
    public void authChangepwdPost(ChangePwd body) throws ApiException {
        User user = null;

        // check if the new password has at least 6 characters
        if(body.getNewPassword().length() < 6){
            throw new ApiException(400, "Password must be at least 6 characters");
        }
        
        if(body.getSecret()!=null){
            // fetch in the database the user by secret
            user = userRepo.findBySecret(body.getSecret());
        }
        // check if user exists
        if(user == null){
            throw new ApiException(400, "Link has expired");
        }
        
        if (user.getSecretExpirationTime().isBefore(new LocalDateTime())){
            user.dropSecret();
            throw new ApiException(400, "Link has expired");
        }
        
        
        // new password != old password => update the password
        // else do nothing (controller will still return 200)
        if(!user.validatePassword(body.getNewPassword())){
         
            user.setPassword(body.getNewPassword());
         
        }
        user.dropSecret();
    }
    
    @Transactional
    public void authForgotpwdPost(ForgotPwd body) throws ApiException {
        User user = userRepo.findByEmail(body.getEmail());
        
        if (user == null){
            throw new ApiException (400, "Email does not exist.");
        }
        
        if(!user.isApproved()){
            throw new ApiException (400, "User is not approved yet.");
        }
            
        user.setSecret();
        

        mailService.sendResetPasswordLinkEmail(user);
        
    }
    

    @Transactional
    public Token authLoginPost(Login body) throws ApiException {

        // fetch in the database the user
        User user = userRepo.findByEmail(body.getEmail());

        // verify its password.. use bcrypt for password hashing (implemented in User Entity)
        // also verify that the user exists and he/she is approved!
        
        if(user == null || !user.isApproved() || !user.validatePassword(body.getPassword())){
            throw new ApiException(403, "Invalid credentials");
        }
       
        // roles is an array of string:
        ArrayList<String> roles = new ArrayList<>();
        
        // if user exists in system then roles should be "hungry"
        // if user.role == "chef" or "admin" then add the string "chef" to the array.
        // if user.role == "admin" then add "admin" to the array.
        roles.add("hungry");
        if(user.getRole().equals(UserRole.CHEF)){
            roles.add("chef");
        }else if(user.getRole().equals(UserRole.ADMIN)){
            roles.add("chef");
            roles.add("admin");
        }

        // the subject should be the ID of the user converted to string
          String subject = user.getId().toString();
        
        String compactJws = JwtCodec.encode(subject, roles);
                
        Token token = new Token();
        token.setToken(compactJws);
        token.setId(user.getId());
        token.setRole(user.getRole().toString());
        token.setFirstName(user.getFirstName());
        token.setLastName(user.getLastName());
        if ( user.getPicture()!=null ){
            token.setHasPicture(true);
        }

        return token;
    }
    
    public Policy authPolicyGet() {
        Policy policy = new Policy();
        policy.setPolicy(yum.getSettings().getPrivacy());
        policy.setTerms(yum.getSettings().getTerms());
        return policy;
    }

    @Transactional
    public void authRegisterPost(UserSave body) throws ApiException {

        // check if email is valid and if the password has atleast 6 characters
        if(!body.hasValidEmail() && !body.hasValidPassword()){
            throw new ApiException(400, "Bad request: the email and the password are invalid");
        }

        if(!body.hasValidEmail()){
            throw new ApiException(400, "Bad request: the email is invalid");
        }

        if(!body.hasValidPassword()){
            throw new ApiException(400, "Bad request: the password is invalid");
        }
        
        // check if the email given is already registered in an existing user
        User existingUser = userRepo.findByEmail(body.getEmail());
        if(existingUser != null){
            throw new ApiException(409, "User already exists");
        }
        
        // if everything is ok create the new user and return OK
        userRepo.save(new User(body));
    }

}
