/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import java.io.IOException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException ;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.bootcamp.JwtCodec;
import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.Approve;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.api.model.UsersList;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserOrdersState;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.entity.UserRoleConverter;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class UsersService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private DailyOrderRepository dailyOrderRepo;

    @Autowired
    private GlobalSettingsService yumService;

    @Autowired
    private MailService mailService;

    private User getUser() throws ApiException {
        return userRepo.findOne((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public UsersList usersGet(String sort, String order, Integer page, Integer size) {

        // direction of sorting should be DESC by default. It will change to ASC if the user requests it
        Direction direction = Direction.DESC;
        if (order.equalsIgnoreCase("asc")) {
            direction = Direction.ASC;
        }

        // validate sort field
        if (!User.validateSortField(sort)) {
            sort = "id";
        }

        // pagination construction
        Pageable pageable = new PageRequest(page, size, direction, sort);

        Page<User> users = userRepo.findAll(pageable);

        UsersList dtoUsers = new UsersList();

        for (User user : users) {

            dtoUsers.getUsers().add(new org.bootcamp.yum.api.model.User(user));

        }

        dtoUsers.setTotalItems(userRepo.count());

        return dtoUsers;
    }

    @Transactional
    public void usersIdApprovedPut(Long id, Approve approve) throws ApiException {

        //The superAdmin (id=1) is not allowed
        if (id == 1) {

            throw new ApiException(400, "SuperAdmin cannot be modified ");
        }
        //changing approval to the admin himself 
        if (getUser().getId() == id) {

            throw new ApiException(400, "Admin cannot be modified by himself");
        }
        User user = userRepo.findOne(id);

        if (user == null) {

            throw new ApiException(404, "User not found");

        } else {
            if (approve.getApprove() == true) {

                user.setApproved(approve.getApprove());
                mailService.sendUserApprovedEmailToUser(user);

            } else {

                UserOrdersState state = user.getOrdersCondition(yumService.getSettings().getDeadline());
                switch (state) {

                    case NOORDERS:
                    case PASTONLY:
                        user.setApproved(approve.getApprove());
                        break;
                    case MIXORDERS:
                    case FUTUREONLY:
                        if (approve.getForce()) {

                            deleteFutureOrders(user);

                            user.setApproved(Boolean.FALSE);

                        } else {

                            throw new ApiException(409, "User has orders in the future");

                        }
                }

            }

        }
    }

    public void deleteFutureOrders(User user) {

        for (DailyOrder dailyOrder : user.getDailyOrders()) {

            if (!dailyOrder.isFinal(yumService.getSettings().getDeadline())) {

                dailyOrderRepo.delete(dailyOrder);

            }
        }

    }

    @Transactional
    public void usersIdDelete(Long id, Boolean force) throws ApiException {

        //superAdmin (id=1) cannot be deleted
        if (id == 1) {

            throw new ApiException(400, "SuperAdmin cannot be deleted");
        }

        //delete the admin himself is not allowed
        if (getUser().getId() == id) {

            throw new ApiException(400, "Admin cannot be deleted by himself");
        }

        User toDelete = userRepo.findOne(id);

        if (toDelete == null) {

            throw new ApiException(404, "User not found");

        } else {

            UserOrdersState state = toDelete.getOrdersCondition(yumService.getSettings().getDeadline());
            switch (state) {

                case NOORDERS:
                    userRepo.delete(toDelete);

                    break;

                case PASTONLY:

                    throw new ApiException(412, "User has orders in the past");

                case MIXORDERS:

                    throw new ApiException(402, "User has orders in the future and in the past");

                case FUTUREONLY:
                    if (force != null && force) {

                        deleteFutureOrders(toDelete);

                        userRepo.delete(toDelete);

                    } else {

                        throw new ApiException(409, "User has orders in the future");

                    }
                    break;

            }
        }

    }

    @Transactional
    public void usersIdForgotpwdPost(Long id) throws ApiException {
        User user = userRepo.findOne(id);

        if (user == null) {
            throw new ApiException(400, "User not found.");
        }
        
        if(!user.isApproved()){
            throw new ApiException (400, "User is not approved yet.");
        }

        user.setSecret();

        mailService.sendResetPasswordLinkEmail(user);

    }

    public org.bootcamp.yum.api.model.User usersIdGet(Long id) throws ApiException {

        User toGet = userRepo.findOne(id);

        if (toGet == null) {
            throw new ApiException(404, "User not found");
        } else {
            org.bootcamp.yum.api.model.User dtoUser = new org.bootcamp.yum.api.model.User(toGet);
            return dtoUser;
        }
    }

    public byte[] usersIdPictureTokenGet(Long id, String token) throws ApiException {
        try {
            
            Claims claims = JwtCodec.decode(token);
            User admin = userRepo.findOne(Long.parseLong(claims.getSubject()));
            
            if (admin.getRole() != UserRole.ADMIN){
                throw new ApiException(403, "Token does not belong to admin");
            }
            
            User user = userRepo.findOne(id);
            if (user == null) {
              
                throw new ApiException(404, "User not found");
            }
            
            if (user.getPicture() == null){
                throw new ApiException(404, "Picture not found");
            }
            return user.getPicture();
        } catch (JwtException  ex) {
            
            throw new ApiException(403, "Token invalid");

        }
    }
    
    @Transactional 
    public void usersIdPictureDelete(Long id) throws ApiException{
    
        try {
             
            User user = userRepo.findOne(id);
            if (user == null) {

                throw new ApiException(404, "User not found");
            }
            
            user.setPicture(null);
        } catch (IOException ex) {
            
            throw new ApiException(400, "Picture could not be deleted");
        }
        
    }

    @Transactional
    public void usersIdPicturePost(Long id, MultipartFile file) throws ApiException {
        User user = userRepo.findOne(id);

        if (user == null) {
            throw new ApiException(404, "User not found");
        }
        if (!file.isEmpty()) {
            try {

                user.setPicture(file);

            } catch (IOException ex) {
                throw new ApiException(400, "Problem rendering the picture.");
            }
        } else {

            throw new ApiException(400, "Picture file is empty.");
        }

    }

    @Transactional
    public void usersIdPut(Long id, org.bootcamp.yum.api.model.User body) throws ApiException {

        User toEdit = userRepo.findOne(id);

        if (getUser().equals(toEdit) && !body.getRole().equals("admin")) {
            throw new ApiException(400, "An admin cannot change his own role");
        }

        //User does not exist
        if (toEdit == null) {
            throw new ApiException(404, "User not found");
        }

        //Concurrent modification error
        if (body.getVersion() != toEdit.getVersion()) {
            throw new ApiConcurrentModificationException(new org.bootcamp.yum.api.model.User(toEdit));
        }
        //Not valid email address 
        if (!body.hasValidEmail()) {
            throw new ApiException(400, "Not valid email address");
        }
        //New email already in use
        if (!body.getEmail().equals(toEdit.getEmail()) && userRepo.findByEmail(body.getEmail()) != null) {
            throw new ApiException(400, "New email already in use");
        }

        //Not updated user
        if (body.getEmail().equals(toEdit.getEmail())
                && body.getFirstName().equals(toEdit.getFirstName())
                && body.getLastName().equals(toEdit.getLastName())
                && body.getRole().equals(toEdit.getRole().toString())
                && body.getApproved().equals(toEdit.isApproved())) {

            throw new ApiException(400, "No changes detected");
        }

        toEdit.updateUser(body);

    }

    @Transactional
    public void usersPost(UserSave body) throws ApiException {

        User newUser = userRepo.findByEmail(body.getEmail());

        if (newUser != null) {
            throw new ApiException(409, "Given e-mail already exists");
        } else {
            newUser = new User(body);
            newUser.setRole(new UserRoleConverter().convertToEntityAttribute(body.getRole()));
            userRepo.save(newUser);
            mailService.sendNewUserEmailToAllAdmins(newUser);
        }

    }


}
