/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exception.ApiConcurrentCreationException;
import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.EditedFood;
import org.bootcamp.yum.api.model.FoodDelete;
import org.bootcamp.yum.api.model.FoodEditable;
import org.bootcamp.yum.api.model.FoodSave;
import org.bootcamp.yum.api.model.FoodsList;
import org.bootcamp.yum.api.model.NewEntry;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.entity.FoodTypeConverter;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class FoodsService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private FoodRepository foodRepo;

    @Autowired
    private GlobalSettingsService yum;

    private User getUser() {
        return userRepo.findOne((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public EditedFood foodsFindByNameNameGet(String name) throws ApiException {
        Food food = foodRepo.findByNameAndArchived(name, false);
        //name exists and is not archived
        if (food != null) {
            //return new ResponseEntity<EditedFood>(new EditedFood(food), HttpStatus.OK);
            throw new ApiConcurrentCreationException(new EditedFood(food));
        } else {
            return new EditedFood();
        }
    }

    public FoodsList foodsGet(Integer version, Boolean archived, String type, String sort, String order, Integer page, Integer size) throws ApiException {

        FoodsList foods = new FoodsList();

        if (version != null) {

            // requested all the foods
            // check for if the client is up to date and if not send the new version
            int foodsVersion = yum.getSettings().getFoodsVersion();

            if (foodsVersion == version) {
                throw new ApiException(304, "List not modified");
            }

            Iterable<Food> foodList = foodRepo.findAll();

            // fill the json
            foods.setVersion(foodsVersion);
            foods.setTotalItems(foodRepo.count());
            for (Food food : foodList) {
                foods.addFoodsItem(new org.bootcamp.yum.api.model.Food(food));
            }

        } else {

            // requested list for page
            // set defaults if needed
            if (archived == null) {
                archived = false;
            }
            if (type == null || !(type.equals("mainDish") || type.equals("salad") || type.equals("drink"))) {
                type = "all";
            }
            if (sort == null || !(sort.equals("price") || sort.equals("popularity"))) {
                sort = "id";
            }
            if (order == null || !order.equalsIgnoreCase("asc")) {
                order = "desc";
            }
            if (page == null || page < 0) {
                page = 0;
            }
            if (size == null || size < 1) {
                size = 1;
            }

            FoodType foodType = new FoodTypeConverter().convertToEntityAttribute(type);
            Pageable pageable = new PageRequest(page, size, Direction.fromString(order), sort);

            // if there is no specified type of food the list will contain all types
            Page<Food> foodList;
            if (foodType == null) {
                foodList = foodRepo.findByArchived(archived, pageable);
                foods.setTotalItems(foodRepo.countByArchived(archived));
            } else {
                foodList = foodRepo.findByFoodTypeAndArchived(foodType, archived, pageable);
                foods.setTotalItems(foodRepo.countByFoodTypeAndArchived(foodType, archived));
            }

            // fill the json
            for (Food food : foodList) {
                foods.addFoodsItem(new org.bootcamp.yum.api.model.Food(food));
            }
        }

        return foods;
    }

    @Transactional
    public FoodDelete foodsIdDelete(Long id, Boolean archive) throws ApiException {
        FoodDelete foodDelete = new FoodDelete();
        Food food = foodRepo.findById(id);

        //check if id exists
        if (food == null) {
            throw new ApiException(404, "Food not found");
        } else {
            if (archive == null) {
                //user demands to delete
                if (!food.getOrderItems().isEmpty() || !food.getDailyMenus().isEmpty()) {
                    //food has been ordered and cannot be deleted                   
                    throw new ApiException(406, "Food cannot be deleted");
                } else {
                    foodRepo.deleteById(id);
                    foodDelete.setDeleted(true);
                    yum.incrementFoodsVersion();

                }
            } else {
                //user demands to archive - food gets archived and removed from future dMenus
                food.setArchived(true);
                foodDelete.setDeleted(false);
                yum.incrementFoodsVersion();

            }
        }

        return foodDelete;
    }

    public FoodEditable foodsIdGet(Long id, Boolean edit) throws ApiException {
        Food food = foodRepo.findById(id);

        if (food == null) {
            //return new ResponseEntity<FoodEditable>(HttpStatus.NOT_FOUND);
            throw new ApiException(404, "Food not found");

        } else {
            EditedFood eFood = new EditedFood(food);
            FoodEditable fe = new FoodEditable();

            fe.setEditedFood(eFood);
            fe.setEditable(!food.isOrdered());

            return fe;
        }
    }

    @Transactional
    public NewEntry foodsIdPut(Long id, EditedFood body, Boolean clone) throws ApiException {
        Food food = foodRepo.findById(id);

        if (clone == null) {
            clone = false;
        }

        if (food == null) {
            //return new ResponseEntity<EditedFood>(HttpStatus.NOT_FOUND);
            throw new ApiException(404, "Food not found");
        }

        if (food.getVersion() != body.getVersion()) {
            //return new ResponseEntity<EditedFood>(new EditedFood(food), HttpStatus.CONFLICT);
            throw new ApiConcurrentModificationException(new EditedFood(food));
        }

        if (food.isArchived() == true) {
            throw new ApiException(400, "Food is archived can not be edited");
        }

        FoodTypeConverter converter = new FoodTypeConverter();

        // check if food is updated when it cannot be updated (hacked)
        if (!clone
                && !food.getOrderItems().isEmpty()
                && (!food.getName().equals(body.getName())
                || food.getPrice() != body.getPrice()
                || !food.getFoodType().equals(converter.convertToEntityAttribute(body.getType())))) {
            //return new ResponseEntity<EditedFood>(HttpStatus.NOT_ACCEPTABLE);
            throw new ApiException(406, "Not acceptable - conflict");
        }

        if (clone == true) {
            food.setArchived(true);
            Food foodArchived = new Food();

            foodArchived.setName(body.getName());
            foodArchived.setDescription(body.getDescription());
            foodArchived.setPrice(body.getPrice());
            foodArchived.setFoodType(converter.convertToEntityAttribute(body.getType()));

            //foodRepo.save(foodArchived);
            NewEntry newFood = new NewEntry();
            newFood.setId(foodRepo.save(foodArchived).getId());
            newFood.setVersion(0);
            
            yum.incrementFoodsVersion();

            return newFood;
        } else {

            food.setName(body.getName());
            food.setDescription(body.getDescription());
            food.setPrice(body.getPrice());
            food.setFoodType(converter.convertToEntityAttribute(body.getType()));

            yum.incrementFoodsVersion();

            NewEntry dto = new NewEntry();
            dto.setId(food.getId());
            dto.setVersion(food.getVersion() + 1);

            return dto;

        }

    }

    @Transactional
    public void foodsPost(FoodSave body) throws ApiException {

        FoodTypeConverter foodtypeconverter = new FoodTypeConverter();
        FoodType foodType = foodtypeconverter.convertToEntityAttribute(body.getType());

        //wrong price
        if (body.getPrice() < 0) {
            throw new ApiException(400, "Price is below zero");
        }

        //converter fails
        if (foodType == null) {
            throw new ApiException(400, "Food type is null");
        }

        //name exists and is not archived
        if (foodRepo.findByNameAndArchived(body.getName(), false) != null) {
            throw new ApiException(406, "food name already exists");
        }

        Food food = new Food();
        food.setName(body.getName());
        food.setDescription(body.getDescription());
        food.setPrice(body.getPrice());
        food.setFoodType(foodType);
        foodRepo.save(food);
        yum.incrementFoodsVersion();

    }

}
