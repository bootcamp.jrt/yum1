/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import org.bootcamp.yum.exception.ApiConcurrentCreationException;
import org.bootcamp.yum.exception.ApiConcurrentDeletionException;
import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.NewEntry;
import org.bootcamp.yum.api.model.OrderSave;
import org.bootcamp.yum.api.model.OrderSaveOrderItems;
import org.bootcamp.yum.api.model.OrderSummary;
import org.bootcamp.yum.api.model.OrderSummaryDailyOrders;
import org.bootcamp.yum.api.model.OrdersChef;
import org.bootcamp.yum.api.model.OrdersChefInner;
import org.bootcamp.yum.api.model.OrdersChefInnerFoods;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class OrdersService {
    
    @Autowired
    private FoodRepository foodRepo;
    
    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private DailyMenuRepository dmRepo;
    
    @Autowired
    private DailyOrderRepository doRepo;
    
    @Autowired
    private OrderItemRepository oiRepo;
    
    @Autowired
    private GlobalSettingsService yumService;
    
    @Autowired
    private MailService mailService;
    
    private User getUser(){
        return userRepo.findOne((Long)SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public void error410( Long menuId, LocalDate date, Integer menuVersion ) throws ApiException {
        // menu = get menu from repo by its Id.
        DailyMenu menu = dmRepo.findById(menuId);
        if (menu != null) {
            // The menu still exists.
            // newOrder = Is there a new order for that menu (for this user)?
            DailyOrder newOrder = doRepo.findByUserAndDailyMenu(getUser(), menu);
            if (newOrder != null) {
                // ConcurrentDeletionException "You cancelled this order earlier, and placed again. Here is the new one"
                // the DTO to return is from the newOrder and menu
                String error = "You cancelled this order earlier, and placed again. Here is the new one";
                org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(newOrder,yumService.getSettings().getDeadline());
                throw new ApiConcurrentDeletionException(error, dto);
            } else {
                // Has the menu changed? compare the menuVersion you got with the one in the menu
                if (menuVersion != menu.getVersion()){
                    // ConcurrentDeletionException "You cancelled this order earlier, and the menu has changed"
                    // the DTO will not contain any order (orderId = 0)
                    String error = "You cancelled this order earlier, and the menu has changed";
                    org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(menu, yumService.deadLineHasPassedFor(menu.getDate()));
                    throw new ApiConcurrentDeletionException(error, dto);
                } else {
                    // ConcurrentDeletionException "You cancelled this order earlier"
                    // the DTO will not contain any order (orderId = 0)
                    String error = "You cancelled this order earlier";
                    org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(menu, yumService.deadLineHasPassedFor(menu.getDate()));
                    throw new ApiConcurrentDeletionException(error, dto);
                }
            }
        } else {
            // The menu does not exist anymore
            // newMenu = search for a new menu for the same day
            DailyMenu newMenu = dmRepo.findByDate(date);
            if (newMenu != null){
                // ConcurrentDeletionException "You cancelled this order earlier, and the menu has changed"
                // the DTO to return is from the newMenu
                String error = "You cancelled this order earlier, and the menu has changed";
                org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(newMenu, yumService.deadLineHasPassedFor(menu.getDate()));
                throw new ApiConcurrentDeletionException(error, dto);
            } else {
                // ConcurrentDeletionException "You cancelled this order earlier, and no menu is proposed anymore"
                // the DTO to return will not contain any order nor any menu
                String error = "You cancelled this order earlier, and no menu is proposed anymore";
                org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu();
                throw new ApiConcurrentDeletionException(error, dto);
            }
        }
    }
    
    public OrderSummary ordersDailyDailyMenuIdGet(Long dailyMenuId) throws ApiException{
       
        DailyMenu dailyMenu = dmRepo.findOne(dailyMenuId);
        
        if (dailyMenu == null){
            throw new ApiException( 404, "Daily Menu not found" );
        }
        
        OrderSummary orderSummary = new OrderSummary(dailyMenu.getDate());
        List<OrdersChefInnerFoods> foods = new ArrayList<>();
        List<DailyOrder> orders = dailyMenu.getDailyOrders();
        for (DailyOrder orderDB : orders) {
            OrderSummaryDailyOrders order = new OrderSummaryDailyOrders(orderDB.getUser().getFirstName(), orderDB.getUser().getLastName());
            List<OrderItem> orderItems = orderDB.getOrderItems();
           
            for (OrderItem orderItem : orderItems) {
                
                if( !dailyMenu.getFoods().contains(orderItem.getFood()) ){                  
                    throw new ApiException( 404, "Food not in daily menu" );
                }
                
                OrdersChefInnerFoods food = new OrdersChefInnerFoods(orderItem);
                OrdersChefInnerFoods summaryFood = new OrdersChefInnerFoods(orderItem);
                
                if (!foods.contains(summaryFood)){
                    foods.add(summaryFood);
                }else{
                    int index = foods.indexOf(summaryFood);
                    
                    foods.get(index).setQuantity(foods.get(index).getQuantity() + orderItem.getQuantity());
                }
                
                order.addFoodsItem(food);
            }
            
            orderSummary.addDailyOrdersItem(order);
        }
        
        orderSummary.setFoods(foods);
        
        return orderSummary;

    }
    
    @Transactional
    public void ordersIdDelete( Long orderId, Long menuId, LocalDate date, Integer menuVersion ) throws ApiException{

        DailyOrder dailyOrder = doRepo.findOne(orderId);
        if(dailyOrder == null){
            // error 410 instead of 404
            error410( menuId, date, menuVersion);
        }
        
        //just in case.. by Alex before: if(!dailyOrder.getOrderItems().isEmpty() || dailyOrder.isFinal())
        if( dailyOrder.isFinal(yumService.getSettings().getDeadline())){
            throw new ApiException( 406, "Not updatable order" );
        }
        
        doRepo.delete(dailyOrder);
    }
    
    public org.bootcamp.yum.api.model.DailyMenu ordersIdGet( Long orderId, Long menuId, LocalDate date, Integer menuVersion ) throws ApiException{

        
        DailyOrder dailyOrder = doRepo.findOne(orderId);
               
        if(dailyOrder == null){
            // error 410 instead of 404
            error410( menuId, date, menuVersion);
        }
        
        //daily order requested does not belong to current user
        if(!dailyOrder.getUser().equals(getUser())){
           throw new ApiException(400, "daily order requested does not belong to current user");
        }
        
        return new org.bootcamp.yum.api.model.DailyMenu(dailyOrder,yumService.getSettings().getDeadline());
    }

    @Transactional
    public NewEntry ordersIdPut( Long orderId, OrderSave body, Boolean email) throws ApiException{
        
        if( body == null || body.getOrderItems().isEmpty() ){
            throw new ApiException(400, "Empty DTO");
        }
        
        DailyOrder orderToEdit = doRepo.findOne(orderId);
        
        //order does not exist
        if( orderToEdit == null ){
            // error 410 instead of 404
            error410( body.getDailyMenuId(), body.getDate(), body.getDailyMenuVersion());
        }
        
        //deadline passed
        if ( orderToEdit.isFinal(yumService.getSettings().getDeadline()) ){
            throw new ApiException(400, "deadline passed");
        }
        
        if( body.equals(new OrderSave(orderToEdit)) ){
            throw new ApiException(400, "no changes in incoming update");
        }
        
        //concurrent modification
        if( orderToEdit.getVersion() != body.getOrderVersion()){
            throw new ApiConcurrentModificationException(new OrderSave(orderToEdit));
        }    
        
        DailyMenu menu = dmRepo.findOne(body.getDailyMenuId());
        
        //dailyMenu does not exist  
        if( menu == null ) {      
            throw new ApiException(404, "Daily Menu does not exist");
        }
        
        //user match
        if( !orderToEdit.getUser().equals(getUser()) ){
            throw new ApiException(400, "daily order requested does not belong to current user");
        }
        
        //orderItem with 0 quantity
        for (Iterator<OrderSaveOrderItems> chekIt = body.getOrderItems().iterator(); chekIt.hasNext();) {
            OrderSaveOrderItems check = chekIt.next();
            
            if (check.getQuantity() <= 0){
                System.out.println("0 QUANTITY");
                throw new ApiException(400, "orderItem with 0 quantity");
            }
                
            
        }

        
        boolean found = false;
        
        for (Iterator<OrderItem> orderItemIterator = orderToEdit.getOrderItems().iterator(); orderItemIterator.hasNext();) {
            OrderItem item = orderItemIterator.next();
            
            found = false;
            
            for (Iterator<OrderSaveOrderItems> iterator = body.getOrderItems().iterator(); iterator.hasNext();) {
                OrderSaveOrderItems next = iterator.next();
                
                if( item.getFood().getId() == next.getFoodId() && item.getDailyOrder().getId() == orderToEdit.getId() ){
                    
                    found = true;
                    
                    item.getFood().addPopularity(-item.getQuantity());
                    item.getFood().addPopularity(next.getQuantity());
                    
                    item.setQuantity(next.getQuantity());
                    iterator.remove();
                    break;
                }
                
            }
            if(!found)
                orderItemIterator.remove();
            
            
        }


        
        for (Iterator<OrderSaveOrderItems> odIt = body.getOrderItems().iterator(); odIt.hasNext();) {
            OrderSaveOrderItems odi = odIt.next();
            
            Food food = foodRepo.findById(odi.getFoodId());
            
            //food not found or not contained in dailyMenu
            if( food == null || ( !menu.getFoods().contains(food) ) ){    
                throw new ApiException(404, "food not found or not contained in dailyMenu");
            }
            
            OrderItem orderItem = new OrderItem();
            
            orderItem.setFood(food);
            orderItem.setDailyOrder(orderToEdit);
            
            orderItem.setQuantity(odi.getQuantity());                                    
            food.addPopularity(odi.getQuantity());
            
            orderToEdit.addOrderItem(orderItem);
            
        }
        
        orderToEdit.setVersion(orderToEdit.getVersion()+1);
        if (email != null && email){
            
            mailService.sendConfirmOrderEmailToHungry(orderToEdit);
        
        }
        
        NewEntry dto = new NewEntry();
        dto.setId(orderToEdit.getId());
        dto.setVersion(orderToEdit.getVersion());
        
        return dto;
    }
    
    public OrdersChef ordersMonthlyGet() {
        OrdersChef orders = new OrdersChef();
        
        LocalDate monthBegin = new LocalDate().withDayOfMonth(1);
        LocalDate monthEnd = new LocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
        
        List<DailyMenu> dailyMenusDB = dmRepo.findByDateBetween(monthBegin, monthEnd);
        
        //check if there are no orders for current month
        if (dailyMenusDB.size()==0){
            return orders;
        }
        
        for ( int i = 0 ; i < dailyMenusDB.size() ; i++){
            List<OrdersChefInnerFoods> ordersChefInnerFoods = new ArrayList<>();
            
            OrdersChefInner ordersChefInner = new OrdersChefInner();
            ordersChefInner.setDailyMenuId(dailyMenusDB.get(i).getId());
            ordersChefInner.setDate(dailyMenusDB.get(i).getDate());
            
            
            List<DailyOrder> dailyOrders = dailyMenusDB.get(i).getDailyOrders();
            
            for (DailyOrder dailyOrder : dailyOrders){
                
                List<OrderItem> orderItems = dailyOrder.getOrderItems();
                
                for (OrderItem orderItem : orderItems){
                    OrdersChefInnerFoods food = new OrdersChefInnerFoods();
                    food.setId(orderItem.getFood().getId());
                    food.setQuantity(orderItem.getQuantity());
                    
                    if (!ordersChefInnerFoods.contains(food) ){
                        //edited equals to check id only
                        ordersChefInnerFoods.add(food);
                    
                    }else{
                        
                        int index = ordersChefInnerFoods.indexOf(food);
                        ordersChefInnerFoods.get(index).setQuantity(ordersChefInnerFoods.get(index).getQuantity() + food.getQuantity());
                    }
       
                }
            }

            ordersChefInner.setFoods(ordersChefInnerFoods);
            orders.add(ordersChefInner);

        }
        return orders;
    }
    
    public OrdersChef ordersMonthlyYearMonthGet( Integer month, Integer year) throws ApiException{
        OrdersChef orders = new OrdersChef();
        
        if(month < 1 || month > 12 || String.valueOf(year).length()>4){
            throw new ApiException(400, "Bad request: time out of range") ;
        }
        
        LocalDate monthBegin = new LocalDate(year, month, 1).withDayOfMonth(1);
        LocalDate monthEnd = new LocalDate(year, month, 1).plusMonths(1).withDayOfMonth(1).minusDays(1);
        
        List<DailyMenu> dailyMenusDB = dmRepo.findByDateBetween(monthBegin, monthEnd);
        
        //check if there are no orders for asked month
        if (dailyMenusDB.size()==0){
            return orders;
        }
        
        for ( int i = 0 ; i < dailyMenusDB.size() ; i++){
            List<OrdersChefInnerFoods> ordersChefInnerFoods = new ArrayList<>();
            
            OrdersChefInner ordersChefInner = new OrdersChefInner();
            ordersChefInner.setDailyMenuId(dailyMenusDB.get(i).getId());
            ordersChefInner.setDate(dailyMenusDB.get(i).getDate());
            
            
            List<DailyOrder> dailyOrders = dailyMenusDB.get(i).getDailyOrders();
            
            for (DailyOrder dailyOrder : dailyOrders){
                
                List<OrderItem> orderItems = dailyOrder.getOrderItems();
                
                for (OrderItem orderItem : orderItems){
                    OrdersChefInnerFoods food = new OrdersChefInnerFoods();
                    food.setId(orderItem.getFood().getId());
                    food.setQuantity(orderItem.getQuantity());
                    
                    if (!ordersChefInnerFoods.contains(food) ){
                        //edited equals to check id only
                        ordersChefInnerFoods.add(food);
                    
                    }else{
                        
                        int index = ordersChefInnerFoods.indexOf(food);
                        ordersChefInnerFoods.get(index).setQuantity(ordersChefInnerFoods.get(index).getQuantity() + food.getQuantity());
                    }
       
                }
            }

            ordersChefInner.setFoods(ordersChefInnerFoods);
            orders.add(ordersChefInner);

        }
        return orders;
    }
    
    @Transactional    
    public NewEntry ordersPost( OrderSave body, Boolean email) throws ApiException{

        User logged = getUser();
        
        DailyMenu dm = dmRepo.findOne(body.getDailyMenuId());
        
        //dailyMenu does not exist  
        if( dm == null ) {
            throw new ApiException(404, "dailyMenu does not exist");
        }
        
        if( yumService.deadLineHasPassedFor(dm.getDate()) ){
            throw new ApiException(406, "deadline passed");
        }
        
        //user has already ordered for this day
        DailyOrder existingDailyOrder = doRepo.findByUserAndDailyMenu(logged, dm);
        if( existingDailyOrder != null ){
            String error = "Already ordered";
            org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(existingDailyOrder, yumService.getSettings().getDeadline());
            throw new ApiConcurrentCreationException(error, dto);
        }
        
        DailyOrder dao = new DailyOrder();
        
        dao.setDailyMenu(dm);
        dao.setUser(logged);         
        dao.setFinal(false);
            
        
        for (Iterator<OrderSaveOrderItems> odIt = body.getOrderItems().iterator(); odIt.hasNext();) {
            OrderSaveOrderItems odi = odIt.next();
            
            if(odi.getQuantity() > 0){
                
                Food food = foodRepo.findById(odi.getFoodId());
            
                //food not found or not contained in dailyMenu
                if( food == null || ( !dm.getFoods().contains(food) ) ){    
                        throw new ApiException(404, "food not found or not contained in dailyMenu");
                }

                OrderItem orderItem = new OrderItem();

                orderItem.setFood(food);
                orderItem.setDailyOrder(dao);

                orderItem.setQuantity(odi.getQuantity()); 
                food.addPopularity(odi.getQuantity());

                dao.addOrderItem(orderItem);
            }
            else{
                throw new ApiException(406, "orderItem with 0 quantity");
            }
   
        } 
        
        doRepo.save(dao); 
        NewEntry newEntry = new NewEntry();
        newEntry.setId(dao.getId());
        newEntry.setVersion(dao.getVersion());
        
        if (email != null && email){
            
            mailService.sendConfirmOrderEmailToHungry(dao);
        
        }
        
        return newEntry;

    }
    
}
