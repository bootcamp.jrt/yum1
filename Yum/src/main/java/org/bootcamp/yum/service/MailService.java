/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserRole;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class MailService {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private GlobalSettingsService yum;

    @Value("${yum.mail.senderEmailAddress}")
    private String senderEmailAddress;

    @Value("${yum.hostname}")
    private String yumHostname;

    public void sendNewUserEmailToAllAdmins(User newUser) {

       
        StringBuilder sb = new StringBuilder(200);
        sb.append("A new user just registered. Here are the new user details");
        sb.append("\n\tfirst name: ");
        sb.append(newUser.getFirstName());
        sb.append("\n\tlast name: ");
        sb.append(newUser.getLastName());
        sb.append("\n\temail: ");
        sb.append(newUser.getEmail());
        sb.append("\n\nYou have to approve this user so he/she can log in.");
        sb.append("\nclick on this link to approve the user:\n");
        //sb.append("http://");
        sb.append(yumHostname);
        sb.append("/admin/users/");
        sb.append(newUser.getId());
        String text = sb.toString();

        // A new user just registered. Here are the new user details
        //     first name: <newUser.firstName>
        //      last name: <newUser.lastName>
        //          email: <newUser.email>
        // 
        // You have to approve this user so he/she can log in.
        // click on this link to approve the user:
        // http://<yumHostname>/admin/users/<newUser.id>
        // Then iterate over a list of admin users. For each admin:
        // Prepare the message
        for (User admin : userRepo.findByRole(UserRole.ADMIN)) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(senderEmailAddress);
            message.setTo(admin.getEmail());
            message.setSubject("[Yum] new user registered");
            //  add a greeting sentence to the text:
            StringBuilder sbuilder = new StringBuilder(200);
            sbuilder.append("Dear ");
            sbuilder.append(admin.getFirstName());
            sbuilder.append(" ");
            sbuilder.append(admin.getLastName());
            sbuilder.append(",\r\n");
            sbuilder.append(text);
            message.setText(sbuilder.toString());
            mailSender.send(message);
        }
    }

    public void sendUserApprovedEmailToUser(User userApproved) {

        // When a new user registers, it is unapproved by default.
        // We need to let the user know that an admin approved him.
        StringBuilder sb = new StringBuilder(200);
        sb.append("You got approved. \n\nNow you can login using your email and password following the link:\n");
        //sb.append("http://");
        sb.append(yumHostname + "/login");

        String text = sb.toString();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);
        message.setTo(userApproved.getEmail());
        message.setSubject("[Yum] got approved");

        message.setText(text);
        mailSender.send(message);

    }

    public void sendConfirmOrderEmailToHungry(DailyOrder order) {
        Double total = 0D;
        StringBuilder sb = new StringBuilder();
        sb.append("Dear ");
        sb.append(order.getUser().getFirstName());
        sb.append(" ");
        sb.append(order.getUser().getLastName());
        sb.append("\n\nYou just placed this order for the day ");
        sb.append(order.getDailyMenu().getDate());
        sb.append("\n\n");
        for (OrderItem item : order.getOrderItems()) {

            sb.append("\t\t");
            sb.append(item.getFood().getName());
            sb.append("\tqty ");
            sb.append(item.getQuantity());
            sb.append(" x ");
            sb.append(item.getFood().getPrice());
            sb.append(" euro\n");
            total += item.getQuantity() * item.getFood().getPrice();
        }
        sb.append("\n\t\ttotal : ");
        sb.append(total);
        sb.append(" euro\n");
        sb.append("\nYou can modify this order until ");
        sb.append(order.getDailyMenu().getDate().minusDays(1));
        sb.append(" ");
        sb.append(yum.getSettings().getDeadline().toString("HH:mm:ss"));
        sb.append(" by going to the link:\n");
        //sb.append("http://");
        sb.append(yumHostname);
        sb.append("/hungry/");
        sb.append(order.getDailyMenu().getDate().getYear());
        sb.append("/");
        sb.append(order.getDailyMenu().getDate().getWeekOfWeekyear());
        sb.append("/\n\n");
        sb.append("Thank you for your order!");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);
        message.setTo(order.getUser().getEmail());
        message.setSubject("[Yum] new order placed");
        message.setText(sb.toString());
        mailSender.send(message);

        
        // You just placed this order for the day 01/01/2017
        //
        //    food1   qty 1 x 5 euro 
        //    food2
        //
        //    total :  15 euro
        //
        // You can modify this order until <deadline time> <date-1> by going to the link:
        // http://<yumHostname>/hungry/<year>/<week>/
        // 
        // Thank you for your order!
    }

    public void sendResetPasswordLinkEmail(User user) {
        StringBuilder sb = new StringBuilder(200);
        sb.append("Dear ");
        sb.append(user.getFirstName());
        sb.append(" ");
        sb.append(user.getLastName());
        sb.append(",\n\n");
        sb.append("You just requested your password to be reset. If that was not you, please discard this message.\n\n");
        sb.append("To enter your new password, please visit this link:\n");
        //sb.append("http://");
        sb.append(yumHostname);
        sb.append("/changepasswd/");
        sb.append(user.getSecret());
        sb.append("\n\nThis link expires in 12 hours.");
        sb.append("\nThank you for using Yum!");
        

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);
        message.setTo(user.getEmail());
        message.setSubject("[Yum] reset password");
        message.setText(sb.toString());
        mailSender.send(message);
    }

}
