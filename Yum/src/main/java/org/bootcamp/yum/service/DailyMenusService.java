/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import java.util.Iterator;
import java.util.List;

import org.joda.time.LocalDate;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exception.ApiConcurrentCreationException;
import org.bootcamp.yum.exception.ApiConcurrentDeletionException;
import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.DailyMenuSave;
import org.bootcamp.yum.api.model.DailyMenuSaveFoods;
import org.bootcamp.yum.api.model.DailyMenusChef;
import org.bootcamp.yum.api.model.NewEntry;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class DailyMenusService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private DailyMenuRepository dailyMenuRepo;

    @Autowired
    private FoodRepository foodRepo;

    @Autowired
    private GlobalSettingsService yum;
    
    private User getUser() {
        return userRepo.findOne((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
    
    // this method is used in the DailyMenusApiController in OptimisticLockException handling
    public DailyMenuSave getDailyMenuSaveByDailyMenuId(Long dailyMenuId) throws ApiException {
        
        DailyMenu dailyMenu = dailyMenuRepo.findById(dailyMenuId);
        
        // if the dailyMenu does not exist
        if (dailyMenu == null) {
            throw new ApiException(404, "Daily Menu not found");
        }

        return new DailyMenuSave(dailyMenu);
    }
    
    @Transactional
    public DailyMenuSave dailyMenusIdPut(Long id, DailyMenuSave body) throws ApiException {
        
        DailyMenu dailyMenu = dailyMenuRepo.findById(id);
        
        // if the dailyMenu does not exist
        if (dailyMenu == null) {
            DailyMenu newDailyMenu = dailyMenuRepo.findByDate(body.getDate());
            if(newDailyMenu != null){
                String error = "You deleted this menu earlier, and recreated it. Here is the new one";
//                org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(newDailyMenu, yum.deadLineHasPassedFor(newDailyMenu.getDate()));
                DailyMenuSave dto = new DailyMenuSave(newDailyMenu);
                throw new ApiConcurrentDeletionException(error, dto);
            }else{
                String error = "You deleted this menu earlier. Please recreate it again if this is what was intended";
                DailyMenuSave dto = new DailyMenuSave(0L);
                throw new ApiConcurrentDeletionException(error, dto);
            }
        }

        //concurrent modification
        if (dailyMenu.getVersion() != body.getVersion()) {
            throw new ApiConcurrentModificationException(new DailyMenuSave(dailyMenu));
        }

        //Cannot change menu because it's deadline has passed
        if (yum.deadLineHasPassedFor(dailyMenu.getDate())) {
            throw new ApiException(400, "Daily Menu cannot be changed because it cannot receive any orders");
        }
        
        if (body.getFoods().isEmpty()){
            this.dailyMenuRepo.delete(dailyMenu);
            return new DailyMenuSave(0L);
        }

        boolean found = false;

        //Iterate Foods from database
        for (Iterator<Food> foodIterator = dailyMenu.getFoods().iterator(); foodIterator.hasNext();) {
            Food item = foodIterator.next();

            found = false;

            //compare foods from the database with the given foods
            for (Iterator<DailyMenuSaveFoods> iterator = body.getFoods().iterator(); iterator.hasNext();) {
                DailyMenuSaveFoods next = iterator.next();

                if (item.getId() == next.getId()) {
                    found = true;
                    iterator.remove();
                    break;
                }
            }
            if (!found) {
                foodIterator.remove();
            }
        }

        //Add to the daily menu the new foods
        for (Iterator<DailyMenuSaveFoods> odIt = body.getFoods().iterator(); odIt.hasNext();) {
            DailyMenuSaveFoods odi = odIt.next();

            Food food = foodRepo.findById(odi.getId());

            if (food == null) {    //food not found 
                throw new ApiException(404, "There are no foods to put");
            }

            dailyMenu.addFood(food);

        }
        
        DailyMenuSave dtoToReturn  = new DailyMenuSave(dailyMenu);
        
        for (DailyOrder dailyOrder : dailyMenu.getDailyOrders()) {

                for (OrderItem orderItem : dailyOrder.getOrderItems()) {

                        //constructor sets isOrdered to false 
                        DailyMenuSaveFoods foodOrdered = new DailyMenuSaveFoods(orderItem.getFood().getId());

                        //will bypass any food that has been set to ordered before
                        if (dtoToReturn.getFoods().contains(foodOrdered)) {
                            

                            int index = dtoToReturn.getFoods().indexOf(foodOrdered);
                            dtoToReturn.getFoods().get(index).setIsOrdered(true);

                        }

                }

            }
        
        dtoToReturn.setVersion(dailyMenu.getVersion()+1);
        
        return dtoToReturn;
    }

    public DailyMenusChef dailyMenusMonthlyGet() throws ApiException {

        DailyMenusChef dailyMenusChef = new DailyMenusChef();

        LocalDate monthBegin = new LocalDate().withDayOfMonth(1);
        LocalDate monthEnd = new LocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1);

        List<DailyMenu> dailyMenusDB = dailyMenuRepo.findByDateBetween(monthBegin, monthEnd);

        if (dailyMenusDB.size() == 0) {
            return dailyMenusChef;
        }

        for (DailyMenu dailyMenuDB : dailyMenusDB) {
            DailyMenuSave dailyMenuChef = new DailyMenuSave(dailyMenuDB);

            //check if food is ordered
            for (DailyOrder dailyOrder : dailyMenuDB.getDailyOrders()) {

                for (OrderItem orderItem : dailyOrder.getOrderItems()) {

                    if (dailyMenuDB.getFoods().contains(orderItem.getFood())) {

                        //constructor sets isOrdered to false 
                        DailyMenuSaveFoods foodOrdered = new DailyMenuSaveFoods(orderItem.getFood().getId());

                        //will bypass any food that has been set to ordered before
                        if (dailyMenuChef.getFoods().contains(foodOrdered)) {

                            int index = dailyMenuChef.getFoods().indexOf(foodOrdered);
                            dailyMenuChef.getFoods().get(index).setIsOrdered(Boolean.TRUE);

                        }
                    }

                }

            }
            dailyMenusChef.add(dailyMenuChef);
        }
        return dailyMenusChef;
    }

    public DailyMenusChef dailyMenusMonthlyYearMonthGet(Integer month, Integer year) throws ApiException {
        // do some magic!

        DailyMenusChef dailyMenusChef = new DailyMenusChef();

        LocalDate monthBegin = new LocalDate(year, month, 1);    //.withDayOfMonth(1);
        LocalDate monthEnd = monthBegin.plusMonths(1).minusDays(1);

        List<DailyMenu> dailyMenusDB = dailyMenuRepo.findByDateBetween(monthBegin, monthEnd);

        if (dailyMenusDB.size() == 0) {
            return dailyMenusChef;
        }

        for (DailyMenu dailyMenuDB : dailyMenusDB) {
            DailyMenuSave dailyMenuChef = new DailyMenuSave(dailyMenuDB);

            //check if food is ordered
            for (DailyOrder dailyOrder : dailyMenuDB.getDailyOrders()) {

                for (OrderItem orderItem : dailyOrder.getOrderItems()) {

                    if (dailyMenuDB.getFoods().contains(orderItem.getFood())) {

                        //constructor sets isOrdered to false 
                        DailyMenuSaveFoods foodOrdered = new DailyMenuSaveFoods(orderItem.getFood().getId());

                        //will bypass any food that has been set to ordered before
                        if (dailyMenuChef.getFoods().contains(foodOrdered)) {

                            int index = dailyMenuChef.getFoods().indexOf(foodOrdered);
                            dailyMenuChef.getFoods().get(index).setIsOrdered(Boolean.TRUE);

                        }
                    }

                }

            }
            dailyMenusChef.add(dailyMenuChef);
        }
        return dailyMenusChef;
    }

    @Transactional
    public NewEntry dailyMenusPost(DailyMenuSave body) throws ApiException {

        DailyMenu dao = new DailyMenu();
        dao.setDate(body.getDate());

        // check if a dailyMenu already exists for that day
        DailyMenu existingDailyMenu = dailyMenuRepo.findByDate(body.getDate());
        if(existingDailyMenu != null){
            String error = "A menu was already created for that day";
//            org.bootcamp.yum.api.model.DailyMenu dto = new org.bootcamp.yum.api.model.DailyMenu(existingDailyMenu, yum.deadLineHasPassedFor(existingDailyMenu.getDate()));
            DailyMenuSave dto = new DailyMenuSave(existingDailyMenu);
            throw new ApiConcurrentCreationException(error, dto);
        }
        
        //Add to the daily menu the new foods
        for (Iterator<DailyMenuSaveFoods> odIt = body.getFoods().iterator(); odIt.hasNext();) {
            DailyMenuSaveFoods odi = odIt.next();

            Food food = foodRepo.findById(odi.getId());

            if (food == null) {    //food not found 
                throw new ApiException(404, "There are no foods to put");
            }
            dao.addFood(food);

        }

        DailyMenu dm = dailyMenuRepo.save(dao);

        NewEntry newEntry = new NewEntry();
        newEntry.setId(dm.getId());
        newEntry.setVersion(dm.getVersion());

        return newEntry;

    }
}
