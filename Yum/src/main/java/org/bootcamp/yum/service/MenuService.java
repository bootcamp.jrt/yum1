/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import java.util.ArrayList;
import java.util.List;
import org.joda.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.api.model.DailyMenus;
import org.bootcamp.yum.api.model.Orders;
import org.bootcamp.yum.api.model.OrdersInner;
import org.bootcamp.yum.api.model.OrdersInnerFoods;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class MenuService {

    @Autowired
    private DailyMenuRepository dailyMenuRepo;

    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private OrderItemRepository orderItemRepo;

    @Autowired
    private GlobalSettingsService yum;
    
    private User getUser() {
        return userRepo.findOne((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public Orders getMenusMonthly(int month, int year) {

        Orders orders = new Orders();
        
        LocalDate monthBegin = new LocalDate().withYear(year).withMonthOfYear(month).withDayOfMonth(1);
        LocalDate monthEnd = new LocalDate().withYear(year).withMonthOfYear(month).plusMonths(1).withDayOfMonth(1).minusDays(1);

        //daily menus from the database
        List<DailyMenu> dailyMenusDB = dailyMenuRepo.findByDateBetween(monthBegin, monthEnd);

        //check if there are no orders for current month
        if (dailyMenusDB.isEmpty()) {
            return orders;
        }

        for (int i = 0; i < dailyMenusDB.size(); i++) {

            List<OrdersInnerFoods> ordersInnerFoods = new ArrayList<>();

            OrdersInner ordersInner = new OrdersInner();
            ordersInner.setDailyMenuId(dailyMenusDB.get(i).getId());
            ordersInner.setDate(dailyMenusDB.get(i).getDate());

            List<DailyOrder> dailyOrders = dailyMenusDB.get(i).getDailyOrders();

            for (DailyOrder dailyOrder : dailyOrders) {

                if (dailyOrder.getUser().equals(getUser())) {

                    List<OrderItem> orderItems = dailyOrder.getOrderItems();

                    for (OrderItem orderItem : orderItems) {

                        OrdersInnerFoods food = new OrdersInnerFoods();

                        food.setName(orderItem.getFood().getName());
                        food.setType(orderItem.getFood().getFoodType().toString());
                        food.setDescription(orderItem.getFood().getDescription());
                        food.setQuantity(orderItem.getQuantity());
                        food.setPrice(orderItem.getFood().getPrice());

                        ordersInnerFoods.add(food);
                    }
                }

                ordersInner.setFoods(ordersInnerFoods);

            }

            orders.add(ordersInner);
        }

        return orders;

    }

    public DailyMenus getWeeklyMenus(int week, int year) {

        // create a json list to be returned
        DailyMenus dm = new DailyMenus();

        // get me the date of Monday in the current week
        LocalDate monday = new LocalDate().withYear(year).withWeekOfWeekyear(week).withDayOfWeek(1);

        // iterate 5 times (monday - friday)
        for (int i = 0; i < 5; i++) {

            // get me the date of the day I want
            LocalDate date = monday.plusDays(i);
            DailyMenu dailyMenu = dailyMenuRepo.findByDate(date);

            // if that date does not have a daily menu in the DB go to the next day
            if (dailyMenu == null) {
                continue;
            }

            // fill dto with menu
            org.bootcamp.yum.api.model.DailyMenu myDailyMenu = new org.bootcamp.yum.api.model.DailyMenu(dailyMenu, yum.deadLineHasPassedFor(dailyMenu.getDate()));
            
            // if that date has a menu -> check if logged in user has placed an order
            Long loggedId = getUser().getId();
            for (DailyOrder dailyOrder : dailyMenu.getDailyOrders()) {
                if (dailyOrder.getUser().getId() == loggedId) {
                    
                    // if there is an order -> fill dto with the order
                    myDailyMenu.fillWithOrder(dailyOrder, yum.getSettings().getDeadline());
                    break;
                }
            }
            
            // put the dto in the return list
            dm.add(myDailyMenu);
        }

        // send the list
        return dm;

    }

}
