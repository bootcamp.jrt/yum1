/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;


import java.io.IOException;
import javax.transaction.Transactional;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.bootcamp.JwtCodec;
import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class SettingsService {

    @Autowired
    private UserRepository userRepo;

    private User getUser() {
        return userRepo.findOne((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public UserGet settingsGet() {
        // get the logged in user, pass it in the json object constractor and return the json object
        return new UserGet(getUser());
    }

    public byte[] settingsPictureTokenGet(String token) throws ApiException{
        try{
            
            Claims claims = JwtCodec.decode(token);
            User user = userRepo.findOne(Long.parseLong(claims.getSubject()));
            if (user == null){
            
                throw new ApiException(404 , "User not found.");
            }
           
            if (user.getPicture() == null){
                throw new ApiException(404 , "Picture not found.");
            }
            return user.getPicture();
        }catch(JwtException ex){
            
            throw new ApiException(403 , "Invalid token.");
            
        }
    }
    
    @Transactional
    public void settingsPictureDelete() throws ApiException{
        try {
            getUser().setPicture(null);
        } catch (IOException ex) {
            throw new ApiException(400 , "Something went wrong.");
        }
    }

    // this method is used in the SettingsApiController in OptimisticLockException handling
    public UserSave getUserSaveOfLoggedInUser() {
        return new UserSave(getUser());
    }

    @Transactional
    public void settingsPicturePost(MultipartFile file) throws ApiException{
     
        if (!file.isEmpty()) {
            try {
                
                getUser().setPicture(file);

            } catch (IOException ex) {
                throw new ApiException(400,  "Problem rendering the picture.");
            }
        }else{
            throw new ApiException(400,  "Picture file is empty.");
        }
    }

    @Transactional
    public void settingsPut(UserSave body) throws ApiException {

        // check if the data are valid to be saved
        if (!body.hasValidEmail()) {

            throw new ApiException(400, "Not valid email or password");

        }

        if (!body.getPassword().isEmpty()) {
            if (!body.hasValidPassword()) {
                throw new ApiException(400, "Not valid email or password");
            }
        }

        // get the logged in user
        User user = getUser();

        // check for concurrent modification error. If occurs send back the newer version
        if (body.getVersion() != user.getVersion()) {

            throw new ApiConcurrentModificationException(new UserSave(user));

        }

        // if everything is good update the user
        user.setEmail(body.getEmail());
        user.setFirstName(body.getFirstName());
        user.setLastName(body.getLastName());
        if (!body.getPassword().isEmpty()){
            user.setPassword(body.getPassword());
        }

        userRepo.save(user);

    }

}
