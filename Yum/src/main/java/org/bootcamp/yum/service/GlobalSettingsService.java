/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.service;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.Settings;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.YumRepository;

@Service
public class GlobalSettingsService {

    private Yum yum;

    @Autowired
    private YumRepository yumRepo;

    @Transactional
    public Yum getSettings() {

        if (yum != null) {
            return yum;
        }

        yum = yumRepo.findOne(1L);

        // if there are no settings -> create them
        if (yum == null) {
            yum = new Yum();
            yumRepo.save(yum);
        }

        return yum;
    }
    
    @Transactional
    public void incrementFoodsVersion() {
        Yum yum = getSettings();
        yum.setFoodsVersion(yum.getFoodsVersion() + 1);
        yumRepo.save(yum);
    }

    public Settings globalsettingsGet() {
        return new Settings(getSettings());
    }

    public boolean deadLineHasPassedFor(LocalDate date) {
        LocalDateTime d = date.minusDays(1).toLocalDateTime(getSettings().getDeadline());
        LocalDateTime now = LocalDateTime.now();
        return now.isAfter(d);
    }

    @Transactional
    public void globalsettingsPut(Settings body) throws ApiException {

        // validate body (time and currency.. the others are plain strings)
        if (!body.isValid()) {
            throw new ApiException(400, "Bad request");
        }

        // get the settings using the nested class in the entity Yum
        Yum yum = getSettings();//yumRepo.findOne(1L);//getSettings();

        // concurrent modification error: 409
        if (yum.getVersion() != body.getVersion()) {
            throw new ApiConcurrentModificationException(new Settings(yum));
        }

        // if there are no errors proceed to update
        yum.updateSettings(body);
        yum.setVersion(yum.getVersion()+1);
        this.yum = yumRepo.save(yum);

    }
}
