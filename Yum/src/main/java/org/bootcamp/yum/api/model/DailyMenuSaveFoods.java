/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * DailyMenuSaveFoods
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class DailyMenuSaveFoods   {
  @JsonProperty("id")
  private Long id = null;
  
  @JsonProperty("isOrdered")
  private Boolean isOrdered = null;
  
  
    public DailyMenuSaveFoods(Long id) {
        this.id = id;
        this.isOrdered = false;
    }

    public DailyMenuSaveFoods() {
    }

  public DailyMenuSaveFoods id(Long id) {
    this.id = id;
    this.isOrdered = false;
    return this;
  }
  
  public DailyMenuSaveFoods isOrdered(Boolean isOrdered) {
    this.isOrdered = isOrdered;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Get isOrdered
   * @return isOrdered
  **/
  @ApiModelProperty(value = "")
  public Boolean getIsOrdered() {
    return isOrdered;
  }

  public void setIsOrdered(Boolean isOrdered) {
    this.isOrdered = isOrdered;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyMenuSaveFoods dailyMenuSaveFoods = (DailyMenuSaveFoods) o;
    return Objects.equals(this.id, dailyMenuSaveFoods.id);
//            && Objects.equals(this.isOrdered, dailyMenuSaveFoods.isOrdered);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, isOrdered);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyMenuSaveFoods {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isOrdered: ").append(toIndentedString(isOrdered)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

