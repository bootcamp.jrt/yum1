/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;




import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.persistence.OptimisticLockException;

import io.swagger.annotations.*;

import org.bootcamp.yum.service.OrdersService;
import org.bootcamp.yum.api.model.OrderSave;
import org.bootcamp.yum.api.model.OrderSummary;
import org.bootcamp.yum.api.model.OrdersChef;
import org.bootcamp.yum.api.model.DailyMenu;
import org.bootcamp.yum.api.model.NewEntry;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@Controller
public class OrdersApiController implements OrdersApi {

    OrdersService ordersService;

    @Autowired
    public OrdersApiController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<OrderSummary> ordersDailyDailyMenuIdGet(@ApiParam(value = "", required = true) @PathVariable("dailyMenuId") Long dailyMenuId) throws ApiException {

        return new ResponseEntity<OrderSummary>(ordersService.ordersDailyDailyMenuIdGet(dailyMenuId), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Transactional
    public ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "date", required = true) String date,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion) throws ApiException {

        ordersService.ordersIdDelete(id, menuId, LocalDate.parse(date), menuVersion);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<DailyMenu> ordersIdGet(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "date", required = true) String date,
           @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion) throws ApiException {

        return new ResponseEntity<DailyMenu>(ordersService.ordersIdGet(id, menuId, LocalDate.parse(date), menuVersion), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<NewEntry> ordersIdPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
           @ApiParam(value = "" ,required=true ) @RequestBody OrderSave body,
           @ApiParam(value = "") @RequestParam(value = "email", required = false) Boolean email) throws ApiException {

        try {
            return new ResponseEntity<>(ordersService.ordersIdPut(id, body, email), HttpStatus.OK);
        } catch (OptimisticLockException ex) {
            try {
                throw new ApiConcurrentModificationException(ordersService.ordersIdGet(id, body.getDailyMenuId(), body.getDate(), body.getDailyMenuVersion()));
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<OrdersChef> ordersMonthlyGet() throws ApiException {
        return new ResponseEntity<OrdersChef>(ordersService.ordersMonthlyGet(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<OrdersChef> ordersMonthlyYearMonthGet(@ApiParam(value = "", required = true) @PathVariable("month") Integer month,
            @ApiParam(value = "", required = true) @PathVariable("year") Integer year) throws ApiException {

        return new ResponseEntity<OrdersChef>(ordersService.ordersMonthlyYearMonthGet(month, year), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Transactional
    public ResponseEntity<NewEntry> ordersPost(@ApiParam(value = "" ,required=true ) @RequestBody OrderSave body,
         @ApiParam(value = "") @RequestParam(value = "email", required = false) Boolean email) throws ApiException {

        //ordersService.ordersPost(body, email);
        return new ResponseEntity<>(ordersService.ordersPost(body, email), HttpStatus.OK);

    }

}
