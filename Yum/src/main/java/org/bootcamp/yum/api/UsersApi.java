/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import java.io.File;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.constraints.*;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.UsersList;
import org.bootcamp.yum.api.model.User;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.api.model.Approve;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-02T19:14:36.153+03:00")

@Api(value = "users", description = "the users API")
@RequestMapping(value = "/api")
public interface UsersApi {

    @ApiOperation(value = "returns the list of users", notes = "returs the list of all users", response = UsersList.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "the list of users", response = UsersList.class),
        @ApiResponse(code = 400, message = "something went wrong", response = UsersList.class) })
    @RequestMapping(value = "/users",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<UsersList> usersGet( @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "sort", required = true) String sort,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "order", required = true) String order,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "page", required = true) Integer page,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "size", required = true) Integer size) throws ApiException;


    @ApiOperation(value = "admin toggles approved boolean for the user with the given id", notes = "admin toggles approved boolean for the user with the given id", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "OK", response = Void.class),
        @ApiResponse(code = 400, message = "something went wrong", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class),
        @ApiResponse(code = 409, message = "user has future orders", response = Void.class) })
    @RequestMapping(value = "/users/{id}/approved",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin            
    ResponseEntity<Void> usersIdApprovedPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "" ,required=true ) @RequestBody Approve approve) throws ApiException;


    @ApiOperation(value = "deletes the user with the given id", notes = "admin deletes the user with the given id", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "user deleted succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "something went wrong", response = Void.class),
        @ApiResponse(code = 402, message = "user has future and past orders", response = Void.class),
        @ApiResponse(code = 404, message = "user not found", response = Void.class),
        @ApiResponse(code = 409, message = "user has future orders", response = Void.class),
        @ApiResponse(code = 412, message = "user has past orders", response = Void.class) })
    @RequestMapping(value = "/users/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> usersIdDelete(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
         @ApiParam(value = "") @RequestParam(value = "force", required = false) Boolean force) throws ApiException;


    @ApiOperation(value = "admin triggers reset password process for a user", notes = "admin triggers reset password process for a user", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "user's password reset succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class) })
    @RequestMapping(value = "/users/{id}/forgotpwd",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin            
    ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "",required=true ) @PathVariable("id") Long id) throws ApiException;


    @ApiOperation(value = "returns the user with the given id", notes = "returns the user with the given id", response = User.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "User succesfully retrieved", response = User.class),
        @ApiResponse(code = 400, message = "Something can always go wrong", response = User.class),
        @ApiResponse(code = 404, message = "User not found", response = User.class) })
    @RequestMapping(value = "/users/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin            
    ResponseEntity<User> usersIdGet(@ApiParam(value = "",required=true ) @PathVariable("id") Long id) throws ApiException;


    @ApiOperation(value = "admin deletes the picture of the user with the given id", notes = "admin deletes the picture of the user with the given id", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "picture deleted successfully", response = Void.class),
        @ApiResponse(code = 404, message = "picture or user was not found", response = Void.class) })
    @RequestMapping(value = "/users/{id}/picture",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    @CrossOrigin            
    ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "",required=true ) @PathVariable("id") Long id) throws ApiException;


    @ApiOperation(value = "admin uploads a picture for the user with the given id", notes = "admin clicks the \"upload picture\" button and chooses a picture in the dialog. The API uploads the picture and sends it to the server to be stored for the user with the given id", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "picture has been successfully uploaded", response = Void.class),
        @ApiResponse(code = 400, message = "There was a problem uploading the picture", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class) })
    @RequestMapping(value = "/users/{id}/picture",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    @CrossOrigin            
    ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) throws ApiException;


    @ApiOperation(value = "admin gets the picture of the user with the given id", notes = "admin gets the picture of the user with the given id", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = File.class) })
    @RequestMapping(value = "/users/{id}/picture/{token}",
        produces = { "image/jpeg" }, 
        method = RequestMethod.GET)
    @CrossOrigin            
    ResponseEntity<Object> usersIdPictureTokenGet(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "") @RequestParam(value = "token", required = true) String token) throws ApiException;

    
    @ApiOperation(value = "updates user account settings", notes = "admin updates user info from user account settings", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "user edited succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "Something can always go wrong", response = Void.class),
        @ApiResponse(code = 404, message = "User not found", response = Void.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = Void.class) })
    @RequestMapping(value = "/users/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin            
    ResponseEntity<Void> usersIdPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "" ,required=true ) @RequestBody User body) throws ApiException;


    @ApiOperation(value = "admin creates a user", notes = "admin creates a new user", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "User created succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class),
        @ApiResponse(code = 409, message = "User already exists", response = Void.class) })
    @RequestMapping(value = "/users",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin            
    ResponseEntity<Void> usersPost(@ApiParam(value = "" ,required=true ) @RequestBody UserSave body) throws ApiException;

}
