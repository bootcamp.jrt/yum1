/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.stereotype.Controller;

import io.swagger.annotations.*;
import javax.persistence.OptimisticLockException;

import org.bootcamp.yum.api.model.EditedFood;
import org.bootcamp.yum.api.model.FoodEditable;
import org.bootcamp.yum.api.model.FoodSave;
import org.bootcamp.yum.api.model.FoodDelete;
import org.bootcamp.yum.api.model.FoodsList;
import org.bootcamp.yum.api.model.NewEntry;
import org.bootcamp.yum.service.FoodsService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class FoodsApiController implements FoodsApi {

    FoodsService foodsService;

    @Autowired
    public FoodsApiController(FoodsService foodsService) {
        this.foodsService = foodsService;
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<EditedFood> foodsFindByNameNameGet(@ApiParam(value = "", required = true) @PathVariable("name") String name) throws ApiException {

        return new ResponseEntity<EditedFood>(foodsService.foodsFindByNameNameGet(name), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<FoodsList> foodsGet( @ApiParam(value = "") @RequestParam(value = "version", required = false) Integer version,
           @ApiParam(value = "") @RequestParam(value = "archived", required = false) Boolean archived,
           @ApiParam(value = "") @RequestParam(value = "type", required = false) String type,
           @ApiParam(value = "") @RequestParam(value = "sort", required = false) String sort,
           @ApiParam(value = "") @RequestParam(value = "order", required = false) String order,
           @ApiParam(value = "") @RequestParam(value = "page", required = false) Integer page,
           @ApiParam(value = "") @RequestParam(value = "size", required = false) Integer size) throws ApiException {

        return new ResponseEntity<FoodsList>(foodsService.foodsGet(version, archived, type, sort, order, page, size), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<FoodDelete> foodsIdDelete(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
         @ApiParam(value = "") @RequestParam(value = "archive", required = false) Boolean archive) throws ApiException{

        return new ResponseEntity<FoodDelete>(foodsService.foodsIdDelete(id, archive),HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<FoodEditable> foodsIdGet(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "") @RequestParam(value = "edit", required = false) Boolean edit) throws ApiException {

        return new ResponseEntity<FoodEditable>(foodsService.foodsIdGet(id, edit), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<NewEntry> foodsIdPut(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "", required = true) @RequestBody EditedFood body,
            @ApiParam(value = "") @RequestParam(value = "clone", required = false) Boolean clone) throws ApiException {

        try {
            return new ResponseEntity<>(foodsService.foodsIdPut(id, body, clone), HttpStatus.OK);            
        } catch (OptimisticLockException ex) {
            try {
                throw new ApiConcurrentModificationException(foodsService.foodsIdGet(id, clone).getEditedFood());
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<Void> foodsPost(@ApiParam(value = "", required = true) @RequestBody FoodSave body) throws ApiException {

        foodsService.foodsPost(body);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

}
