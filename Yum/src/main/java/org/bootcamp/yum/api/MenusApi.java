/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.DailyMenus;
import org.bootcamp.yum.api.model.Orders;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "menus", description = "the menus API")
@RequestMapping(value = "/api")
public interface MenusApi {

    @ApiOperation(value = "Returns a list containing all daily orders of the month", notes = "The monthly menu fetches all daily orders of the current month.", response = Orders.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Orders.class) })
    @RequestMapping(value = "/menus/monthly",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Orders> menusMonthlyGet() throws ApiException;


    @ApiOperation(value = "returns a list containing all daily orders of the given month", notes = "returns a list containing all daily orders of the given month", response = Orders.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Orders.class),
        @ApiResponse(code = 400, message = "Bad Request.", response = Orders.class) })
    @RequestMapping(value = "/menus/monthly/{year}/{month}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Orders> menusMonthlyYearMonthGet(@ApiParam(value = "",required=true ) @PathVariable("month") Integer month,
        @ApiParam(value = "",required=true ) @PathVariable("year") Integer year) throws ApiException;


    @ApiOperation(value = "Returns a list containing all 5 daily Menus of this week", notes = "The weekly menu fetches the five daily menus of the current week. Each daily menu contains the list of foods offered for that day along with the price it costs. Also the daily menus contains any ordering information for the specific user who made the request. ", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily menus were successfully delivered ", response = DailyMenus.class) })
    @RequestMapping(value = "/menus/weekly",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenus> menusWeeklyGet() throws ApiException;
 

    @ApiOperation(value = "Returns a list containing all 5 daily Menus of the given week", notes = "The weekly menu fetches the five daily menus of a given week. Each daily menu contains the list of foods offered for that day along with the price it costs. Also the daily menus contains any ordering information for the specific user who made the request ", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily menus were successfully delivered ", response = DailyMenus.class),
        @ApiResponse(code = 400, message = "Bad Request.", response = DailyMenus.class) })
    @RequestMapping(value = "/menus/weekly/{year}/{week}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenus> menusWeeklyYearWeekGet(@ApiParam(value = "",required=true ) @PathVariable("week") Integer week,
        @ApiParam(value = "",required=true ) @PathVariable("year") Integer year) throws ApiException;

}
