/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import java.io.ByteArrayInputStream;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.persistence.OptimisticLockException;
import javax.validation.constraints.*;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.api.model.User;
import org.bootcamp.yum.api.model.Approve;
import org.bootcamp.yum.api.model.UsersList;
import org.bootcamp.yum.service.UsersService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class UsersApiController implements UsersApi {

    private UsersService usersService;

    @Autowired
    public UsersApiController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<UsersList> usersGet(@NotNull @ApiParam(value = "", required = true) @RequestParam(value = "sort", required = true) String sort,
            @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "order", required = true) String order,
            @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "page", required = true) Integer page,
            @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "size", required = true) Integer size) throws ApiException {

        return new ResponseEntity<UsersList>(usersService.usersGet(sort, order, page, size), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdApprovedPut(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "", required = true) @RequestBody Approve approve) throws ApiException {

        usersService.usersIdApprovedPut(id, approve);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdDelete(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "") @RequestParam(value = "force", required = false) Boolean force) throws ApiException {

        usersService.usersIdDelete(id, force);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "", required = true) @PathVariable("id") Long id) throws ApiException {

        usersService.usersIdForgotpwdPost(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<User> usersIdGet(@ApiParam(value = "", required = true) @PathVariable("id") Long id) throws ApiException {

        return new ResponseEntity<org.bootcamp.yum.api.model.User>(usersService.usersIdGet(id), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "", required = true) @PathVariable("id") Long id) throws ApiException {
        
        
        usersService.usersIdPictureDelete(id);
        
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) throws ApiException {

        usersService.usersIdPicturePost(id, file);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Object> usersIdPictureTokenGet(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "") @RequestParam(value = "token", required = true) String token) throws ApiException {
        
            if (token.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            try{
                byte[] bytes = usersService.usersIdPictureTokenGet(id, token);
            
            
        
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

            return ResponseEntity
                    .ok()
                    .contentLength(bytes.length)
                    .contentType(MediaType.parseMediaType("image/jpeg"))
                    .body(new InputStreamResource(inputStream));
            }catch (ApiException ex){
                return new ResponseEntity<>(HttpStatus.valueOf(ex.getCode()));
            }

    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersIdPut(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "", required = true) @RequestBody User body) throws ApiException {

        try {
            usersService.usersIdPut(id, body);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                throw new ApiConcurrentModificationException(usersService.usersIdGet(id));
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> usersPost(@ApiParam(value = "", required = true) @RequestBody UserSave body) throws ApiException {

        usersService.usersPost(body);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
