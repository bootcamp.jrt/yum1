/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.DailyMenuSave;
import org.bootcamp.yum.api.model.DailyMenusChef;
import org.bootcamp.yum.api.model.NewEntry;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "dailyMenus", description = "the dailyMenus API")
@RequestMapping(value = "/api")
public interface DailyMenusApi {

    @ApiOperation(value = "updates DailyMenu by ID", notes = "updates DailyMenu by ID", response = DailyMenuSave.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily Menu was successfully updated", response = DailyMenuSave.class),
        @ApiResponse(code = 400, message = "Daily Menu could not be updated", response = DailyMenuSave.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = DailyMenuSave.class),
        @ApiResponse(code = 410, message = "You deleted this menu earlier", response = DailyMenuSave.class) })
    @RequestMapping(value = "/dailyMenus/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<DailyMenuSave> dailyMenusIdPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "" ,required=true ) @RequestBody DailyMenuSave body, @Valid Errors errors) throws ApiException;


    @ApiOperation(value = "Returns a list containing all daily menus of the current month", notes = "Returns a list containing all daily menus of the current month", response = DailyMenusChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = DailyMenusChef.class) })
    @RequestMapping(value = "/dailyMenus/monthly",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenusChef> dailyMenusMonthlyGet() throws ApiException;


    @ApiOperation(value = "Returns a list containing all daily Menus of the given month", notes = "The monthly menu fetches the daily menus of a month. Each daily menu contains the list of foods offered for that day along with the price it costs and the quantity ordered by all users. ", response = DailyMenusChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = DailyMenusChef.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = DailyMenusChef.class) })
    @RequestMapping(value = "/dailyMenus/monthly/{year}/{month}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenusChef> dailyMenusMonthlyYearMonthGet(@ApiParam(value = "",required=true ) @PathVariable("month") Integer month,
        @ApiParam(value = "",required=true ) @PathVariable("year") Integer year) throws ApiException;


   @ApiOperation(value = "Creates a new DailyMenu", notes = "Creates a new DailyMenu", response = NewEntry.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "DailyMenu was successfuly created", response = NewEntry.class),
        @ApiResponse(code = 400, message = "Daily Menu could not be created", response = NewEntry.class),
        @ApiResponse(code = 409, message = "A menu was already created for that day", response = DailyMenuSave.class) })
    @RequestMapping(value = "/dailyMenus",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<NewEntry> dailyMenusPost(@ApiParam(value = "" ,required=true ) @RequestBody DailyMenuSave body) throws ApiException;
}
