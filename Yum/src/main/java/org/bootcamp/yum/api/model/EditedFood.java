/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.data.entity.Food;

/**
 * EditedFood
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class EditedFood {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("type")
    private String type = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("price")
    private Double price = null;

    @JsonProperty("version")
    private Integer version = null;

    public EditedFood() {
    }

    public EditedFood(Food food) {
        this.id = food.getId();
        this.name = food.getName();
        this.type = food.getFoodType().toString();
        this.description = food.getDescription();
        this.price = food.getPrice();
        this.version = food.getVersion();
    }

    public EditedFood id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
  *
     */
    @ApiModelProperty(value = "")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EditedFood name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
  *
     */
    @ApiModelProperty(value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EditedFood type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     *
     * @return type
  *
     */
    @ApiModelProperty(value = "")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EditedFood description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
  *
     */
    @ApiModelProperty(value = "")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EditedFood price(Double price) {
        this.price = price;
        return this;
    }

    /**
     * Get price
     *
     * @return price
  *
     */
    @ApiModelProperty(value = "")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public EditedFood version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
  *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EditedFood editedFood = (EditedFood) o;
        return Objects.equals(this.id, editedFood.id)
                && Objects.equals(this.name, editedFood.name)
                && Objects.equals(this.type, editedFood.type)
                && Objects.equals(this.description, editedFood.description)
                && Objects.equals(this.price, editedFood.price)
                && Objects.equals(this.version, editedFood.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, description, price, version);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class EditedFood {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    price: ").append(toIndentedString(price)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
