/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;


import org.bootcamp.yum.exception.ApiException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.EditedFood;
import org.bootcamp.yum.api.model.FoodEditable;
import org.bootcamp.yum.api.model.FoodSave;
import org.bootcamp.yum.api.model.FoodDelete;
import org.bootcamp.yum.api.model.FoodsList;
import org.bootcamp.yum.api.model.NewEntry;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "foods", description = "the foods API")
@RequestMapping(value = "/api")
public interface FoodsApi {

    @ApiOperation(value = "returns a non archived food by its name", notes = "returns a non archived food by its name", response = EditedFood.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "food exists and is not archived", response = EditedFood.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = EditedFood.class),
        @ApiResponse(code = 404, message = "food does not exist or is archived", response = EditedFood.class) })
    @RequestMapping(value = "/foods/findByName/{name}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<EditedFood> foodsFindByNameNameGet(@ApiParam(value = "",required=true ) @PathVariable("name") String name) throws ApiException;

    @ApiOperation(value = "Returns foods list", notes = "Returns foods list", response = FoodsList.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = FoodsList.class),
        @ApiResponse(code = 304, message = "List not modified", response = FoodsList.class) })
    @RequestMapping(value = "/foods",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
	@CrossOrigin		
    ResponseEntity<FoodsList> foodsGet( @ApiParam(value = "") @RequestParam(value = "version", required = false) Integer version,
         @ApiParam(value = "") @RequestParam(value = "archived", required = false) Boolean archived,
         @ApiParam(value = "") @RequestParam(value = "type", required = false) String type,
         @ApiParam(value = "") @RequestParam(value = "sort", required = false) String sort,
         @ApiParam(value = "") @RequestParam(value = "order", required = false) String order,
         @ApiParam(value = "") @RequestParam(value = "page", required = false) Integer page,
         @ApiParam(value = "") @RequestParam(value = "size", required = false) Integer size) throws ApiException;

    @ApiOperation(value = "Deletes or archives an existing food.", notes = "Checks for the food's orderings and makes deletes or archives it.", response = FoodDelete.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "food was successfully deleted or archived", response = FoodDelete.class),
        @ApiResponse(code = 404, message = "food not found", response = FoodDelete.class),
        @ApiResponse(code = 406, message = "food can not be deleted", response = FoodDelete.class) })
    @RequestMapping(value = "/foods/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
	@CrossOrigin		
    ResponseEntity<FoodDelete> foodsIdDelete(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
         @ApiParam(value = "") @RequestParam(value = "archive", required = false) Boolean archive) throws ApiException;

    @ApiOperation(value = "Returns the requested food to edit", notes = "Returns the requested food to edit", response = FoodEditable.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "ok", response = FoodEditable.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = FoodEditable.class),
        @ApiResponse(code = 404, message = "Food does not exist.", response = FoodEditable.class) })
    @RequestMapping(value = "/foods/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<FoodEditable> foodsIdGet(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
         @ApiParam(value = "") @RequestParam(value = "edit", required = false) Boolean edit) throws ApiException;


    @ApiOperation(value = "Edits or clones an existing food", notes = "Checks for the version of the food to solve the concurrent editing problem, proceeds with the editing or proposes to clone or rejects it.", response = NewEntry.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "food was successfully modified", response = NewEntry.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = Void.class),
        @ApiResponse(code = 404, message = "food not found", response = Void.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = EditedFood.class) })
    @RequestMapping(value = "/foods/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<NewEntry> foodsIdPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "" ,required=true ) @RequestBody EditedFood body,
         @ApiParam(value = "") @RequestParam(value = "clone", required = false) Boolean clone) throws ApiException;


    @ApiOperation(value = "Creates a new food", notes = "Creates a new food for global menu", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "Food item has been created", response = Void.class),
        @ApiResponse(code = 400, message = "price or type is not valid", response = Void.class),
        @ApiResponse(code = 406, message = "food name already exists", response = Void.class) })
    @RequestMapping(value = "/foods",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> foodsPost(@ApiParam(value = "" ,required=true ) @RequestBody FoodSave body) throws ApiException;

}
