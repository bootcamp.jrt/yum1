/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.Settings;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "globalsettings", description = "the globalsettings API")
@RequestMapping(value = "/api")
public interface GlobalsettingsApi {

    @ApiOperation(value = "get global settings", notes = "get global settings", response = Settings.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Settings.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Settings.class) })
    @RequestMapping(value = "/globalsettings",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Settings> globalsettingsGet() throws ApiException;


    @ApiOperation(value = "admin editing of global settings", notes = "admin request for editing global settings", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "settings succesfully edited", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = Void.class) })
    @RequestMapping(value = "/globalsettings",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> globalsettingsPut(@ApiParam(value = "" ,required=true ) @Valid @RequestBody Settings body,Errors errors) throws ApiException;
}
