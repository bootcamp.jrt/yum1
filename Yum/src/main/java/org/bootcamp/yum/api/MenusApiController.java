/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import org.joda.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.DailyMenus;
import org.bootcamp.yum.api.model.Orders;
import org.bootcamp.yum.service.MenuService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class MenusApiController implements MenusApi {

    MenuService menuService;

    @Autowired
    public MenusApiController(MenuService menuService) {
        this.menuService = menuService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Orders> menusMonthlyGet() throws ApiException {

        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthOfYear();

        return new ResponseEntity<>(menuService.getMenusMonthly(month, year), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Orders> menusMonthlyYearMonthGet(@ApiParam(value = "", required = true) @PathVariable("month") Integer month,
            @ApiParam(value = "", required = true) @PathVariable("year") Integer year) throws ApiException {

        return new ResponseEntity<>(menuService.getMenusMonthly(month, year), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<DailyMenus> menusWeeklyGet() throws ApiException {

        int week = LocalDate.now().getWeekOfWeekyear();
        int year = LocalDate.now().getYear();
        System.out.println(year);
        System.out.println(week);

        return new ResponseEntity<>(menuService.getWeeklyMenus(week, year), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<DailyMenus> menusWeeklyYearWeekGet(@ApiParam(value = "", required = true) @PathVariable("week") Integer week,
            @ApiParam(value = "", required = true) @PathVariable("year") Integer year) throws ApiException {

        return new ResponseEntity<>(menuService.getWeeklyMenus(week, year), HttpStatus.OK);
    }
}
