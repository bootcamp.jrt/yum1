/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.*;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.OrderSave;
import org.bootcamp.yum.api.model.OrderSummary;
import org.bootcamp.yum.api.model.OrdersChef;import org.bootcamp.yum.api.model.DailyMenu;
import org.bootcamp.yum.api.model.NewEntry;



@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "orders", description = "the orders API")
@RequestMapping(value = "/api")
public interface OrdersApi {

    @ApiOperation(value = "Returns daily orders summary for given daily menu Id", notes = "Returns daily orders summary for given daily menu Id", response = OrderSummary.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = OrderSummary.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = OrderSummary.class),
        @ApiResponse(code = 404, message = "Daily menu with this Id could not be found", response = OrderSummary.class) })
    @RequestMapping(value = "/orders/daily/{dailyMenuId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<OrderSummary> ordersDailyDailyMenuIdGet(@ApiParam(value = "", required = true) @PathVariable("dailyMenuId") Long dailyMenuId) throws ApiException;

    @ApiOperation(value = "deletes order if it is not final", notes = "deletes order if it is not final", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "order deleted successfully", response = Void.class),
        @ApiResponse(code = 400, message = "bad request. Cannot delete the order is final", response = Void.class),
        @ApiResponse(code = 410, message = "You cancelled this order earlier", response = DailyMenu.class) })
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "date", required = true) String date,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion) throws ApiException;

    @ApiOperation(value = "gets the order by id", notes = "gets the order by id", response = DailyMenu.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = DailyMenu.class),
        @ApiResponse(code = 400, message = "bad request", response = DailyMenu.class),
        @ApiResponse(code = 410, message = "You cancelled this order earlier", response = DailyMenu.class) })
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenu> ordersIdGet(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "date", required = true) String date,
         @NotNull @ApiParam(value = "", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion) throws ApiException;

    @ApiOperation(value = "updates order found by its id", notes = "updates order found by its id", response = OrderSave.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "order updated successfully", response = NewEntry.class),
        @ApiResponse(code = 400, message = "bad request", response = Void.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = OrderSave.class),
        @ApiResponse(code = 410, message = "You cancelled this order earlier", response = NewEntry.class) })
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<NewEntry> ordersIdPut(@ApiParam(value = "",required=true ) @PathVariable("id") Long id,
        @ApiParam(value = "" ,required=true ) @RequestBody OrderSave body,
         @ApiParam(value = "") @RequestParam(value = "email", required = false) Boolean email) throws ApiException;

    @ApiOperation(value = "Returns daily orders of the current month", notes = "The monthly menu fetches all daily orders of the current month.", response = OrdersChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = OrdersChef.class) })
    @RequestMapping(value = "/orders/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<OrdersChef> ordersMonthlyGet() throws ApiException;

    @ApiOperation(value = "Returns daily orders for given month", notes = "Returns daily orders for given month", response = OrdersChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = OrdersChef.class),
        @ApiResponse(code = 400, message = "Something went wrong.", response = OrdersChef.class) })
    @RequestMapping(value = "/orders/monthly/{year}/{month}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<OrdersChef> ordersMonthlyYearMonthGet(@ApiParam(value = "", required = true) @PathVariable("month") Integer month,
            @ApiParam(value = "", required = true) @PathVariable("year") Integer year) throws ApiException;

    @ApiOperation(value = "Places order in specific day", notes = "Creates a dailyOrder with new orderItems", response = NewEntry.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "order has been placed", response = NewEntry.class),
        @ApiResponse(code = 400, message = "bad request", response = NewEntry.class),
        @ApiResponse(code = 409, message = "An order already exists for this day", response = NewEntry.class) })
    @RequestMapping(value = "/orders",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
	@CrossOrigin
    ResponseEntity<NewEntry> ordersPost(@ApiParam(value = "" ,required=true ) @RequestBody OrderSave body,
         @ApiParam(value = "") @RequestParam(value = "email", required = false) Boolean email) throws ApiException;

}
