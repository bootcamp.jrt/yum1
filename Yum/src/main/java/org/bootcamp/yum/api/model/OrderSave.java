/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItem;

/**
 * OrderSave
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T19:45:14.528+03:00")

public class OrderSave {

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("dailyMenuId")
    private Long dailyMenuId = null;

    @JsonProperty("dailyMenuVersion")
    private Integer dailyMenuVersion = null;

    @JsonProperty("orderVersion")
    private Integer orderVersion = null;

    @JsonProperty("orderItems")
    private List<OrderSaveOrderItems> orderItems = new ArrayList<OrderSaveOrderItems>();

    public OrderSave() {
    }
    
    public OrderSave(DailyOrder dao) {

        this.date = dao.getDailyMenu().getDate();
        this.dailyMenuId = dao.getDailyMenu().getId();
        this.dailyMenuVersion = dao.getDailyMenu().getVersion();
        this.orderVersion = dao.getVersion();

        for (OrderItem orderItem : dao.getOrderItems()) {
            addOrderItemsItem(new OrderSaveOrderItems(orderItem));
        }

    }

    public String toJsonString() {

        String toRet = "{\n"
                + "  \"date\": " + this.date + ",\n"
                + "  \"dailyMenuId\": " + this.dailyMenuId + ",\n"
                + "  \"dailyMenuVersion\": " + this.dailyMenuVersion + ",\n"
                + "  \"orderVersion\": " + this.orderVersion + ",\n"
                + "  \"orderItems\": [\n";

        for (Iterator<OrderSaveOrderItems> iterator = orderItems.iterator(); iterator.hasNext();) {
            OrderSaveOrderItems next = iterator.next();

            toRet += next.toJsonString() + (iterator.hasNext() ? ",\n" : "\n");

        }

        toRet += "  ]\n" + "}";

        return toRet;
    }

    public OrderSave date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
  *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public OrderSave dailyMenuId(Long dailyMenuId) {
        this.dailyMenuId = dailyMenuId;
        return this;
    }

    /**
     * Get dailyMenuId
     *
     * @return dailyMenuId
  *
     */
    @ApiModelProperty(value = "")
    public Long getDailyMenuId() {
        return dailyMenuId;
    }

    public void setDailyMenuId(Long dailyMenuId) {
        this.dailyMenuId = dailyMenuId;
    }

    public OrderSave dailyMenuVersion(Integer dailyMenuVersion) {
        this.dailyMenuVersion = dailyMenuVersion;
        return this;
    }

    /**
     * Get dailyMenuVersion
     *
     * @return dailyMenuVersion
  *
     */
    @ApiModelProperty(value = "")
    public Integer getDailyMenuVersion() {
        return dailyMenuVersion;
    }

    public void setDailyMenuVersion(Integer dailyMenuVersion) {
        this.dailyMenuVersion = dailyMenuVersion;
    }

    public OrderSave orderVersion(Integer orderVersion) {
        this.orderVersion = orderVersion;
        return this;
    }

    /**
     * Get orderVersion
     *
     * @return orderVersion
  *
     */
    @ApiModelProperty(value = "")
    public Integer getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(Integer orderVersion) {
        this.orderVersion = orderVersion;
    }

    public OrderSave orderItems(List<OrderSaveOrderItems> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    public OrderSave addOrderItemsItem(OrderSaveOrderItems orderItemsItem) {
        this.orderItems.add(orderItemsItem);
        return this;
    }

    /**
     * Get orderItems
     *
     * @return orderItems
  *
     */
    @ApiModelProperty(value = "")
    public List<OrderSaveOrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderSaveOrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderSave orderSave = (OrderSave) o;
        return Objects.equals(this.date, orderSave.date)
                && Objects.equals(this.dailyMenuId, orderSave.dailyMenuId)
                && Objects.equals(this.dailyMenuVersion, orderSave.dailyMenuVersion)
                && Objects.equals(this.orderVersion, orderSave.orderVersion)
                && Objects.equals(this.orderItems, orderSave.orderItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, dailyMenuId, dailyMenuVersion, orderVersion, orderItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderSave {\n");

        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    dailyMenuId: ").append(toIndentedString(dailyMenuId)).append("\n");
        sb.append("    dailyMenuVersion: ").append(toIndentedString(dailyMenuVersion)).append("\n");
        sb.append("    orderVersion: ").append(toIndentedString(orderVersion)).append("\n");
        sb.append("    orderItems: ").append(toIndentedString(orderItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
