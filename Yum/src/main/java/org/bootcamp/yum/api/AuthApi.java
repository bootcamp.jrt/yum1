/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;


import org.bootcamp.yum.exception.ApiException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;

import javax.validation.Valid;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Token;
import org.bootcamp.yum.api.model.ChangePwd;
import org.bootcamp.yum.api.model.ForgotPwd;
import org.bootcamp.yum.api.model.Policy;
import org.bootcamp.yum.api.model.UserSave;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "auth", description = "the auth API")
@RequestMapping(value = "/api")
public interface AuthApi {

    @ApiOperation(value = "user resets the password", notes = "user types the new password twice and clicks \"Change Password\". The API changes the password of the user.", response = Void.class, tags={ "auth", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "The password has been successfully changed.", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class),
        @ApiResponse(code = 403, message = "User is not approved", response = Void.class) })
    @RequestMapping(value = "/auth/changepwd",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> authChangepwdPut(@ApiParam(value = "The email/password" ,required=true ) @RequestBody ChangePwd body, @Valid Errors errors) throws ApiException;
    
    @ApiOperation(value = "user resets the password", notes = "user resets the password", response = Void.class, tags={ "auth", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "user's password reset succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class) })
    @RequestMapping(value = "/auth/forgotpwd",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> authForgotpwdPost(@ApiParam(value = "The email/password" ,required=true ) @RequestBody ForgotPwd body, @Valid Errors errors) throws ApiException;


    @ApiOperation(value = "", notes = "Allow users to log in, and to receive a Token", response = Token.class, tags={ "auth", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Login Success", response = Token.class),
        @ApiResponse(code = 403, message = "If user is not found (bad credentials) OR if user can not login (not approved)", response = Token.class) })
    @RequestMapping(value = "/auth/login",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Token> authLoginPost(@ApiParam(value = "The email/password" ,required=true ) @RequestBody Login body, @Valid Errors errors) throws ApiException;

    @ApiOperation(value = "anonymous user gets terms of use and privacy policy", notes = "anonymous user gets terms of use and privacy policy", response = Policy.class, tags={ "auth", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Policy.class) })
    @RequestMapping(value = "/auth/policy",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Policy> authPolicyGet();

    @ApiOperation(value = "", notes = "Register a new user", response = Void.class, tags={ "auth", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "User created succesfully", response = Void.class),
        @ApiResponse(code = 400, message = "Something went wrong", response = Void.class),
        @ApiResponse(code = 409, message = "User already exists", response = Void.class) })
    @RequestMapping(value = "/auth/register",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> authRegisterPost(@ApiParam(value = "" ,required=true ) @RequestBody UserSave body, @Valid Errors errors) throws ApiException;

}
