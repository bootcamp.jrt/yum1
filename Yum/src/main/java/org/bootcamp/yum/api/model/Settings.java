/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;
import org.joda.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.data.entity.Yum;

/**
 * Settings
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class Settings {

    @JsonProperty("deadline")
    private String deadline = null;

    @JsonProperty("currency")
    private String currency = null;

    @JsonProperty("notes")
    private String notes = null;

    @JsonProperty("terms")
    private String terms = null;

    @JsonProperty("policy")
    private String policy = null;

    @JsonProperty("version")
    private Integer version = null;

    public Settings() {
    }

    public Settings(Yum yum) {

        this.setCurrency(yum.getCurrency());
        this.setDeadline(yum.getDeadline().toString());
        this.setNotes(yum.getNotes());
        this.setPolicy(yum.getPrivacy());  //TODO: match the names!
        this.setTerms(yum.getTerms());
        this.setVersion(yum.getVersion());
    }

    public Settings deadline(String deadline) {
        this.deadline = deadline;
        return this;
    }

    
    @JsonIgnore
    public boolean isValid() {
        try {
            LocalTime.parse(getDeadline());
        } catch (Exception ex) {
            return false;
        }

        return true;
    }    
    /**
     * Get deadline
     *
     * @return deadline
  *
     */
    @ApiModelProperty(value = "")
    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Settings currency(String currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Get currency
     *
     * @return currency
  *
     */
    @ApiModelProperty(value = "")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Settings notes(String notes) {
        this.notes = notes;
        return this;
    }

    /**
     * Get notes
     *
     * @return notes
  *
     */
    @ApiModelProperty(value = "")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Settings terms(String terms) {
        this.terms = terms;
        return this;
    }

    /**
     * Get terms
     *
     * @return terms
  *
     */
    @ApiModelProperty(value = "")
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public Settings policy(String policy) {
        this.policy = policy;
        return this;
    }

    /**
     * Get policy
     *
     * @return policy
  *
     */
    @ApiModelProperty(value = "")
    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public Settings version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
  *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Settings settings = (Settings) o;
        return Objects.equals(this.deadline, settings.deadline)
                && Objects.equals(this.currency, settings.currency)
                && Objects.equals(this.notes, settings.notes)
                && Objects.equals(this.terms, settings.terms)
                && Objects.equals(this.policy, settings.policy)
                && Objects.equals(this.version, settings.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deadline, currency, notes, terms, policy, version);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Settings {\n");

        sb.append("    deadline: ").append(toIndentedString(deadline)).append("\n");
        sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
        sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
        sb.append("    terms: ").append(toIndentedString(terms)).append("\n");
        sb.append("    policy: ").append(toIndentedString(policy)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
