/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import java.io.ByteArrayInputStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.persistence.OptimisticLockException;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserSave;
import org.bootcamp.yum.service.SettingsService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class SettingsApiController implements SettingsApi {

    private SettingsService settingsService;

    @Autowired
    public SettingsApiController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<UserGet> settingsGet() throws ApiException {
        // get the logged in user, pass it in the json object constractor and return the json object
        return new ResponseEntity<UserGet>(settingsService.settingsGet(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPictureDelete() throws ApiException {
        settingsService.settingsPictureDelete();
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) throws ApiException {

        settingsService.settingsPicturePost(file);
    
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Object> settingsPictureTokenGet(@ApiParam(value = "") @RequestParam(value = "token", required = true) String token) throws ApiException {
        
            if (token.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            try{
            byte[] bytes = settingsService.settingsPictureTokenGet(token);
            
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

            return ResponseEntity
                    .ok()
                    .contentLength(bytes.length)
                    .contentType(MediaType.parseMediaType("image/jpeg"))
                    .body(new InputStreamResource(inputStream));
            }catch (ApiException ex){
            
                return new ResponseEntity<>(HttpStatus.valueOf(ex.getCode())); 
            }

    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPut(@ApiParam(value = "", required = true) @RequestBody UserSave body) throws ApiException {

        try {
            settingsService.settingsPut(body);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            throw new ApiConcurrentModificationException(settingsService.getUserSaveOfLoggedInUser());
        }

    }

}
