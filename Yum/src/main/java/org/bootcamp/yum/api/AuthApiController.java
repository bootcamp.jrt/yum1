/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;


import org.bootcamp.yum.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.Errors;

import javax.validation.Valid;
import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.ChangePwd;
import org.bootcamp.yum.api.model.ForgotPwd;
import org.bootcamp.yum.api.model.Policy;
import org.bootcamp.yum.service.AuthService;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Token;
import org.bootcamp.yum.api.model.UserSave;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class AuthApiController implements AuthApi {

    private AuthService authService;
    
    @Autowired
    public AuthApiController(AuthService authService) {
        this.authService = authService;
    }

    public ResponseEntity<Void> authChangepwdPut(@ApiParam(value = "The email/password", required = true) @RequestBody ChangePwd body, @Valid Errors errors) throws ApiException {

        authService.authChangepwdPost(body);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    public ResponseEntity<Void> authForgotpwdPost(@ApiParam(value = "The email/password", required = true) @RequestBody ForgotPwd body, @Valid Errors errors) throws ApiException {

        authService.authForgotpwdPost(body);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    public ResponseEntity<Token> authLoginPost(@ApiParam(value = "The email/password", required = true) @RequestBody Login body, @Valid Errors errors) throws ApiException {

        Token token = authService.authLoginPost(body);
        return new ResponseEntity<Token>(token, HttpStatus.OK);
    }

    public ResponseEntity<Policy> authPolicyGet() {
        
        return new ResponseEntity<Policy>(authService.authPolicyGet(), HttpStatus.OK);
    }
    
    public ResponseEntity<Void> authRegisterPost(@ApiParam(value = "", required = true) @RequestBody UserSave body, @Valid Errors errors) throws ApiException {

        authService.authRegisterPost(body);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }
}
