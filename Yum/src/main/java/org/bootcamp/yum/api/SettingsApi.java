/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;

import org.bootcamp.yum.exception.ApiException;
import java.io.File;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserSave;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")

@Api(value = "settings", description = "the settings API")
@RequestMapping(value = "/api")
public interface SettingsApi {

    @ApiOperation(value = "returns the current user's details", notes = "returns the current user's details", response = UserGet.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "profile data succesfully fetched from database", response = UserGet.class) })
    @RequestMapping(value = "/settings",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<UserGet> settingsGet() throws ApiException;


    @ApiOperation(value = "user deletes his/her picture", notes = "user deletes his/her picture", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "picture deleted successfully", response = Void.class),
        @ApiResponse(code = 404, message = "picture was not found", response = Void.class) })
    @RequestMapping(value = "/settings/picture",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> settingsPictureDelete() throws ApiException;


    @ApiOperation(value = "user uploads his/her picture", notes = "user clicks the \"upload picture\" button and chooses his/her picture in the dialog. The API uploads the picture and sends it to the server", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "picture has been successfully uploaded", response = Void.class),
        @ApiResponse(code = 400, message = "There was a problem uploading the picture", response = Void.class) })
    @RequestMapping(value = "/settings/picture",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) throws ApiException;

    
    

    @ApiOperation(value = "user gets his picture", notes = "user gets his picture", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = File.class) })
    @RequestMapping(value = "/settings/picture/{token}",
        produces = { "image/jpeg" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Object> settingsPictureTokenGet(@ApiParam(value = "") @RequestParam(value = "token", required = true) String token) throws ApiException;

    @ApiOperation(value = "change profile data", notes = "change profile data", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "account settings updated successfully", response = Void.class),
        @ApiResponse(code = 400, message = "bad request", response = Void.class),
        @ApiResponse(code = 409, message = "concurrent modification error", response = Void.class) })
    @RequestMapping(value = "/settings",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> settingsPut(@ApiParam(value = "" ,required=true ) @RequestBody UserSave body) throws ApiException;
}
