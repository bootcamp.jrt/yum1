/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;



import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;

import io.swagger.annotations.*;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import org.bootcamp.yum.service.GlobalSettingsService;
import org.bootcamp.yum.api.model.Settings;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class GlobalsettingsApiController implements GlobalsettingsApi {

    private GlobalSettingsService globalSettingsService;

    @Autowired
    public GlobalsettingsApiController(GlobalSettingsService globalSettingsService) {
        this.globalSettingsService = globalSettingsService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Settings> globalsettingsGet() throws ApiException {
        return new ResponseEntity<Settings>(globalSettingsService.globalsettingsGet(),HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> globalsettingsPut(@ApiParam(value = "", required = true) @Valid @RequestBody Settings body, Errors errors) throws ApiException {

        try{
            globalSettingsService.globalsettingsPut(body);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            throw new ApiConcurrentModificationException(globalSettingsService.getSettings());
        }
    }

}
