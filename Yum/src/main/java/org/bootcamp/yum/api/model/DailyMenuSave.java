/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.Food;

/**
 * DailyMenuSave
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class DailyMenuSave {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("version")
    private Integer version = null;

    @JsonProperty("foods")
    private List<DailyMenuSaveFoods> foods = new ArrayList<DailyMenuSaveFoods>();

    public DailyMenuSave() {
    }
    
    public DailyMenuSave(long id) {
        this.id = id;
        this.version = -1;
    }

    public DailyMenuSave(DailyMenu dailyMenu) {
        this.id = dailyMenu.getId();
        this.date = dailyMenu.getDate();
        this.version = dailyMenu.getVersion();
        for (Food food : dailyMenu.getFoods()) {
            foods.add(new DailyMenuSaveFoods(food.getId()));
        }
    }

    public String toJsonString() {

        String out = "{\n"
                + "  \"date\": \"" + this.date + "\",\n"
                + "  \"version\": " + this.version + ",\n"
                + "  \"foods\": [\n";

        for (DailyMenuSaveFoods food : foods) {
            out += "    {\n"
                    + "      \"id\": " + food.getId() + "\n"
                    + "    },\n";
        }

        out = out.substring(0, out.length() - 2);
        out += "\n  ]\n"
                + "}";

        return out;

    }

    public DailyMenuSave id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DailyMenuSave date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DailyMenuSave version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public DailyMenuSave foods(List<DailyMenuSaveFoods> foods) {
        this.foods = foods;
        return this;
    }

    public DailyMenuSave addFoodsItem(DailyMenuSaveFoods foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
     *
     */
    @ApiModelProperty(value = "")
    public List<DailyMenuSaveFoods> getFoods() {
        return foods;
    }

    public void setFoods(List<DailyMenuSaveFoods> foods) {
        this.foods = foods;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DailyMenuSave dailyMenuSave = (DailyMenuSave) o;
        return Objects.equals(this.id, dailyMenuSave.id)
                && Objects.equals(this.date, dailyMenuSave.date)
                && Objects.equals(this.version, dailyMenuSave.version)
                && Objects.equals(this.foods, dailyMenuSave.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, version, foods);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DailyMenuSave {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
