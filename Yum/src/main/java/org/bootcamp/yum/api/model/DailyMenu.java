/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;

/**
 * DailyMenu
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class DailyMenu   {
  @JsonProperty("date")
  private LocalDate date = null;

  @JsonProperty("menuId")
  private Long menuId = null;

  @JsonProperty("menuVersion")
  private Integer menuVersion = null;

  @JsonProperty("orderId")
  private Long orderId = null;

  @JsonProperty("orderVersion")
  private Integer orderVersion = null;

  @JsonProperty("orderIsFinal")
  private Boolean orderIsFinal = null;

  @JsonProperty("foods")
  private List<DailyMenuFoods> foods = new ArrayList<DailyMenuFoods>();
  
  public DailyMenu() {}

  public DailyMenu(org.bootcamp.yum.data.entity.DailyMenu menu, Boolean isPast) {
      
      this.date = menu.getDate();
      this.menuId = menu.getId();
      this.menuVersion = menu.getVersion();

      this.orderId = 0L;
      this.orderVersion = 0;
      this.orderIsFinal = isPast;
      
      this.foods = new ArrayList<DailyMenuFoods>();
      for (Food food : menu.getFoods()) {
          if(!food.isArchived()){
              this.foods.add(new DailyMenuFoods(food));
          }
      }
  }
  
  public DailyMenu(DailyOrder order, LocalTime deadLine) {
      
      this.date = order.getDailyMenu().getDate();
      this.menuId = order.getDailyMenu().getId();
      this.menuVersion = order.getDailyMenu().getVersion();

      this.fillWithOrder(order, deadLine);
  }
  
  
  public void fillWithOrder(DailyOrder order, LocalTime deadLine){
      
      this.orderId = order.getId();
      this.orderVersion = order.getVersion();
      this.orderIsFinal = order.isFinal(deadLine);
      
      HashMap<Long, DailyMenuFoods> foodsMap = new HashMap<Long, DailyMenuFoods>();
      
      this.foods = new ArrayList<DailyMenuFoods>();
      for (Food food : order.getDailyMenu().getFoods()) {
          DailyMenuFoods dmf = new DailyMenuFoods(food);
          foodsMap.put(food.getId(), dmf);
      }
      
      for (OrderItem orderItem : order.getOrderItems()) {
          DailyMenuFoods dmf = foodsMap.get(orderItem.getId().getFoodId());
          dmf.setQuantity(dmf.getQuantity() + orderItem.getQuantity());
      }

      this.foods = new ArrayList<DailyMenuFoods>();
      for (DailyMenuFoods dmf : foodsMap.values()) {
          if(!dmf.getArchived() || dmf.getQuantity() > 0){
              this.foods.add(dmf);
          }
      }

  }
  
  public DailyMenu date(LocalDate date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @ApiModelProperty(value = "")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public DailyMenu menuId(Long menuId) {
    this.menuId = menuId;
    return this;
  }

   /**
   * Get menuId
   * @return menuId
  **/
  @ApiModelProperty(value = "")
  public Long getMenuId() {
    return menuId;
  }

  public void setMenuId(Long menuId) {
    this.menuId = menuId;
  }

  public DailyMenu menuVersion(Integer menuVersion) {
    this.menuVersion = menuVersion;
    return this;
  }

   /**
   * Get menuVersion
   * @return menuVersion
  **/
  @ApiModelProperty(value = "")
  public Integer getMenuVersion() {
    return menuVersion;
  }

  public void setMenuVersion(Integer menuVersion) {
    this.menuVersion = menuVersion;
  }

  public DailyMenu orderId(Long orderId) {
    this.orderId = orderId;
    return this;
  }

   /**
   * the id of the order or 0 if it doesn't have
   * @return orderId
  **/
  @ApiModelProperty(value = "the id of the order or 0 if it doesn't have")
  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public DailyMenu orderVersion(Integer orderVersion) {
    this.orderVersion = orderVersion;
    return this;
  }

   /**
   * Get orderVersion
   * @return orderVersion
  **/
  @ApiModelProperty(value = "")
  public Integer getOrderVersion() {
    return orderVersion;
  }

  public void setOrderVersion(Integer orderVersion) {
    this.orderVersion = orderVersion;
  }

  public DailyMenu orderIsFinal(Boolean orderIsFinal) {
    this.orderIsFinal = orderIsFinal;
    return this;
  }

   /**
   * Get orderIsFinal
   * @return orderIsFinal
  **/
  @ApiModelProperty(value = "")
  public Boolean getOrderIsFinal() {
    return orderIsFinal;
  }

  public void setOrderIsFinal(Boolean orderIsFinal) {
    this.orderIsFinal = orderIsFinal;
  }

  public DailyMenu foods(List<DailyMenuFoods> foods) {
    this.foods = foods;
    return this;
  }

  public DailyMenu addFoodsItem(DailyMenuFoods foodsItem) {
    this.foods.add(foodsItem);
    return this;
  }

   /**
   * Get foods
   * @return foods
  **/
  @ApiModelProperty(value = "")
  public List<DailyMenuFoods> getFoods() {
    return foods;
  }

  public void setFoods(List<DailyMenuFoods> foods) {
    this.foods = foods;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyMenu dailyMenu = (DailyMenu) o;
    return Objects.equals(this.date, dailyMenu.date) &&
        Objects.equals(this.menuId, dailyMenu.menuId) &&
        Objects.equals(this.menuVersion, dailyMenu.menuVersion) &&
        Objects.equals(this.orderId, dailyMenu.orderId) &&
        Objects.equals(this.orderVersion, dailyMenu.orderVersion) &&
        Objects.equals(this.orderIsFinal, dailyMenu.orderIsFinal) &&
        Objects.equals(this.foods, dailyMenu.foods);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, menuId, menuVersion, orderId, orderVersion, orderIsFinal, foods);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyMenu {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    menuId: ").append(toIndentedString(menuId)).append("\n");
    sb.append("    menuVersion: ").append(toIndentedString(menuVersion)).append("\n");
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    orderVersion: ").append(toIndentedString(orderVersion)).append("\n");
    sb.append("    orderIsFinal: ").append(toIndentedString(orderIsFinal)).append("\n");
    sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

