/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;



/**
 * OrderSummary
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T17:47:06.075+03:00")

public class OrderSummary {

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("foods")
    private List<OrdersChefInnerFoods> foods = new ArrayList<OrdersChefInnerFoods>();

    @JsonProperty("dailyOrders")
    private List<OrderSummaryDailyOrders> dailyOrders = new ArrayList<OrderSummaryDailyOrders>();

    public OrderSummary(LocalDate date) {
        this.date = date;
    }

    public OrderSummary() {
    }

    public OrderSummary date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
  *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public OrderSummary foods(List<OrdersChefInnerFoods> foods) {
        this.foods = foods;
        return this;
    }

    public OrderSummary addFoodsItem(OrdersChefInnerFoods foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
  *
     */
    @ApiModelProperty(value = "")
    public List<OrdersChefInnerFoods> getFoods() {
        return foods;
    }

    public void setFoods(List<OrdersChefInnerFoods> foods) {
        this.foods = foods;
    }

    public OrderSummary dailyOrders(List<OrderSummaryDailyOrders> dailyOrders) {
        this.dailyOrders = dailyOrders;
        return this;
    }

    public OrderSummary addDailyOrdersItem(OrderSummaryDailyOrders dailyOrdersItem) {
        this.dailyOrders.add(dailyOrdersItem);
        return this;
    }

    /**
     * Get dailyOrders
     *
     * @return dailyOrders
  *
     */
    @ApiModelProperty(value = "")
    public List<OrderSummaryDailyOrders> getDailyOrders() {
        return dailyOrders;
    }

    public void setDailyOrders(List<OrderSummaryDailyOrders> dailyOrders) {
        this.dailyOrders = dailyOrders;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderSummary orderSummary = (OrderSummary) o;
        return Objects.equals(this.date, orderSummary.date)
                && Objects.equals(this.foods, orderSummary.foods)
                && Objects.equals(this.dailyOrders, orderSummary.dailyOrders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, foods, dailyOrders);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class OrderSummary {\n");

        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("    dailyOrders: ").append(toIndentedString(dailyOrders)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
