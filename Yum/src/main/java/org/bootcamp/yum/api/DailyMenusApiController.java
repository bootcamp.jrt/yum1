/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.bootcamp.yum.api;




import org.bootcamp.yum.exception.ApiConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;
import io.swagger.annotations.*;

import org.bootcamp.yum.service.DailyMenusService;
import org.bootcamp.yum.api.model.DailyMenuSave;
import org.bootcamp.yum.api.model.DailyMenusChef;
import org.bootcamp.yum.api.model.NewEntry;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T09:32:53.730+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class DailyMenusApiController implements DailyMenusApi {

    private DailyMenusService dailyMenusService;

    @Autowired
    public DailyMenusApiController(DailyMenusService dailyMenusService) {
        this.dailyMenusService = dailyMenusService;
    }
    
    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<DailyMenuSave> dailyMenusIdPut(@ApiParam(value = "", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "", required = true) @RequestBody DailyMenuSave body, @Valid Errors errors) throws ApiException {

        try {
            return new ResponseEntity<DailyMenuSave>(dailyMenusService.dailyMenusIdPut(id, body),HttpStatus.OK);
        } catch (OptimisticLockException ex) {
            try {
                throw new ApiConcurrentModificationException(dailyMenusService.getDailyMenuSaveByDailyMenuId(id));
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<DailyMenusChef> dailyMenusMonthlyGet() throws ApiException{

            DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyGet();
            return new ResponseEntity<DailyMenusChef>(dailyMenusChef,HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<DailyMenusChef> dailyMenusMonthlyYearMonthGet(@ApiParam(value = "", required = true) @PathVariable("month") Integer month,
            @ApiParam(value = "", required = true) @PathVariable("year") Integer year) throws ApiException {

            DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyYearMonthGet(month, year);
            return new ResponseEntity<DailyMenusChef>(dailyMenusChef,HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<NewEntry> dailyMenusPost(@ApiParam(value = "", required = true) @RequestBody DailyMenuSave body) throws ApiException{
      

        NewEntry newEntry =  dailyMenusService.dailyMenusPost(body);
       
        return new ResponseEntity<NewEntry>(newEntry,HttpStatus.OK);

    }

}
