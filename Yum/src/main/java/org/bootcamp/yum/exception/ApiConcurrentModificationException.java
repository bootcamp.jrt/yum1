/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.bootcamp.yum.exception;

import org.bootcamp.yum.exception.ApiException;


public class ApiConcurrentModificationException extends ApiException{
    
    private Object dto;

    public ApiConcurrentModificationException(String message, Object dto) {
        super(409, message);
        this.dto = dto;
    }
    
    public ApiConcurrentModificationException(Object dto) {
        super(409, "Concurrent Modification Error");
        this.dto = dto;
    }
    
    public Object getDto() {
        return dto;
    }
}
