drop schema if exists yum;
create schema yum;
use yum;

CREATE TABLE yum(
	id bigint not null,
	terms longtext not null,
    privacy longtext not null,
    notes longtext not null,
    deadline time not null,
    currency varchar(10) not null,
    foods_version int not null DEFAULT 0,
    version int not null DEFAULT 0,
    primary key(id)
);

CREATE TABLE user(
	user_id bigint not null auto_increment,
    first_name varchar(100) null,
	last_name varchar(100) null,
	role varchar(10) not null,
	password varchar(64) not null,
	registration_date date not null,
	picture MEDIUMBLOB,
	approved BOOLEAN null,
	email varchar(40) not null,
    version int not null DEFAULT 0,    
    secret varchar(60) null,
	secret_expiration_time timestamp null,
    primary key(user_id)
);

CREATE TABLE food(
	food_id bigint NOT NULL AUTO_INCREMENT,
	food_name varchar(255) not null,
    description varchar(255) null,
	price double not null,
	archived boolean null,
	type varchar(10) not null,
    popularity bigint not null default 0,
    version int not null DEFAULT 0,
    primary key(food_id)
);

CREATE TABLE daily_menu(
    daily_menu_id bigint NOT NULL auto_increment, 
	date DATE not null,
    version int not null DEFAULT 0,    
	PRIMARY KEY(daily_menu_id)
);

create table daily_order(
	daily_order_id bigint not null auto_increment,
	user_id bigint not null,
	daily_menu_id bigint not null,
	final bit null,
    version int not null DEFAULT 0,
	primary key(daily_order_id)
);

create table order_item(
	daily_order_id bigint not null,
	food_id bigint not null,
    quantity int not null,
	primary key (daily_order_id, food_id)
);

CREATE TABLE daily_menu_food(
    daily_menu_id bigint not null,
    food_id bigint not null,
	PRIMARY KEY(daily_menu_id, food_id)
);

ALTER TABLE daily_order ADD foreign key (user_id) references user(user_id);
ALTER TABLE daily_order ADD foreign key (daily_menu_id) references daily_menu(daily_menu_id);
ALTER TABLE order_item ADD foreign key (daily_order_id) references daily_order(daily_order_id);
ALTER TABLE order_item ADD foreign key (food_id) references food(food_id);
ALTER TABLE daily_menu_food ADD foreign key (daily_menu_id) references daily_menu(daily_menu_id);
ALTER TABLE daily_menu_food ADD foreign key (food_id) references food(food_id);

INSERT INTO yum (id,terms,privacy,notes,deadline,currency) 
VALUES (1,'<h2>Conditions of Use</h2>&#10;<p>Welcome to our online store! MYCOMPANY and its associates provide their services to you subject to the following conditions. If you visit or shop within this website, you accept these conditions. Please read them carefully.&#160;&#8203;</p>&#10;<h2>PRIVACY</h2>&#10;<p>Please review our Privacy Notice, which also governs your visit to our website, to understand our practices.</p>&#10;<h2>ELECTRONIC COMMUNICATIONS</h2>&#10;<p>When you visit MYCOMPANY or send e-mails to us, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.</p>&#10;<h2>COPYRIGHT</h2>&#10;<p>All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of MYCOMPANY or its content suppliers and protected by international copyright laws. The compilation of all content on this site is the exclusive property of MYCOMPANY, with copyright authorship for this collection by MYCOMPANY, and protected by international copyright laws.</p>&#10;<h2>TRADE MARKS</h2>&#10;<p>MYCOMPANYs trademarks and trade dress may not be used in connection with any product or service that is not MYCOMPANYs, in any manner that is likely to cause confusion among customers, or in any manner that disparages or discredits MYCOMPANY. All other trademarks not owned by MYCOMPANY or its subsidiaries that appear on this site are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by MYCOMPANY or its subsidiaries.</p>&#10;<h2>LICENSE AND SITE ACCESS</h2>&#10;<p>MYCOMPANY grants you a limited license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent of MYCOMPANY. This license does not include any resale or commercial use of this site or its contents: any collection and use of any product listings, descriptions, or prices: any derivative use of this site or its contents: any downloading or copying of account information for the benefit of another merchant: or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of MYCOMPANY. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of MYCOMPANY and our associates without express written consent. You may not use any meta tags or any other &#34;hidden text&#34; utilizing MYCOMPANYs name or trademarks without the express written consent of MYCOMPANY. Any unauthorized use terminates the permission or license granted by MYCOMPANY. You are granted a limited, revocable, and nonexclusive right to create a hyperlink to the home page of MYCOMPANY so long as the link does not portray MYCOMPANY, its associates, or their products or services in a false, misleading, derogatory, or otherwise offensive matter. You may not use any MYCOMPANY logo or other proprietary graphic or trademark as part of the link without express written permission.</p>&#10;<h2>YOUR MEMBERSHIP ACCOUNT</h2>&#10;<p>If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. If you are under 18, you may use our website only with involvement of a parent or guardian. MYCOMPANY and its associates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.</p>','<p><u>Business privacy policy &#8211; sample template</u></p>&#10;<p>This privacy policy sets out how [business name] uses and protects any information that you give [business name] when you use this website.</p>&#10;<p>[business name] is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>&#10;<p>[business name] may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from [date].</p>&#10;<p><strong>What we collect</strong></p>&#10;<p>We may collect the following information:</p>&#10;<ul>&#10;<li>name and job title</li>&#10;<li>contact information including email address</li>&#10;<li>demographic information such as postcode, preferences and interests</li>&#10;<li>other information relevant to customer surveys and/or offers</li>&#10;</ul>&#10;<p><strong>What we do with the information we gather</strong></p>&#10;<p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>&#10;<ul>&#10;<li>Internal record keeping.</li>&#10;<li>We may use the information to improve our products and services.</li>&#10;<li>We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li>&#10;<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.</li>&#10;</ul>&#10;<p><strong>Security</strong></p>&#10;<p>We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>&#10;<p><strong>How we use cookies</strong></p>&#10;<p>A cookie is a small file which asks permission to be placed on your computer\'s hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>&#10;<p>We use traffic log cookies to identify which pages are being used. This helps us analyse data about web page traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>&#10;<p>Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>&#10;<p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>&#10;<p><strong>Links to other websites</strong></p>&#10;<p>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>&#10;<p><strong>Controlling your personal information</strong></p>&#10;<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>&#10;<ul>&#10;<li>whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</li>&#10;<li>if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at [email address]</li>&#10;</ul>&#10;<p>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>&#10;<p>You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to [address].</p>&#10;<p>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>&#10;<p>&#160;</p>','<ul>&#10;<li>All meals include slices of bread.</li>&#10;<li>Meals are delivered in a micorwave compatible plastic box. Disposable utensils are also included.</li>&#10;<li>Payments will be collected by the reception desk. Mind to have the exact amount.</li>&#10;<li>Deadline for signing-up for the next day\'s meal is by 12:00 of the previous day. Make sure that you entered your name in the respective online list and saved the file.</li>&#10;</ul>','12:00:00','&euro;');

INSERT INTO user (user_id,first_name,last_name,role,password,registration_date,approved,email) 
values (1,'admin','admin','admin','$2a$10$/To1IbcEX6l7HiRsg5xMu.f/Gh.b9RS3vXrmX1ebyO1DA0Oe4oqOe',CURDATE(),true,'admin@yum.com');
